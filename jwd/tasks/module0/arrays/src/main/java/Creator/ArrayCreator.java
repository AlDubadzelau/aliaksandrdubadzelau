package Creator;

public class ArrayCreator {

    public static int[] createIntArray(int amountOfItems) {

        return new int[amountOfItems];
    }

    public static double[] createDoubleArray(int amountOfItems) {

        return new double[amountOfItems];
    }

    public static int[][] createIntArray(int amountOfRows, int amountOfColumns){
        return new int[amountOfRows][amountOfColumns];
    }

    public static double[][] createDoubleArray(int amountOfRows, int amountOfColumns){
        return new double[amountOfRows][amountOfColumns];
    }
}
