package view;

import java.util.Arrays;

public class Printer {

    private static final String RESULT_MESSAGE = "Result: ";
    private static final String DEFAULT_DIVIDER = " ";

    public static void printInfo(String message){
        System.out.println(message);
    }

    public static void printInfo(StringBuilder message){
        System.out.println(RESULT_MESSAGE + message.toString());
    }

    public static void printInfo(int number){
        System.out.println(RESULT_MESSAGE + number);
    }

    public static void printInfo(double number){
        System.out.println(RESULT_MESSAGE + number);
    }

    public static void printInfo(boolean number){
        System.out.println(RESULT_MESSAGE + number);
    }

    public static void printInfo(int[] array){
        System.out.println(RESULT_MESSAGE + Arrays.toString(array));
    }

    public static void printInfo(double[] array){
        System.out.println(RESULT_MESSAGE + Arrays.toString(array));
    }

    public static void printInfo(int[][] array){
        for (int[] ints : array) {
            for (int anInt : ints) {
                System.out.print(anInt + DEFAULT_DIVIDER);
            }
            System.out.println();
        }
    }

    public static void printInfo(double[][] array){
        for (double[] ints : array) {
            for (double anInt : ints) {
                System.out.print(anInt + DEFAULT_DIVIDER);
            }
            System.out.println();
        }
    }
}
