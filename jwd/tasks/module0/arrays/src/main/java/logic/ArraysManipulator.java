package logic;

import Creator.ArrayCreator;
import utils.ArrayInitializer;
import view.Printer;

public class ArraysManipulator {

    private static final int DEFAULT_ZERO_NUMBER = 0;
    private static final int DEFAULT_FIRST_NUMBER = 1;
    private static final int DEFAULT_SECOND_NUMBER = 2;
    private static final String POSITIVE_NUMBER_ANSWER = "Positive number";
    private static final String NEGATIVE_NUMBER_ANSWER = "Negative number";
    private static final String DEFAULT_ANSWER = "First number is 0";
    private static final String DEFAULT_MESSAGE = "Something go wrong";
    private static final boolean POSITIVE_RESULT = true;
    private static final boolean NEGATIVE_RESULT = false;

    //1
    public static int findSumOfItems(int amountOfItems, int divider) {

        int sum = DEFAULT_ZERO_NUMBER;
        int[] array = ArrayCreator.createIntArray(amountOfItems);
        ArrayInitializer.initializeIntArray(array);

        for (int item : array) {
            if (item % divider == DEFAULT_ZERO_NUMBER) {
                sum += item;
            }
        }

        return sum;
    }

    //2
    public static int[] createZeroPositionArray(double[] array) {

        int amountOfZero = ArraysManipulator.findAmountOfZeroItems(array);
        int[] zeroPositionArray = ArrayCreator.createIntArray(amountOfZero);
        int iterator = DEFAULT_ZERO_NUMBER;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == DEFAULT_ZERO_NUMBER) {
                zeroPositionArray[iterator] = i;
                iterator++;
            }
        }

        return zeroPositionArray;
    }

    public static int findAmountOfZeroItems(double[] array) {

        int result = DEFAULT_ZERO_NUMBER;

        for (double item : array) {
            if (item == DEFAULT_ZERO_NUMBER) {
                result += DEFAULT_FIRST_NUMBER;
            }
        }

        return result;
    }

    //3
    public static String checkFirstItem(double[] array) {

        String result = DEFAULT_ANSWER;

        if (array[DEFAULT_ZERO_NUMBER] < DEFAULT_ZERO_NUMBER) {
            result = NEGATIVE_NUMBER_ANSWER;
        } else {
            result = POSITIVE_NUMBER_ANSWER;
        }

        return result;
    }

    //4
    public static boolean checkIncreasingSequence(double[] array) {

        boolean result = POSITIVE_RESULT;
        int iterator = DEFAULT_FIRST_NUMBER;
        double previousNumber = array[DEFAULT_ZERO_NUMBER];

        while (iterator < array.length) {
            if (array[iterator] <= previousNumber) {
                result = NEGATIVE_RESULT;
                break;
            }

            previousNumber = array[iterator];
            iterator++;
        }

        return result;
    }

    //5  (so-bad :< )
    public static double[] findEvenNumbers(double[] array) {

        int amountOfItems = ArraysManipulator.findAmountOfEvenNumbers(array);
        double[] evenNumbersArray = ArrayCreator.createDoubleArray(amountOfItems);
        int iterator = DEFAULT_ZERO_NUMBER;

        if (amountOfItems == DEFAULT_ZERO_NUMBER) {
            Printer.printInfo(DEFAULT_MESSAGE);
            return evenNumbersArray;
        }
        for (double number : array) {
            if (number % DEFAULT_SECOND_NUMBER == DEFAULT_ZERO_NUMBER) {
                evenNumbersArray[iterator] = number;
                iterator++;
            }
        }

        return evenNumbersArray;
    }

    private static int findAmountOfEvenNumbers(double[] array) {

        int amountOfEvenNumbers = DEFAULT_ZERO_NUMBER;

        for (double number : array) {
            if (number % DEFAULT_SECOND_NUMBER == DEFAULT_ZERO_NUMBER) {
                amountOfEvenNumbers += DEFAULT_FIRST_NUMBER;
            }
        }

        return amountOfEvenNumbers;
    }

    //6
    public static int findLengthBetweenMaxAndMin(double[] array) {

        int positionOfMaxItem = ArraysManipulator.findMaxItemPosition(array);
        int positionOfMinItem = ArraysManipulator.findMinItemPosition(array);

        return Math.abs(positionOfMaxItem - positionOfMinItem);
    }

    private static int findMaxItemPosition(double[] array) {

        double maxItem = array[DEFAULT_ZERO_NUMBER];
        int positionOfMaxItem = DEFAULT_ZERO_NUMBER;

        for (int i = DEFAULT_FIRST_NUMBER; i < array.length; i++) {
            if (maxItem < array[i]) {
                maxItem = array[i];
                positionOfMaxItem = i;
            }
        }

        return positionOfMaxItem;
    }

    private static int findMinItemPosition(double[] array) {

        double minItem = array[DEFAULT_ZERO_NUMBER];
        int positionOfMinItem = DEFAULT_ZERO_NUMBER;

        for (int i = DEFAULT_FIRST_NUMBER; i < array.length; i++) {
            if (minItem > array[i]) {
                minItem = array[i];
                positionOfMinItem = i;
            }
        }

        return positionOfMinItem;
    }

    //7
    public static double[] changeArrayItems(double comparedNumber, double[] array) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] > comparedNumber) {
                array[i] = comparedNumber;
            }
        }

        return array;
    }

    public static int countAmountOfReplacement(double[] array, double[] newArray) {

        int amountOfReplacement = DEFAULT_ZERO_NUMBER;

        for (int i = 0; i < array.length; i++) {
            if (array[i] != newArray[i]) {
                amountOfReplacement += DEFAULT_FIRST_NUMBER;
            }
        }

        return amountOfReplacement;
    }

    //8
    public static int findAmountOfNegativeNumbers(double[] array) {

        int amountOfNegativeNumbers = DEFAULT_ZERO_NUMBER;

        for (double item : array) {
            if (item < DEFAULT_ZERO_NUMBER) {
                amountOfNegativeNumbers += DEFAULT_FIRST_NUMBER;
            }
        }

        return amountOfNegativeNumbers;
    }

    public static int findAmountOfPositiveNumbers(double[] array) {

        int amountOfPositiveNumbers = DEFAULT_ZERO_NUMBER;

        for (double item : array) {
            if (item > DEFAULT_ZERO_NUMBER) {
                amountOfPositiveNumbers += DEFAULT_FIRST_NUMBER;
            }
        }

        return amountOfPositiveNumbers;
    }

    //9
    public static double[] changeMaxAndMinItems(double[] array) {

        int positionOfMaxItem = ArraysManipulator.findMaxItemPosition(array);
        int positionOfMinItem = ArraysManipulator.findMinItemPosition(array);

        array[positionOfMaxItem] = array[positionOfMaxItem] + array[positionOfMinItem];
        array[positionOfMinItem] = array[positionOfMaxItem] - array[positionOfMinItem];
        array[positionOfMaxItem] = array[positionOfMaxItem] - array[positionOfMinItem];

        return array;
    }

    //10
    public static void findCertainItems(double[] arrray) {

        for (int i = 0; i < arrray.length; i++) {
            if (i < arrray[i]) {
                Printer.printInfo(arrray[i]);
            }
        }
    }

    //11
    public static int[] findDivisibleNumbers(int[] array, int divider, int remains) {

        int iterator = DEFAULT_ZERO_NUMBER;
        int amountOfItems = ArraysManipulator.findAmountOfCertainItems(array, divider, remains);
        int[] certainItemsArray = ArrayCreator.createIntArray(amountOfItems);


        for (int item : array) {
            if (item % divider == remains) {
                certainItemsArray[iterator] = item;
                iterator++;
            }
        }

        return certainItemsArray;
    }

    private static int findAmountOfCertainItems(int[] array, int divider, int remains) {

        int amountOfItems = DEFAULT_ZERO_NUMBER;

        for (int item : array) {
            if (item % divider == remains) {
                amountOfItems += DEFAULT_FIRST_NUMBER;
            }
        }

        return amountOfItems;
    }


    //12


    //13
    public static int findAmountOfChanges(int[] array, int divider, int firstBorder, int secondBorder) {

        int amountOfChanges = DEFAULT_ZERO_NUMBER;

        for (int item : array) {
            if (item % divider == DEFAULT_ZERO_NUMBER && item < secondBorder && item > firstBorder) {
                amountOfChanges += DEFAULT_FIRST_NUMBER;
            }
        }

        return amountOfChanges;
    }

    //14
    public static double findSumOfMaxAndMin(double[] array) {

        double maxItem = array[ArraysManipulator.findMaxItemPosition(array)];
        double minItem = array[ArraysManipulator.findMinItemPosition(array)];

        return maxItem + minItem;
    }

    //15
    public static double[] findNumbersInInterval(double[] array, double firstBorder, double secondBorder) {

        int iterator = DEFAULT_ZERO_NUMBER;
        int amountOfItems = ArraysManipulator.findAmountOfItems(array, firstBorder, secondBorder);
        double[] suitableItems = ArrayCreator.createDoubleArray(amountOfItems);


        for (double item : array) {
            if (item <= secondBorder && item >= firstBorder) {
                suitableItems[iterator] = item;
                iterator++;
            }
        }

        return suitableItems;
    }

    private static int findAmountOfItems(double[] array, double firstBorder, double secondBorder) {

        int amountOfItems = DEFAULT_ZERO_NUMBER;


        for (double item : array) {
            if (item <= secondBorder && item >= firstBorder) {
                amountOfItems++;
            }
        }

        return amountOfItems;
    }


    //16


    //17
    public static double[] deleteMinItems(double[] array) {

        int iterator = DEFAULT_ZERO_NUMBER;
        double minItem = array[ArraysManipulator.findMinItemPosition(array)];
        int amountOfMinItems = ArraysManipulator.findAmountOfMinItems(array, minItem);
        int lengthOfNewArray = ArraysManipulator.findNewLength(array.length, amountOfMinItems);
        double[] newArray = ArrayCreator.createDoubleArray(lengthOfNewArray);

        for (double item : array) {
            if (item != minItem) {
                newArray[iterator] = item;
                iterator++;
            }
        }

        return newArray;
    }

    private static int findAmountOfMinItems(double[] array, double minItem) {

        int amountOfItems = DEFAULT_ZERO_NUMBER;

        for (double item : array) {
            if (item == minItem) {
                amountOfItems++;
            }
        }

        return amountOfItems;
    }

    private static int findNewLength(int length, int difference) {

        return length - difference;
    }


    //18


    //19


    //20
    public static double[] squeezeArray(double[] array) {

        ArraysManipulator.swapElements(array);
        int swapCounter = DEFAULT_ZERO_NUMBER;
        int lastPosition = array.length - DEFAULT_FIRST_NUMBER;

        for (int i = lastPosition; i >= DEFAULT_FIRST_NUMBER; i--) {
            if (array[i] == DEFAULT_ZERO_NUMBER) {
                array[i] = array[lastPosition - swapCounter];
                array[lastPosition - swapCounter] = DEFAULT_ZERO_NUMBER;
                swapCounter++;
            }
        }

        return array;
    }

    private static double[] swapElements(double[] array){

        for (int i = DEFAULT_FIRST_NUMBER; i < array.length; i += DEFAULT_SECOND_NUMBER) {
            array[i] = 0;
        }
        return array;
    }
}
