package logic;

import Creator.ArrayCreator;
import utils.ArrayInitializer;
import view.Printer;

import java.util.Random;

public class MultidimensionalArraysManipulator {

    private static final int MAX_NUMBER = 15;
    private static final int AMOUNT_OF_ROWS = 3;
    private static final int AMOUNT_OF_COLUMNS = 4;
    private static final int DEFAULT_REMAINS = -1;
    private static final int DEFAULT_ZERO_NUMBER = 0;
    private static final int DEFAULT_FIRST_NUMBER = 1;
    private static final int DEFAULT_SECOND_NUMBER = 2;
    private static final int DEFAULT_THIRD_NUMBER = 3;
    private static final int DEFAULT_FIFTH_NUMBER = 5;
    private static final int DEFAULT_SEVENTH_NUMBER = 7;
    private static final int DEFAULT_NINTH_NUMBER = 9;
    private static final int MIN_TWO_DIGIT_NUMBER = 10;
    private static final int MAX_TWO_DIGIT_NUMBER = 99;

    private static final String DEFAULT_DIVIDER = " ";


    //1 !!!!!!
    public static int[][] createLargeMatrix() {

        int[][] array = ArrayCreator.createIntArray(AMOUNT_OF_ROWS, AMOUNT_OF_COLUMNS);

        for (int i = 0; i < AMOUNT_OF_ROWS; i++) {
            array[i][DEFAULT_ZERO_NUMBER] = DEFAULT_FIRST_NUMBER;
        }

        return array;
    }

    //2
    public static int[][] createLittleMatrix() {

        Random random = new Random();
        int[][] array = ArrayCreator.createIntArray(DEFAULT_SECOND_NUMBER, DEFAULT_THIRD_NUMBER);

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                array[i][k] = random.nextInt(DEFAULT_NINTH_NUMBER);
            }
        }

        return array;

    }

    //3 -
    public static void printCertainColumns(int[][] array) {

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (k == DEFAULT_ZERO_NUMBER || k == array[i].length - DEFAULT_FIRST_NUMBER) {
                    System.out.print(array[i][k] + DEFAULT_DIVIDER);
                }
            }
            System.out.println();
        }
    }


    //4 -
    public static void printCertainRows(int[][] array) {

        for (int i = 0; i < array.length; i++) {
            if (i == DEFAULT_ZERO_NUMBER || i == array.length - DEFAULT_FIRST_NUMBER) {
                Printer.printInfo(array[i]);
            }
        }
    }

    //5 -
    public static void printEvenNumberedRows(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            if (i % DEFAULT_SECOND_NUMBER == DEFAULT_FIRST_NUMBER) {
                Printer.printInfo(array[i]);
            }
        }
    }

    //6 -
    public static void printEvenNumberedColumns(int[][] array) {
        for (int[] ints : array) {
            for (int i = 0; i < ints.length; i++) {
                if (i % DEFAULT_SECOND_NUMBER == DEFAULT_ZERO_NUMBER && array[DEFAULT_ZERO_NUMBER][i] > array[array.length - DEFAULT_FIRST_NUMBER][i]) {
                    System.out.print(ints[i] + DEFAULT_DIVIDER);
                }
            }
            System.out.println();
        }
    }

    //7
    public static int findSumOfNegativeNumbers(int[][] array) {

        int sum = DEFAULT_ZERO_NUMBER;

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (array[i][k] % DEFAULT_SECOND_NUMBER == DEFAULT_REMAINS) {
                    sum += Math.abs(array[i][k]);
                }
            }
        }

        return sum;
    }

    //8
    public static int findAmountOfSevenNumbers(int[][] array) {

        int amount = DEFAULT_ZERO_NUMBER;

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (array[i][k] == DEFAULT_SEVENTH_NUMBER) {
                    amount += DEFAULT_FIRST_NUMBER;
                }
            }
        }

        return amount;
    }

    //9 -
    public static void printDiagonalItems(double[][] array) {

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (i == k) {
                    Printer.printInfo(array[i][k]);
                }
            }
        }
    }

    //10 -
    public static void printColumn(double[][] array, int numOfColumn) {

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array.length; k++) {
                if (k == numOfColumn) {
                    Printer.printInfo(array[i][k]);
                }
            }
        }
    }

    //11 -
    public static void printMatrix(double[][] array) {

        for (int i = 0; i < array.length; i++) {
            if (i % DEFAULT_SECOND_NUMBER == DEFAULT_ZERO_NUMBER) {
                for (int k = array[i].length - DEFAULT_FIRST_NUMBER; k >= 0; k--) {
                    System.out.print(array[i][k] + DEFAULT_DIVIDER);
                }
            } else {
                for (int k = 0; k < array[i].length; k++) {
                    System.out.print(array[i][k] + DEFAULT_DIVIDER);
                }
            }
            System.out.println();
        }
    }

    //12
    public static int[][] createSquareMatrix(int size) {

        int[][] array = ArrayCreator.createIntArray(size, size);

        for (int i = 0; i < size; i++) {
            for (int k = 0; k < size; k++) {
                if (i == k) {
                    array[i][k] = DEFAULT_ZERO_NUMBER + i;
                }
            }
        }

        return array;
    }

    //13
    public static int[][] createSquareCertainMatrix(int size) {

        int[][] array = ArrayCreator.createIntArray(size, size);

        for (int i = 0; i < array.length; i++) {
            if (i % DEFAULT_SECOND_NUMBER == DEFAULT_ZERO_NUMBER) {
                for (int k = array[i].length - DEFAULT_FIRST_NUMBER; k >= 0; k--) {
                    array[i][k] = k + DEFAULT_FIRST_NUMBER;
                }
            } else {
                for (int k = 0; k < array[i].length; k++) {
                    array[i][k] = size - k;
                }
            }

        }

        return array;
    }

    //14
    public static int[][] createReverseMatrix(int size) {

        int[][] array = ArrayCreator.createIntArray(size, size);

        int maxNumberOfCell = size - DEFAULT_FIRST_NUMBER;

        for (int i = 0; i < size; i++) {
            for (int k = 0; k < size; k++) {
                if (i + k == maxNumberOfCell) {
                    array[i][k] = DEFAULT_FIRST_NUMBER + i;
                }
            }
        }

        return array;
    }

    //15
    public static int[][] createSquareReverseCertainMatrix(int size) {

        int[][] array = ArrayCreator.createIntArray(size, size);

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (i == k) {
                    array[i][k] = size - i;

                }
            }
        }

        return array;
    }

    //16
    public static int[][] createMultiplicativeMatrix(int size) {

        int[][] array = ArrayCreator.createIntArray(size, size);

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (i == k) {
                    array[i][k] = (i + DEFAULT_FIRST_NUMBER) * (i + DEFAULT_SECOND_NUMBER);

                }
            }
        }

        return array;

    }

    //17
    public static int[][] createOneMatrix(int size) {

        int[][] array = ArrayCreator.createIntArray(size, size);
        int maxNumberOfCell = size - DEFAULT_FIRST_NUMBER;

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (i == DEFAULT_ZERO_NUMBER || k == DEFAULT_ZERO_NUMBER || k == maxNumberOfCell || i == maxNumberOfCell) {
                    array[i][k] = DEFAULT_FIRST_NUMBER;

                }
            }
        }

        return array;

    }

    //18
    public static int[][] createSemiMatrix(int size) {

        int[][] array = ArrayCreator.createIntArray(size, size);

        int maxNumberOfCell = size - DEFAULT_FIRST_NUMBER;

        for (int i = 0; i < size; i++) {
            for (int k = 0; k < size; k++) {
                if (i + k <= maxNumberOfCell) {
                    array[i][k] = DEFAULT_FIRST_NUMBER + i;
                }
            }
        }

        return array;
    }


    //19


    //20


    //21


    //22


    //23
    public static double[][] createSinMatrix(int size) {

        double[][] array = ArrayCreator.createDoubleArray(size, size);

        for (int i = 0; i < size; i++) {
            for (int k = 0; k < size; k++) {
                array[i][k] = Math.sin(((i * i) - (k * k)) / size);
            }
        }

        return array;
    }

    public static int findAmountOfPositiveNumbers(double[][] array) {

        int amountOfPositiveNumbers = DEFAULT_ZERO_NUMBER;

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (array[i][k] > 0) {
                    amountOfPositiveNumbers++;
                }
            }
        }

        return amountOfPositiveNumbers;
    }

    //24
    public static double[][] createMatrix(double[] array) {

        int size = array.length;
        double[][] matrix = ArrayCreator.createDoubleArray(size, size);

        for (int i = 0; i < size; i++) {
            for (int k = 0; k < size; k++) {
                matrix[i][k] = Math.pow(array[k], i + DEFAULT_FIRST_NUMBER);
            }
        }

        return matrix;
    }

    //25
    public static double[][] createMatrix(int size) {

        double[][] array = ArrayCreator.createDoubleArray(size, size);

        for (int i = 0; i < size; i++) {
            for (int k = 0; k < size; k++) {
                array[i][k] = DEFAULT_FIRST_NUMBER + k + size * i;
            }
        }

        return array;
    }


    //26


    //27
    public static void changeColumns(double[][] array, int firstColumn, int secondColumn) {


        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (k == firstColumn) {
                    array[i][k] = array[i][k] + array[i][secondColumn];
                    array[i][secondColumn] = array[i][k] - array[i][secondColumn];
                    array[i][k] = array[i][k] - array[i][secondColumn];
                }
            }
        }
    }

    //28
    public static double findMaxSum(double[][] matrix) {

        double[] sumArray = MultidimensionalArraysManipulator.createArrayOfColumnSums(matrix);
        double maxSum = DEFAULT_ZERO_NUMBER;

        for (int i = 0; i < sumArray.length; i++) {
            if (maxSum < sumArray[i]) {
                maxSum = sumArray[i];
            }
        }

        return maxSum;
    }

    public static double[] createArrayOfColumnSums(double[][] matrix) {

        double[] sumArray = ArrayCreator.createDoubleArray(matrix[DEFAULT_ZERO_NUMBER].length);

        for (int i = 0; i < matrix.length; i++) {
            int sum = DEFAULT_ZERO_NUMBER;
            for (int k = 0; k < matrix[i].length; k++) {
                sum += matrix[k][i];
            }
            sumArray[i] = sum;
        }

        return sumArray;
    }

    //29
    public static double[] findPositiveItems(double[][] matrix) {

        int size = MultidimensionalArraysManipulator.findAmountOfPositiveItems(matrix);
        int iterator = DEFAULT_ZERO_NUMBER;
        double[] amountOfPositiveNumbers = new double[size];


        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                if (i == k && matrix[i][k] > DEFAULT_ZERO_NUMBER) {
                    amountOfPositiveNumbers[iterator] = matrix[i][k];
                    iterator++;
                }
            }
        }

        return amountOfPositiveNumbers;
    }

    private static int findAmountOfPositiveItems(double[][] matrix) {

        int amount = DEFAULT_ZERO_NUMBER;

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                if (i == k && matrix[i][k] > DEFAULT_ZERO_NUMBER) {
                    amount++;
                }
            }
        }

        return amount;
    }

    //30
    public static void initializeArray(int[][] matrix) {
        Random random = new Random();

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                matrix[i][k] = random.nextInt(MAX_NUMBER);
            }
        }
    }

    public static StringBuilder findRows(int[][] matrix) {

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < matrix.length; i++) {
            int counter = DEFAULT_ZERO_NUMBER;
            for (int k = 0; k < matrix[i].length; k++) {
                if (matrix[i][k] == DEFAULT_FIFTH_NUMBER) {
                    counter++;
                }
            }
            if (counter >= DEFAULT_THIRD_NUMBER) {
                result.append(i);
                result.append(DEFAULT_DIVIDER);
            }
        }

        return result;
    }

    //31
    public static void initializeLargeArray(int[][] matrix) {

        Random random = new Random();

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                ArrayInitializer.initializeIntArray(matrix[i]);
            }
        }
    }

    public static int findAmountOfTwoDigitNumbers(int[][] matrix) {

        int amountOfTwoDigitNumbers = DEFAULT_ZERO_NUMBER;

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                if (matrix[i][k] >= MIN_TWO_DIGIT_NUMBER && matrix[i][k] <= MAX_TWO_DIGIT_NUMBER) {
                    amountOfTwoDigitNumbers++;
                }
            }
        }

        return amountOfTwoDigitNumbers;
    }


    //32


    //33


    //34(условие?? если строк меньше столбцов) (работает корректно только с квадратной)
    public static void initializeSpecialMatrix(int[][] matrix) {

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                if (i + DEFAULT_FIRST_NUMBER > k) {
                    matrix[k][i] = DEFAULT_FIRST_NUMBER;
                } else {
                    matrix[k][i] = DEFAULT_ZERO_NUMBER;
                }
            }
        }

    }

    //35
    private static double findMaxItem(double[][] matrix) {

        double maxNum = DEFAULT_ZERO_NUMBER;

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                if (maxNum < matrix[i][k]) {
                    maxNum = matrix[i][k];
                }
            }
        }

        return maxNum;
    }

    public static void changeItems(double[][] matrix) {

        double maxNum = findMaxItem(matrix);

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                if (matrix[i][k] % DEFAULT_SECOND_NUMBER == DEFAULT_FIRST_NUMBER || matrix[i][k] % DEFAULT_SECOND_NUMBER == DEFAULT_REMAINS) {
                    matrix[i][k] = maxNum;
                }
            }
        }
    }




    //36




    //37




    //38
    public static double[][] findSumOfMatrix(double[][] firstMatrix, double[][] secondMatrix){

        double[][] newMatrix = ArrayCreator.createDoubleArray(firstMatrix.length, firstMatrix[DEFAULT_ZERO_NUMBER].length);

        for(int i = 0; i < newMatrix.length; i++){
            for (int k = 0; k < newMatrix[i].length; k++){
                newMatrix[i][k] = firstMatrix[i][k] + secondMatrix[i][k];
            }
        }

        return newMatrix;
    }


    //39

    //40
}
