package controller;

import logic.ArraysManipulator;
import logic.MultidimensionalArraysManipulator;
import utils.UserInput;
import view.Printer;

public class ArraysStarter {

    public static void main(String[] args) {

        //1
        Printer.printInfo("1)\n");
        int sumOfItems = ArraysManipulator.findSumOfItems(100, 5);
        Printer.printInfo(sumOfItems);

        //2
        Printer.printInfo("2)\n");
        double[] zeroArray = new double[]{-1, 0, 0, 0, 1, 0, 1, 2, 3, 0, 0, 0, 45, 0};
        Printer.printInfo("Source array:");
        Printer.printInfo(zeroArray);
        int[] zeroPositionArray = ArraysManipulator.createZeroPositionArray(zeroArray);
        Printer.printInfo("Zero position:");
        Printer.printInfo(zeroPositionArray);

        //3
        Printer.printInfo("3)\n");
        Printer.printInfo("Source array:");
        Printer.printInfo(zeroArray);
        Printer.printInfo("First item is positive or negative: ");
        String answer = ArraysManipulator.checkFirstItem(zeroArray);
        Printer.printInfo(answer);

        //4
        Printer.printInfo("4)\n");
        double[] arr1 = new double[]{10, 11, 20, 21, 40, 39};
        Printer.printInfo(arr1);
        Printer.printInfo("Increasing sequence? - ");
        System.out.println(ArraysManipulator.checkIncreasingSequence(arr1));

        //5
        Printer.printInfo("5)\n");
        Printer.printInfo(arr1);
        Printer.printInfo("New array: ");
        double[] arr2 = ArraysManipulator.findEvenNumbers(arr1);
        Printer.printInfo(arr2);

        //6
        Printer.printInfo("6)\n");
        Printer.printInfo(arr1);
        int result = ArraysManipulator.findLengthBetweenMaxAndMin(arr1);
        Printer.printInfo(result);

        //7
        Printer.printInfo("7)\n");
        Printer.printInfo(arr1);
        int number = 20;
        Printer.printInfo(number);
        double[] arr3 = ArraysManipulator.changeArrayItems(number, arr1);
        Printer.printInfo(arr3);
        int amountOfReplacement = ArraysManipulator.countAmountOfReplacement(arr1, arr3);
        Printer.printInfo("Amount of replacements: ");
        Printer.printInfo(amountOfReplacement);

        //8
        Printer.printInfo("8)\n");
        double[] arr4 = new double[]{0, 0, 0, 0, 1, 20, 0, -1, -4, 11, 0, 0, 1, -5, 0};
        int amountOfNegativeItems = ArraysManipulator.findAmountOfNegativeNumbers(arr4);
        Printer.printInfo("Amount of negative items: ");
        Printer.printInfo(amountOfNegativeItems);
        int amountOfPositiveItems = ArraysManipulator.findAmountOfPositiveNumbers(arr4);
        Printer.printInfo("Amount of positive items: ");
        Printer.printInfo(amountOfPositiveItems);
        int amountOfZeroItems = ArraysManipulator.findAmountOfZeroItems(arr4);
        Printer.printInfo("Amount of zero items: ");
        Printer.printInfo(amountOfZeroItems);

        //9
        Printer.printInfo("9)\n");
        Printer.printInfo(arr4);
        double[] arr5 = ArraysManipulator.changeMaxAndMinItems(arr4);
        Printer.printInfo("New array: ");
        Printer.printInfo(arr5);

        //10
        Printer.printInfo("10)\n");
        Printer.printInfo(arr5);
        ArraysManipulator.findCertainItems(arr5);

        //11
        Printer.printInfo("11)\n");
        int[] arr6 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        Printer.printInfo(arr6);
        int divider = 2;
        int remains = 1;
        int[] divisibleNumbers = ArraysManipulator.findDivisibleNumbers(arr6, divider, remains);
        Printer.printInfo("Divider: " + divider);
        Printer.printInfo("Remains: " + remains);
        Printer.printInfo(divisibleNumbers);

        //13
        Printer.printInfo("13)\n");
        int[] arr7 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        Printer.printInfo(arr7);
        Printer.printInfo("Divider: " + remains);
        int firstBorder = 4;
        int secondBorder = 12;
        Printer.printInfo("Borders: " + firstBorder + " " + secondBorder);
        int amountOfNumbers = ArraysManipulator.findAmountOfChanges(arr7, divider, firstBorder, secondBorder);
        Printer.printInfo(amountOfNumbers);

        //14
        Printer.printInfo("14)\n");
        Printer.printInfo(arr4);
        double sum = ArraysManipulator.findSumOfMaxAndMin(arr4);
        Printer.printInfo(sum);

        //15
        Printer.printInfo("15)\n");
        Printer.printInfo(arr4);
        Printer.printInfo("Borders: " + firstBorder + " " + secondBorder);
        Printer.printInfo(ArraysManipulator.findNumbersInInterval(arr4, firstBorder, secondBorder));

        //17
        Printer.printInfo("17)\n");
        double[] arr8 = new double[]{0, 0, 0, 0, 1, 2, 5, 6, 0, 0, 0, 64, 633, 25, 0, 0};
        Printer.printInfo(arr8);
        double[] arr9 = ArraysManipulator.deleteMinItems(arr8);
        Printer.printInfo(arr9);

        //20
        Printer.printInfo("20)\n");
        double[] arr10 = new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        Printer.printInfo(arr10);
        double[] arr11 = ArraysManipulator.squeezeArray(arr10);
        Printer.printInfo(arr11);


        //1
        Printer.printInfo("1)\n");
        int[][] largeArray = MultidimensionalArraysManipulator.createLargeMatrix();
        Printer.printInfo(largeArray);

        //2
        Printer.printInfo("2)\n");
        int[][] littleArray = MultidimensionalArraysManipulator.createLittleMatrix();
        Printer.printInfo(littleArray);

        //3
        Printer.printInfo("3)\n");
        MultidimensionalArraysManipulator.printCertainColumns(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});

        //4
        Printer.printInfo("4)\n");
        MultidimensionalArraysManipulator.printCertainRows(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});

        //5
        Printer.printInfo("5)\n");
        MultidimensionalArraysManipulator.printEvenNumberedRows(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});

        //6
        Printer.printInfo("6)\n");
        MultidimensionalArraysManipulator.printEvenNumberedColumns(new int[][]{{1, 2, 9, 5, 3}, {4, 5, 6, 5, 2}, {7, 8, 3, 5, 1}});

        //7
        Printer.printInfo("7)\n");
        int[][] arrayWithNegativesNumbers = new int[][]{
                {-1, -2, 3, 4, 5},
                {6, -7, 8, 9, -10},
                {11, 12, -13, 14, 15},
                {-16, 17, 18, -19, 20},
                {21, -22, 23, 24, -25}};
        int sum1 = MultidimensionalArraysManipulator.findSumOfNegativeNumbers(arrayWithNegativesNumbers);
        Printer.printInfo(sum1);

        //8
        Printer.printInfo("8)\n");
        int[][] array = new int[][]{
                {-1, -7, 3, 4, 5},
                {6, -7, 8, 9, 7},
                {11, 12, -13, 14, 7},
                {-16, 7, 18, -19, 7},
                {21, 77, 23, 777, -25}};
        int amount = MultidimensionalArraysManipulator.findAmountOfSevenNumbers(array);
        Printer.printInfo(amount);

        //9
        Printer.printInfo("9)\n");
        double[][] squareArray = new double[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        MultidimensionalArraysManipulator.printDiagonalItems(squareArray);

        //10
        Printer.printInfo("10)\n");
        int numberOfColumn = 2;
        MultidimensionalArraysManipulator.printColumn(squareArray, numberOfColumn);

        //11
        Printer.printInfo("11)\n");
        MultidimensionalArraysManipulator.printMatrix(squareArray);

        //12
        Printer.printInfo("12)\n");
        Printer.printInfo("Enter the size of matrix: ");
        int size = UserInput.inputInt();
        int[][] matrix = MultidimensionalArraysManipulator.createSquareMatrix(size);
        Printer.printInfo(matrix);

        //13
        Printer.printInfo("13)\n");
        size = 6;
        matrix = MultidimensionalArraysManipulator.createSquareCertainMatrix(size);
        Printer.printInfo(matrix);

        //14
        Printer.printInfo("14)\n");
        matrix = MultidimensionalArraysManipulator.createReverseMatrix(size);
        Printer.printInfo(matrix);

        //15
        Printer.printInfo("15)\n");
        matrix = MultidimensionalArraysManipulator.createSquareReverseCertainMatrix(size);
        Printer.printInfo(matrix);

        //16
        Printer.printInfo("16)\n");
        matrix = MultidimensionalArraysManipulator.createMultiplicativeMatrix(size);
        Printer.printInfo(matrix);

        //17
        Printer.printInfo("17)\n");
        matrix = MultidimensionalArraysManipulator.createOneMatrix(size);
        Printer.printInfo(matrix);

        //18
        Printer.printInfo("18)\n");
        matrix = MultidimensionalArraysManipulator.createSemiMatrix(size);
        Printer.printInfo(matrix);

        //23
        Printer.printInfo("23)\n");
        double[][] sinArray = MultidimensionalArraysManipulator.createSinMatrix(size);
        Printer.printInfo(sinArray);
        int amountOfPositiveItem = MultidimensionalArraysManipulator.findAmountOfPositiveNumbers(sinArray);
        Printer.printInfo(amountOfPositiveItem);

        //24
        Printer.printInfo("24)\n");
        double[] arr = new double[]{2, 3.5, 4, 7};
        double[][] powerMatrix = MultidimensionalArraysManipulator.createMatrix(arr);
        Printer.printInfo(powerMatrix);

        //25
        Printer.printInfo("25)\n");
        powerMatrix = MultidimensionalArraysManipulator.createMatrix(size);
        Printer.printInfo(powerMatrix);

        //27
        Printer.printInfo("27)\n");
        Printer.printInfo("Enter the number of the first column [0-5]: ");
        int firstColumn = UserInput.inputInt();
        Printer.printInfo("Enter the number of the second column [0-5]: ");
        int secondColumn = UserInput.inputInt();
        MultidimensionalArraysManipulator.changeColumns(powerMatrix, firstColumn, secondColumn);
        Printer.printInfo(powerMatrix);

        //28
        Printer.printInfo("28)\n");
        double[] sumArray = MultidimensionalArraysManipulator.createArrayOfColumnSums(powerMatrix);
        Printer.printInfo(sumArray);
        double maxSum = MultidimensionalArraysManipulator.findMaxSum(powerMatrix);
        Printer.printInfo(maxSum);

        //29
        Printer.printInfo("29)\n");
        double[][] squareMatrix = new double[][]{
                {1, 2, 3, 4, 5},
                {6, -7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, -19, 20},
                {21, 22, 23, 24, 0}};
        double[] positiveNumbers = MultidimensionalArraysManipulator.findPositiveItems(squareMatrix);
        Printer.printInfo(positiveNumbers);

        //30
        Printer.printInfo("30)\n");
        int[][] certainArray = new int[10][20];
        MultidimensionalArraysManipulator.initializeArray(certainArray);
        Printer.printInfo(certainArray);
        StringBuilder result1 = MultidimensionalArraysManipulator.findRows(certainArray);
        Printer.printInfo(result1);

        //31
        Printer.printInfo("31)\n");
        MultidimensionalArraysManipulator.initializeLargeArray(certainArray);
        Printer.printInfo(certainArray);
        int amountOfTwoDigitNumbers = MultidimensionalArraysManipulator.findAmountOfTwoDigitNumbers(certainArray);
        Printer.printInfo(amountOfTwoDigitNumbers);

        //34
        Printer.printInfo("34)\n");
        int[][] specialMatrix = new int[15][15];
        MultidimensionalArraysManipulator.initializeSpecialMatrix(specialMatrix);
        Printer.printInfo(specialMatrix);

        //35
        Printer.printInfo("35)\n");
        double[][] newMatrix = new double[][]{
                {1, 2, -3, 4, 5},
                {6, 7, 8, 9, 10, -11},
                {12, 13, 115, 14},
                {15, -199, 17, 18, 19, 20},
                {21, -29}};
        MultidimensionalArraysManipulator.changeItems(newMatrix);
        Printer.printInfo(newMatrix);

        //38
        Printer.printInfo("38)\n");
        double[][] firstMatrix = new double[][]{
                {1.1, 1.1},
                {1.3, 1.4},
                {1.5, 1.6}};
        double[][] secondMatrix = new double[][]{
                {2.1, 3.1},
                {2.2, 2.4},
                {2.5, 2.6}};

        double[][] resultMatrix = MultidimensionalArraysManipulator.findSumOfMatrix(firstMatrix, secondMatrix);
        Printer.printInfo(resultMatrix);


    }
}
