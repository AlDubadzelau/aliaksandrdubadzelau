package utils;

import java.util.Random;

public class ArrayInitializer {

    private static final int MAX_NUMBER = 1000;

    public static void initializeIntArray(int[] array) {

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(MAX_NUMBER);
        }

    }
}
