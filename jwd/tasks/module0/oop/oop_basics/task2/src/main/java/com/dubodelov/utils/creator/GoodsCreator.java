package com.dubodelov.utils.creator;

import com.dubodelov.model.Payment;
import com.dubodelov.utils.Printer;

import java.math.BigDecimal;
import java.util.Scanner;

public class GoodsCreator {

    private final Scanner scan = new Scanner(System.in);
    private final static String DEFAULT_NAME_MESSAGE = "Input name: ";
    private final static String DEFAULT_COST_MESSAGE = "Input cost: ";

    public Payment.Goods create() {

        Printer.print(DEFAULT_NAME_MESSAGE);
        String name = scan.next();
        Printer.print(DEFAULT_COST_MESSAGE);
        double cost = scan.nextDouble();
        BigDecimal resultCost = new BigDecimal(cost);

        Payment.Goods goods = new Payment.Goods(name, resultCost);

        return goods;
    }
}
