package com.dubodelov.command;

import com.dubodelov.model.Payment;

public interface AppCommand {

    public void execute(Payment payment);
}
