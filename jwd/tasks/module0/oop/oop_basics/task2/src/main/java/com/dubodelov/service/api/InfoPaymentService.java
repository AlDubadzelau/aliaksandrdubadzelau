package com.dubodelov.service.api;

import com.dubodelov.model.Payment;

public interface InfoPaymentService {

    public void printInfo(Payment payment);

}
