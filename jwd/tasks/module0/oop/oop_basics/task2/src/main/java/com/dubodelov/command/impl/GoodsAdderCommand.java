package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Payment;
import com.dubodelov.service.api.PaymentService;
import com.dubodelov.utils.creator.GoodsCreator;

public class GoodsAdderCommand implements AppCommand {

    private final PaymentService paymentService;

    public GoodsAdderCommand(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Override
    public void execute(Payment payment) {

        GoodsCreator creator = new GoodsCreator();
        Payment.Goods goods = creator.create();

        paymentService.addGoods(payment, goods);
    }
}
