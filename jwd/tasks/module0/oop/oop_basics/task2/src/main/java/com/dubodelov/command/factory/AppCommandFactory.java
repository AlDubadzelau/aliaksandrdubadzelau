package com.dubodelov.command.factory;

import com.dubodelov.command.AppCommand;

public interface AppCommandFactory {

    public AppCommand getCommand(String commandName);
}
