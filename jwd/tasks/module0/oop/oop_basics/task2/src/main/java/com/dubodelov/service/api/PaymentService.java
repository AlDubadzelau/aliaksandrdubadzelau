package com.dubodelov.service.api;

import com.dubodelov.model.Payment;

public interface PaymentService {

    public void addGoods(Payment payment, Payment.Goods goods);

}
