package com.dubodelov.service.impl;

import com.dubodelov.model.Payment;
import com.dubodelov.service.api.InfoPaymentService;

import java.util.List;

public class SimpleInfoPaymentService implements InfoPaymentService {

    @Override
    public void printInfo(Payment payment) {

        List<Payment.Goods> goodsInPayment = payment.getGoods();

        for (Payment.Goods goods : goodsInPayment) {
            System.out.println(goods);
        }
    }
}
