package com.dubodelov.service.impl;

import com.dubodelov.model.Payment;
import com.dubodelov.service.api.PaymentService;

import java.util.List;

public class SimplePaymentService implements PaymentService {

    public void addGoods(Payment payment, Payment.Goods goods) {

        List<Payment.Goods> goodsInPayment = payment.getGoods();
        goodsInPayment.add(goods);

        payment.setGoods(goodsInPayment);
    }
}

