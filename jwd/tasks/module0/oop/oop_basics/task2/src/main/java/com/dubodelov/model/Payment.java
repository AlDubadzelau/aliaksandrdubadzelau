package com.dubodelov.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Payment {

    List<Goods> goods = new ArrayList<Goods>();

    public List<Goods> getGoods() {
        return goods;
    }

    public void setGoods(List<Goods> goods) {
        this.goods = goods;
    }

    public static class Goods {

        String goodsName;
        BigDecimal cost;

        public Goods(String goodsName, BigDecimal cost) {
            this.goodsName = goodsName;
            this.cost = cost;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public BigDecimal getCost() {
            return cost;
        }

        public void setCost(BigDecimal cost) {
            this.cost = cost;
        }

        @Override
        public String toString() {
            return "Name= " + goodsName +
                    ", cost= " + cost;
        }
    }
}
