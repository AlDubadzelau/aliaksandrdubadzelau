package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.AppCommandName;
import com.dubodelov.command.impl.GoodsAdderCommand;
import com.dubodelov.command.impl.PrinterInfoCommand;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.Payment;
import com.dubodelov.service.api.InfoPaymentService;
import com.dubodelov.service.api.PaymentService;
import com.dubodelov.service.impl.SimpleInfoPaymentService;
import com.dubodelov.service.impl.SimplePaymentService;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        InfoPaymentService infoPaymentService = new SimpleInfoPaymentService();
        PaymentService paymentService = new SimplePaymentService();

        AppCommand add = new GoodsAdderCommand(paymentService);
        AppCommand print = new PrinterInfoCommand(infoPaymentService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT, print);
        commands.put(AppCommandName.ADD, add);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        Payment payment = new Payment();

        while (isRunning) {
            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.ADD);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(payment, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}
