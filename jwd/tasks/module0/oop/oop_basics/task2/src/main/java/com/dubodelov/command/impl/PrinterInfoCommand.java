package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Payment;
import com.dubodelov.service.api.InfoPaymentService;

public class PrinterInfoCommand implements AppCommand {

    private final InfoPaymentService infoPaymentService;

    public PrinterInfoCommand(InfoPaymentService infoPaymentService) {
        this.infoPaymentService = infoPaymentService;
    }

    @Override
    public void execute(Payment payment) {
        infoPaymentService.printInfo(payment);
    }
}
