package com.dubodelov.controller;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.model.Treasure;

import java.util.List;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(List<Treasure> treasures, String userCommand) {

        final AppCommand command = commandFactory.getCommand(userCommand);
        command.execute(treasures);
    }

}
