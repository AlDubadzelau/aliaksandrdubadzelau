package com.dubodelov.service.api;

import com.dubodelov.model.Treasure;

import java.math.BigDecimal;
import java.util.List;

public interface TreasureService {

    public Treasure findGreatTreasure(List<Treasure> treasures);

    public List<Treasure> findCertainTreasure(BigDecimal money, List<Treasure> treasures);
}
