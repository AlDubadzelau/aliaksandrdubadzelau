package com.dubodelov.command;

import com.dubodelov.model.Treasure;

import java.util.List;

public interface AppCommand {

    public void execute(List<Treasure> treasure);

}
