package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.AppCommandName;
import com.dubodelov.command.impl.CertainTreasureFinderCommand;
import com.dubodelov.command.impl.GreatTreasureFinderCommand;
import com.dubodelov.command.impl.PrinterTreasureCommand;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.Treasure;
import com.dubodelov.service.api.TreasureInfoService;
import com.dubodelov.service.api.TreasureService;
import com.dubodelov.service.impl.SimpleTreasureInfoService;
import com.dubodelov.service.impl.SimpleTreasureService;

import java.math.BigDecimal;
import java.util.*;

public class Application {

    private final static int AMOUNT_OF_DRAGONS_TREASURE = 100;

    public static void main(String[] args) {

        TreasureInfoService treasureInfoService = new SimpleTreasureInfoService();
        TreasureService treasureService = new SimpleTreasureService();

        AppCommand print = new PrinterTreasureCommand(treasureInfoService);
        AppCommand findCertainTreasure = new CertainTreasureFinderCommand(treasureService, treasureInfoService);
        AppCommand findGreatTreasure = new GreatTreasureFinderCommand(treasureService, treasureInfoService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT, print);
        commands.put(AppCommandName.FIND_CERTAIN_TREASURE, findCertainTreasure);
        commands.put(AppCommandName.FIND_GREAT_TREASURE, findGreatTreasure);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);

        List<Treasure> treasures = new ArrayList<>();
        //
        for (int i = 0; i < AMOUNT_OF_DRAGONS_TREASURE; i++) {
            Treasure treasure = new Treasure(new BigDecimal(i));
            treasures.add(treasure);
        }
        //

        while (isRunning) {
            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.FIND_CERTAIN_TREASURE);
            System.out.println(AppCommandName.FIND_GREAT_TREASURE);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(treasures, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}
