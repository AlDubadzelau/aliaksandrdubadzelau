package com.dubodelov.service.impl;

import com.dubodelov.model.Treasure;
import com.dubodelov.service.api.TreasureInfoService;

public class SimpleTreasureInfoService implements TreasureInfoService {

    public void printInfo(Treasure treasure) {
        System.out.println(treasure);
    }
}
