package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Treasure;
import com.dubodelov.service.api.TreasureInfoService;
import com.dubodelov.service.api.TreasureService;
import com.dubodelov.util.Printer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CertainTreasureFinderCommand implements AppCommand {

    private final Scanner scan = new Scanner(System.in);
    private final TreasureService treasureService;
    private final TreasureInfoService treasureInfoService;
    private static final String DEFAULT_MESSAGE = "Input amount of money: ";

    public CertainTreasureFinderCommand(TreasureService treasureService, TreasureInfoService treasureInfoService) {
        this.treasureService = treasureService;
        this.treasureInfoService = treasureInfoService;
    }

    @Override
    public void execute(List<Treasure> treasures) {

        List<Treasure> certainTreasures = new ArrayList<>();

        Printer.print(DEFAULT_MESSAGE);
        if (scan.hasNextDouble()) {
            BigDecimal money = new BigDecimal(scan.nextDouble());
            certainTreasures.addAll(treasureService.findCertainTreasure(money, treasures));
        }

        for (Treasure treasure : certainTreasures) {
            treasureInfoService.printInfo(treasure);
        }
    }
}

