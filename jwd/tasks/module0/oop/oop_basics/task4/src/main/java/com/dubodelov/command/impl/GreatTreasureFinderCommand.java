package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Treasure;
import com.dubodelov.service.api.TreasureInfoService;
import com.dubodelov.service.api.TreasureService;

import java.util.List;

public class GreatTreasureFinderCommand implements AppCommand {

    private final TreasureInfoService treasureInfoService;
    private final TreasureService treasureService;

    public GreatTreasureFinderCommand(TreasureService treasureService, TreasureInfoService treasureInfoService) {
        this.treasureInfoService = treasureInfoService;
        this.treasureService = treasureService;
    }

    @Override
    public void execute(List<Treasure> treasures) {

        Treasure greatTreasure = treasureService.findGreatTreasure(treasures);

        treasureInfoService.printInfo(greatTreasure);

    }
}
