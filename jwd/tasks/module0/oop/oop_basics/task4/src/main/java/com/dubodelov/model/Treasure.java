package com.dubodelov.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Treasure {

    public Treasure(BigDecimal cost) {
        this.cost = cost;
    }

    public Treasure() {
    }

    private BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Treasure treasure = (Treasure) o;
        return Objects.equals(getCost(), treasure.getCost());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCost());
    }

    @Override
    public String toString() {
        return "Cost= " + cost;
    }
}
