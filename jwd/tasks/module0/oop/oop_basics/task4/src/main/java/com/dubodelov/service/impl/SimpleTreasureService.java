package com.dubodelov.service.impl;

import com.dubodelov.model.Treasure;
import com.dubodelov.service.api.TreasureService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SimpleTreasureService implements TreasureService {

    private static final int DEFAULT_ZERO_NUMBER = 0;

    public Treasure findGreatTreasure(List<Treasure> treasures) {

        Treasure greatTreasure = treasures.get(DEFAULT_ZERO_NUMBER);

        for (Treasure treasure : treasures) {
            if (greatTreasure.getCost().compareTo(treasure.getCost()) < DEFAULT_ZERO_NUMBER) {
                greatTreasure = treasure;

            }
        }

        return greatTreasure;
    }

    public List<Treasure> findCertainTreasure(BigDecimal money, List<Treasure> treasures) {

        List<Treasure> newTreasures = new ArrayList<>();

        for (Treasure treasure : treasures) {

            BigDecimal currentCost = treasure.getCost();

            if ((money.subtract(currentCost)).doubleValue() > DEFAULT_ZERO_NUMBER) {
                newTreasures.add(treasure);
                money = money.subtract(currentCost);
            }
        }

        return newTreasures;
    }
}
