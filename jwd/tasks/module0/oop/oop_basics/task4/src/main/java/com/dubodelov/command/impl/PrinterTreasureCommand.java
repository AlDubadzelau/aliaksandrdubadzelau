package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Treasure;
import com.dubodelov.service.api.TreasureInfoService;

import java.util.List;

public class PrinterTreasureCommand implements AppCommand {

    private final TreasureInfoService treasureInfoService;

    public PrinterTreasureCommand(TreasureInfoService treasureInfoService) {
        this.treasureInfoService = treasureInfoService;
    }

    @Override
    public void execute(List<Treasure> treasures) {

        for (Treasure treasure : treasures) {
            treasureInfoService.printInfo(treasure);
        }
    }
}
