package com.dubodelov.service.api;

import com.dubodelov.model.Treasure;

public interface TreasureInfoService {

    public void printInfo(Treasure treasure);
}
