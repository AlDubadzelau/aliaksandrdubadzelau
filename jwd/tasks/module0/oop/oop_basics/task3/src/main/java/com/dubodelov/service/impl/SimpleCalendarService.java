package com.dubodelov.service.impl;

import com.dubodelov.model.Calendar;
import com.dubodelov.service.api.CalendarService;
import com.dubodelov.utils.creator.DayCreator;

import java.util.List;

public class SimpleCalendarService implements CalendarService {

    @Override
    public void addDay(Calendar calendar, Calendar.Day day) {

        List<Calendar.Day> days = calendar.getDays();
        days.add(day);

        calendar.setDays(days);

    }
}
