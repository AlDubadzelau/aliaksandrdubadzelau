package com.dubodelov.service.api;

import com.dubodelov.model.Calendar;

public interface CalendarInfoService {

    public void printInfo(Calendar calendar);
}
