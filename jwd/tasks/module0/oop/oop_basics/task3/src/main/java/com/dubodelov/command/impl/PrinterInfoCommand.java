package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Calendar;
import com.dubodelov.service.api.CalendarInfoService;

public class PrinterInfoCommand implements AppCommand {

    private final CalendarInfoService calendarInfoService;

    public PrinterInfoCommand(CalendarInfoService calendarInfoService) {
        this.calendarInfoService = calendarInfoService;
    }

    @Override
    public void execute(Calendar calendar) {
        calendarInfoService.printInfo(calendar);
    }
}
