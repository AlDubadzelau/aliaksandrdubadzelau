package com.dubodelov.service.api;

import com.dubodelov.model.Calendar;

public interface CalendarService {

    public void addDay(Calendar calendar, Calendar.Day day);
}
