package com.dubodelov.utils.creator;

import com.dubodelov.model.Calendar;
import com.dubodelov.model.DayType;
import com.dubodelov.utils.Printer;

import java.util.Scanner;

public class DayCreator {

    private final Scanner scan = new Scanner(System.in);
    private static final int DEFAULT_DAY = 12;
    private static final String DEFAULT_DAY_MESSAGE = "Input number day: ";
    private static final String DEFAULT_NAME_MESSAGE = "Input name: ";

    public Calendar.Day create() {

        Printer.print(DEFAULT_NAME_MESSAGE);
        String name = scan.next();

        int dayNumber = DEFAULT_DAY;

        Printer.print(DEFAULT_DAY_MESSAGE);
        if (scan.hasNextInt()) {
            dayNumber = scan.nextInt();
        }

        DayType dayType = DayType.WORK_DAY;

        Calendar.Day day = new Calendar.Day(name, dayNumber, dayType);

        return day;

    }
}
