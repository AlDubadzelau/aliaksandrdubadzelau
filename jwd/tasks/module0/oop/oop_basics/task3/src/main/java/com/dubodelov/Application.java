package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.AdderDayCommand;
import com.dubodelov.command.impl.AppCommandName;
import com.dubodelov.command.impl.PrinterInfoCommand;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.Calendar;
import com.dubodelov.service.api.CalendarInfoService;
import com.dubodelov.service.api.CalendarService;
import com.dubodelov.service.impl.SimpleCalendarInfoService;
import com.dubodelov.service.impl.SimpleCalendarService;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        CalendarInfoService calendarInfoService = new SimpleCalendarInfoService();
        CalendarService calendarService = new SimpleCalendarService();

        AppCommand add = new AdderDayCommand(calendarService);
        AppCommand print = new PrinterInfoCommand(calendarInfoService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT, print);
        commands.put(AppCommandName.ADD, add);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        Calendar calendar = new Calendar();

        while (isRunning) {
            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.ADD);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(calendar, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}
