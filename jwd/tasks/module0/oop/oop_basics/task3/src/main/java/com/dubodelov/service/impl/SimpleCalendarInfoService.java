package com.dubodelov.service.impl;

import com.dubodelov.model.Calendar;
import com.dubodelov.service.api.CalendarInfoService;

public class SimpleCalendarInfoService implements CalendarInfoService {

    @Override
    public void printInfo(Calendar calendar) {

        for (Calendar.Day data : calendar.getDays()) {
            System.out.println(data);
        }
    }
}
