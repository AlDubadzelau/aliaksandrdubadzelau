package com.dubodelov.model;

public enum DayType {

    WORK_DAY("work day"),
    HOLIDAY("holiday");

    private final String type;

    DayType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "type= " + type;
    }
}
