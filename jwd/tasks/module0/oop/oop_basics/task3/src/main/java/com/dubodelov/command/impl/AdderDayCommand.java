package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Calendar;
import com.dubodelov.service.api.CalendarService;
import com.dubodelov.utils.creator.DayCreator;

public class AdderDayCommand implements AppCommand {

    private final CalendarService calendarService;

    public AdderDayCommand(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @Override
    public void execute(Calendar calendar) {

        DayCreator creator = new DayCreator();
        Calendar.Day day = creator.create();

        calendarService.addDay(calendar, day);
    }
}
