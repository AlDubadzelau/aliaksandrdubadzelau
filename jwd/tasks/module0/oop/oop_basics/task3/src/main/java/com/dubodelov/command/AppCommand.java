package com.dubodelov.command;

import com.dubodelov.model.Calendar;

public interface AppCommand {

    public void execute(Calendar calendar);
}
