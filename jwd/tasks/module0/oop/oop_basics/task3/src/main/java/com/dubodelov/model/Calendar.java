package com.dubodelov.model;

import java.util.ArrayList;
import java.util.List;

public class Calendar {

    private List<Day> days = new ArrayList<Day>();

    public List<Day> getDays() {
        return days;
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }

    public static class Day {

        private DayType dayType;
        private String name;
        private int day;

        public Day(String name, int day, DayType dayType) {
            this.name = name;
            this.day = day;
            this.dayType = dayType;

        }

        public String getName() {
            return name;
        }

        public int getDay() {
            return day;
        }

        public DayType getDayType() {
            return dayType;
        }

        public void setDayType(DayType dayType) {
            this.dayType = dayType;
        }

        @Override
        public String toString() {
            return dayType.toString() +
                    ", name= " + name +
                    ", date= " + day;
        }
    }
}
