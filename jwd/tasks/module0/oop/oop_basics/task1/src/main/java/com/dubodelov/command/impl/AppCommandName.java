package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;

public enum AppCommandName {

    PRINT("-p"),
    CREATE("-c"),
    DELETE("-d"),
    CHANGE_NAME("-cn"),
    ADD_CONTENT("-ad");

    private static final AppCommandName DEFAULT_VALUE = null;
    private final String shortCommand;

    AppCommandName(String shortCommand) {
        this.shortCommand = shortCommand;
    }

    public static AppCommandName fromString(String name) {

        AppCommandName command = DEFAULT_VALUE;
        final AppCommandName[] values = AppCommandName.values();

        for (AppCommandName commandName : values) {
            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
                command = commandName;
            }
        }

        return command;
    }

    @Override
    public String toString() {
        return shortCommand;
    }

}
