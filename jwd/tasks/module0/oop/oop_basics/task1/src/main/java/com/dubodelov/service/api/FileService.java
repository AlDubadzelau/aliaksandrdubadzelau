package com.dubodelov.service.api;

import com.dubodelov.model.File;

import java.util.List;

public interface FileService {

    public void renameFile(File file, String name);

    public void addContent(File file, StringBuilder content);

    public void deleteFile(File file, List<File> files);

    public File createFile();
}
