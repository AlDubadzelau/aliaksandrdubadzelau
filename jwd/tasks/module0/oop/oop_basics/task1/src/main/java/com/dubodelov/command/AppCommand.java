package com.dubodelov.command;

import com.dubodelov.model.File;

import java.util.List;

public interface AppCommand {

    public void execute(List<File> files);
}
