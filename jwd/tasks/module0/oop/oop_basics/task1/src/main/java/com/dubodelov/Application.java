package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.*;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.File;
import com.dubodelov.service.api.FileService;
import com.dubodelov.service.api.InfoFileService;
import com.dubodelov.service.impl.SimpleFileService;
import com.dubodelov.service.impl.SimpleInfoFileService;

import java.util.*;

public class Application {

    public static void main(String[] args) {

        FileService fileService = new SimpleFileService();
        InfoFileService infoFileService = new SimpleInfoFileService();

        AppCommand create = new CreatorCommand(fileService);
        AppCommand delete = new DeleterCommand(fileService);
        AppCommand redactName = new RedactorNameCommand(fileService);
        AppCommand redactContent = new RedactorContentCommand(fileService);
        AppCommand print = new PrinterInfoCommand(infoFileService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT, print);
        commands.put(AppCommandName.ADD_CONTENT, redactContent);
        commands.put(AppCommandName.CHANGE_NAME, redactName);
        commands.put(AppCommandName.CREATE, create);
        commands.put(AppCommandName.DELETE, delete);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        List<File> files = new ArrayList<>();

        while (isRunning) {
            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.CREATE);
            System.out.println(AppCommandName.DELETE);
            System.out.println(AppCommandName.CHANGE_NAME);
            System.out.println(AppCommandName.ADD_CONTENT);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(files, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }

        }

    }
}
