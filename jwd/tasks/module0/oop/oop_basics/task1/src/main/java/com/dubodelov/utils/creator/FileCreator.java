package com.dubodelov.utils.creator;

import com.dubodelov.model.Decoration;
import com.dubodelov.model.File;
import com.dubodelov.model.TextFile;
import com.dubodelov.utils.Printer;
import com.dubodelov.utils.UserInput;

public class FileCreator {

    private final static String DEFAULT_MESSAGE_NAME = "Input file name: ";
    private final static String DEFAULT_MESSAGE_CONTENT = "Input file content: ";

    public File createFile() {

        UserInput input = new UserInput();
        Printer.print(DEFAULT_MESSAGE_NAME);
        String name = input.inputString();
        Printer.print(DEFAULT_MESSAGE_CONTENT);
        String content = input.inputString();
        StringBuilder resultContent = new StringBuilder(content);
        Decoration decoration = Decoration.TEXT;

        File file = new TextFile(name, resultContent, decoration);

        return file;
    }
}
