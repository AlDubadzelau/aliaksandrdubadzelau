package com.dubodelov.model;

public enum Decoration {

    TEXT(".txt"),
    PDF(".pdf");

    private final String format;

    Decoration(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

    @Override
    public String toString() {
        return "format= " + format;
    }
}
