package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.File;
import com.dubodelov.service.api.FileService;
import com.dubodelov.utils.Printer;
import com.dubodelov.utils.UserInput;
import com.dubodelov.utils.checker.FileChecker;
import com.dubodelov.validator.FileValidator;
import com.dubodelov.validator.impl.FileValidatorImpl;

import java.util.List;

public class RedactorContentCommand implements AppCommand {

    private final static String DEFAULT_MESSAGE_NAME = "Input file name: ";
    private final static String DEFAULT_MESSAGE_CONTENT = "Input file content: ";
    private final FileService fileService;

    public RedactorContentCommand(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    public void execute(List<File> files) {

        Printer.print(DEFAULT_MESSAGE_NAME);
        UserInput input = new UserInput();
        String name = input.inputString();
        FileChecker checker = new FileChecker();
        File file = checker.findCertainFile(files, name);
        FileValidator validator = new FileValidatorImpl();
        boolean isNull = validator.isNull(file);

        if (!isNull) {

            Printer.print(DEFAULT_MESSAGE_CONTENT);
            String content = input.inputString();
            StringBuilder resultContent = new StringBuilder(content);

            fileService.addContent(file, resultContent);
        }
    }

}
