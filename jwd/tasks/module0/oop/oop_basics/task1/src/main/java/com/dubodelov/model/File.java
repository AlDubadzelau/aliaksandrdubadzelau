package com.dubodelov.model;

public class File {

    private String name;
    private StringBuilder content;

    public File(String name, StringBuilder content) {
        this.name = name;
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StringBuilder getContent() {
        return content;
    }

    public void setContent(StringBuilder content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Name= " + name + "\n" +
                "content= " + content;
    }
}
