package com.dubodelov.utils.checker;

import com.dubodelov.model.File;

import java.util.List;

public class FileChecker {

    private final File DEFAULT_VALUE = null;

    public File findCertainFile(List<File> files, String name) {

        File resultFile = DEFAULT_VALUE;

        for (File file : files) {

            String fileName = file.getName();

            if (name.equals(fileName)) {
                resultFile = file;
            }
        }

        return resultFile;
    }

}
