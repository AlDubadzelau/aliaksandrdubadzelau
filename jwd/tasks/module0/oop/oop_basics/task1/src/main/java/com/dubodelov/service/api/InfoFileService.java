package com.dubodelov.service.api;

import com.dubodelov.model.File;

public interface InfoFileService {

    public void printInfo(File file);
}
