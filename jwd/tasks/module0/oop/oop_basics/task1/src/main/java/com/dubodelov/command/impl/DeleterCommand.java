package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.File;
import com.dubodelov.service.api.FileService;
import com.dubodelov.utils.Printer;
import com.dubodelov.utils.UserInput;
import com.dubodelov.utils.checker.FileChecker;
import com.dubodelov.validator.FileValidator;
import com.dubodelov.validator.impl.FileValidatorImpl;

import java.util.List;

public class DeleterCommand implements AppCommand {

    private final static String DEFAULT_MESSAGE = "Input file name: ";
    private final FileService fileService;

    public DeleterCommand(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    public void execute(List<File> files) {

        Printer.print(DEFAULT_MESSAGE);
        UserInput input = new UserInput();
        String name = input.inputString();
        FileChecker checker = new FileChecker();
        File removeFile = checker.findCertainFile(files, name);
        FileValidator validator = new FileValidatorImpl();
        boolean isNull = validator.isNull(removeFile);

        if (!isNull) {
            fileService.deleteFile(removeFile, files);
        }
    }
}
