package com.dubodelov.validator;

import com.dubodelov.model.File;

public interface FileValidator {

    public boolean isNull(File file);
}
