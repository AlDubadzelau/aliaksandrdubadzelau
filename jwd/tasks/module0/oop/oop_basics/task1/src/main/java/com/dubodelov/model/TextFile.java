package com.dubodelov.model;

public class TextFile extends File {

    private Decoration decoration;

    public TextFile(String name, StringBuilder content, Decoration decoration) {
        super(name, content);
        this.decoration = decoration;
    }

    public Decoration getDecoration() {
        return decoration;
    }

    public void setDecoration(Decoration decoration) {
        this.decoration = decoration;
    }

    @Override
    public String toString() {
        return super.toString() + "\n" + decoration.toString();
    }
}
