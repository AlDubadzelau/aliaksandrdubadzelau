package com.dubodelov.service.impl;

import com.dubodelov.model.File;
import com.dubodelov.service.api.InfoFileService;

public class SimpleInfoFileService implements InfoFileService {

    @Override
    public void printInfo(File file) {
        System.out.println(file);
    }
}
