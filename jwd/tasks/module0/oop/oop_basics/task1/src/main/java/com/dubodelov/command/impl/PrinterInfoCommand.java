package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.File;
import com.dubodelov.service.api.InfoFileService;

import java.util.List;

public class PrinterInfoCommand implements AppCommand {

    private final InfoFileService infoFileService;

    public PrinterInfoCommand(InfoFileService infoFileService) {
        this.infoFileService = infoFileService;
    }

    @Override
    public void execute(List<File> files) {

        for (File file : files) {
            infoFileService.printInfo(file);
        }
    }
}
