package com.dubodelov.utils;

import java.util.Scanner;

public class UserInput {

    private final Scanner scan = new Scanner(System.in);

    public String inputString() {
        return scan.next();
    }
}
