package com.dubodelov.validator.impl;

import com.dubodelov.model.File;
import com.dubodelov.validator.FileValidator;

public class FileValidatorImpl implements FileValidator {

    private final static boolean DEFAULT_POSITIVE_RESULT = true;
    private final static boolean DEFAULT_NEGATIVE_RESULT = false;

    @Override
    public boolean isNull(File file) {

        boolean isNull = DEFAULT_POSITIVE_RESULT;

        if (file != null) {
            isNull = DEFAULT_NEGATIVE_RESULT;
        }

        return isNull;
    }
}
