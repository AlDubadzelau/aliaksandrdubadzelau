package com.dubodelov.service.impl;

import com.dubodelov.model.File;
import com.dubodelov.service.api.FileService;
import com.dubodelov.utils.creator.FileCreator;

import java.util.List;

public class SimpleFileService implements FileService {

    @Override
    public void renameFile(File file, String name) {
        file.setName(name);
    }

    @Override
    public void addContent(File file, StringBuilder addedContent) {

        StringBuilder oldContent = file.getContent();
        StringBuilder newContent = oldContent.append(addedContent);

        file.setContent(newContent);
    }

    @Override
    public void deleteFile(File file, List<File> files) {

        files.remove(file);

    }

    @Override
    public File createFile() {

        FileCreator creator = new FileCreator();

        return creator.createFile();
    }
}
