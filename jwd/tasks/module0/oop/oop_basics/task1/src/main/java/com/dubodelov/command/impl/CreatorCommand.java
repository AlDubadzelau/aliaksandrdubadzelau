package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.File;
import com.dubodelov.service.api.FileService;

import java.util.List;

public class CreatorCommand implements AppCommand {

    private final FileService fileService;

    public CreatorCommand(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    public void execute(List<File> files) {

        File file = fileService.createFile();

        files.add(file);
    }
}
