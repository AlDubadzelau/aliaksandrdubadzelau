package com.dubodelov.validator.impl;

import com.dubodelov.model.impl.Bouquet;
import com.dubodelov.validator.Validator;

public class BouquetValidator implements Validator {

    private final static boolean DEFAULT_POSITIVE_RESULT = true;
    private final static boolean DEFAULT_NEGATIVE_RESULT = false;

    public boolean isNull(Bouquet bouquet){

        boolean isNull = DEFAULT_NEGATIVE_RESULT;

        if(bouquet.getFlowers() == null || bouquet.getWrappers() == null){
            isNull = DEFAULT_POSITIVE_RESULT;
        }

        return isNull;
    }
}
