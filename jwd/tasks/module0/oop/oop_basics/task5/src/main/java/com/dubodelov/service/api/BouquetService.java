package com.dubodelov.service.api;

import com.dubodelov.model.AbstractFlower;
import com.dubodelov.model.impl.Bouquet;
import com.dubodelov.model.impl.Wrapper;

public interface BouquetService {

    public void addWrapper(Bouquet bouquet, Wrapper wrapper);

    public void addFlower(Bouquet bouquet, AbstractFlower flower);
}
