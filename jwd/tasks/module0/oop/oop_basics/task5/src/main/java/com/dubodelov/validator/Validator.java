package com.dubodelov.validator;

import com.dubodelov.model.impl.Bouquet;

public interface Validator {

    public boolean isNull(Bouquet bouquet);
}
