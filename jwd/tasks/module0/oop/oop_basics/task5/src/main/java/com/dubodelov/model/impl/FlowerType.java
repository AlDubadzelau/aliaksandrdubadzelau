package com.dubodelov.model.impl;

public enum FlowerType {

    ROSE("Rose"),
    BEGONIA("Begonia");

    private final String type;

    FlowerType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return " Type= " + type;
    }
}
