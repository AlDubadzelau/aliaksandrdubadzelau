package com.dubodelov.exceptions;

public class FlowerCreatorException extends Exception {

    public FlowerCreatorException(String message) {
        super(message);
    }

    public FlowerCreatorException(String message, Throwable cause) {
        super(message, cause);
    }
}
