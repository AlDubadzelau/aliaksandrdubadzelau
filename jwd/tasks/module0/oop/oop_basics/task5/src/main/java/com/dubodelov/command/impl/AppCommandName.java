package com.dubodelov.command.impl;

public enum AppCommandName {

    ADD_FLOWER("-adf"),
    ADD_WRAPPER("-adw"),
    PRINT("-p");

    private static final AppCommandName DEFAULT_VALUE = null;
    private final String shortCommand;

    AppCommandName(String shortCommand) {
        this.shortCommand = shortCommand;
    }


    public static AppCommandName fromString(String name) {

        AppCommandName command = DEFAULT_VALUE;
        final AppCommandName[] values = AppCommandName.values();

        for (AppCommandName commandName : values) {
            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
                command = commandName;
            }
        }

        return command;
    }

    @Override
    public String toString() {
        return shortCommand;
    }

}
