package com.dubodelov.model.impl;

import com.dubodelov.model.AbstractFlower;

import java.util.ArrayList;
import java.util.List;

public class Bouquet {

    private List<AbstractFlower> flowers = new ArrayList<>();
    private List<Wrapper> wrappers = new ArrayList<>();

    public void add(AbstractFlower flower) {
        flowers.add(flower);
    }

    public void add(Wrapper wrapper) {
        wrappers.add(wrapper);
    }

    public List<AbstractFlower> getFlowers() {
        return flowers;
    }

    public List<Wrapper> getWrappers() {
        return wrappers;
    }

    @Override
    public String toString() {
        return "Bouquet: " +
                flowers.toString() + "\n" +
                wrappers.toString();
    }
}
