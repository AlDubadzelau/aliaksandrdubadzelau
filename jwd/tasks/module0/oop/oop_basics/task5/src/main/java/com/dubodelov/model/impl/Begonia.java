package com.dubodelov.model.impl;

import com.dubodelov.model.AbstractFlower;

public class Begonia extends AbstractFlower {

    private FlowerType type = FlowerType.BEGONIA;
    private int amountOfBlossoms;

    public Begonia(double lengthOfStem, int amountOfBlossoms) {
        super(lengthOfStem);
        this.amountOfBlossoms = amountOfBlossoms;
    }

    @Override
    public String toString() {
        return type.toString() +
                " Amount of blossoms= " + amountOfBlossoms;
    }
}
