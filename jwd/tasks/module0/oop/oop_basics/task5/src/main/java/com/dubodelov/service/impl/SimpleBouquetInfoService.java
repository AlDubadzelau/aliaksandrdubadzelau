package com.dubodelov.service.impl;

import com.dubodelov.model.AbstractFlower;
import com.dubodelov.model.impl.Bouquet;
import com.dubodelov.model.impl.Wrapper;
import com.dubodelov.service.api.BouquetInfoService;

import java.util.List;

public class SimpleBouquetInfoService implements BouquetInfoService {

    @Override
    public void printInfo(Bouquet bouquet) {

        List<AbstractFlower> flowers = bouquet.getFlowers();
        List<Wrapper> wrappers = bouquet.getWrappers();

        printWrappers(wrappers);
        printFlowers(flowers);
    }

    private void printFlowers(List<AbstractFlower> flowers) {

        for (AbstractFlower flower : flowers) {
            System.out.println(flower);
        }

    }

    private void printWrappers(List<Wrapper> wrappers) {

        for (Wrapper wrapper : wrappers) {
            System.out.println(wrapper);
        }
    }
}
