package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.AbstractFlower;
import com.dubodelov.model.impl.Bouquet;
import com.dubodelov.model.impl.Wrapper;
import com.dubodelov.service.api.BouquetService;
import com.dubodelov.util.creator.FlowerCreator;
import com.dubodelov.util.creator.WrapperCreator;

public class AdderWrapperFlower implements AppCommand {

    private final BouquetService bouquetService;

    public AdderWrapperFlower(BouquetService bouquetService) {
        this.bouquetService = bouquetService;
    }

    @Override
    public void execute(Bouquet bouquet) {

        WrapperCreator creator = new WrapperCreator();
        Wrapper wrapper = creator.create();
        bouquetService.addWrapper(bouquet, wrapper);
    }
}
