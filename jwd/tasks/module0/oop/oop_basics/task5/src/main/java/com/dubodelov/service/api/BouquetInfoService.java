package com.dubodelov.service.api;

import com.dubodelov.model.impl.Bouquet;

public interface BouquetInfoService {

    public void printInfo(Bouquet bouquet);
}
