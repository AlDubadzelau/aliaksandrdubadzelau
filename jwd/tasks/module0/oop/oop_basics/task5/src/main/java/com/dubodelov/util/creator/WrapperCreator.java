package com.dubodelov.util.creator;

import com.dubodelov.model.impl.Wrapper;

import java.util.Scanner;

public class WrapperCreator {

    private final Scanner scan = new Scanner(System.in);
    private static final String BASKET = "-b";
    private static final String PAPER = "-p";
    private static final String STANDARD = "-s";

    public Wrapper create() {

        System.out.println(BASKET);
        System.out.println(PAPER);
        System.out.println(STANDARD);

        String choice = scan.next();
        Wrapper wrapper = Wrapper.STANDARD_WRAPPER;

        if (choice.equals(BASKET)) {
            wrapper = Wrapper.BASKET;
        } else if (choice.equals(PAPER)) {
            wrapper = Wrapper.PAPER_WRAPPER;
        }

        return wrapper;
    }
}
