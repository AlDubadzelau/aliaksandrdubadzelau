package com.dubodelov.model.impl;

public enum Wrapper {

    BASKET("basket"),
    PAPER_WRAPPER("paper wrapper"),
    STANDARD_WRAPPER("standard wrapper");

    private final String type;

    Wrapper(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return " Wrapper type= " + type;
    }
}
