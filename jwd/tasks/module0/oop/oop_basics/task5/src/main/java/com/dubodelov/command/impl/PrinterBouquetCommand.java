package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.impl.Bouquet;
import com.dubodelov.service.api.BouquetInfoService;
import com.dubodelov.validator.Validator;
import com.dubodelov.validator.impl.BouquetValidator;

public class PrinterBouquetCommand implements AppCommand {

    private final BouquetInfoService bouquetInfoService;

    public PrinterBouquetCommand(BouquetInfoService bouquetInfoService) {
        this.bouquetInfoService = bouquetInfoService;
    }

    @Override
    public void execute(Bouquet bouquet) {

        Validator validator = new BouquetValidator();
        boolean isNull = validator.isNull(bouquet);

        if (!isNull) {
            bouquetInfoService.printInfo(bouquet);
        }
    }
}
