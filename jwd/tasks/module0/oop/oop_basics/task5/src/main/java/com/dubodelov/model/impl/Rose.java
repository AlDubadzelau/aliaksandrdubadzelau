package com.dubodelov.model.impl;

import com.dubodelov.model.AbstractFlower;

public class Rose extends AbstractFlower {

    private FlowerType type = FlowerType.ROSE;
    private int amountOfSpikes;

    public Rose(double lengthOfStem, int amountOfSpikes) {
        super(lengthOfStem);
        this.amountOfSpikes = amountOfSpikes;
    }

    @Override
    public String toString() {
        return type.toString() +
                " Amount of spikes=" + amountOfSpikes;
    }
}
