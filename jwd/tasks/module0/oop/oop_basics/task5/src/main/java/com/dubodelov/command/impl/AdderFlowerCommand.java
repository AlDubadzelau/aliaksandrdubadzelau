package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.exceptions.FlowerCreatorException;
import com.dubodelov.model.AbstractFlower;
import com.dubodelov.model.impl.Bouquet;
import com.dubodelov.service.api.BouquetService;
import com.dubodelov.util.creator.FlowerCreator;

public class AdderFlowerCommand implements AppCommand {

    private final BouquetService bouquetService;

    public AdderFlowerCommand(BouquetService bouquetService) {
        this.bouquetService = bouquetService;
    }

    @Override
    public void execute(Bouquet bouquet) {

        FlowerCreator creator = new FlowerCreator();
        try {
            AbstractFlower flower = creator.create();
            bouquetService.addFlower(bouquet, flower);

        } catch (FlowerCreatorException e) {
            System.err.println(e);
        }
    }
}
