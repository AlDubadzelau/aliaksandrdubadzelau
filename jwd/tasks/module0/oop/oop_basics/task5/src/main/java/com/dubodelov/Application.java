package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.AdderFlowerCommand;
import com.dubodelov.command.impl.AdderWrapperFlower;
import com.dubodelov.command.impl.AppCommandName;
import com.dubodelov.command.impl.PrinterBouquetCommand;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.impl.Bouquet;
import com.dubodelov.service.api.BouquetInfoService;
import com.dubodelov.service.api.BouquetService;
import com.dubodelov.service.impl.SimpleBouquetInfoService;
import com.dubodelov.service.impl.SimpleBouquetService;


import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        BouquetInfoService bouquetInfoService = new SimpleBouquetInfoService();
        BouquetService bouquetService = new SimpleBouquetService();

        AppCommand addFlower = new AdderFlowerCommand(bouquetService);
        AppCommand addWrapper = new AdderWrapperFlower(bouquetService);
        AppCommand print = new PrinterBouquetCommand(bouquetInfoService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT, print);
        commands.put(AppCommandName.ADD_FLOWER, addFlower);
        commands.put(AppCommandName.ADD_WRAPPER, addWrapper);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        Bouquet bouquet = new Bouquet();

        while (isRunning) {
            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.ADD_FLOWER);
            System.out.println(AppCommandName.ADD_WRAPPER);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(bouquet, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}
