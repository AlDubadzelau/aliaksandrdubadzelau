package com.dubodelov.command;

import com.dubodelov.model.impl.Bouquet;

public interface AppCommand {

    public void execute(Bouquet bouquet);
}
