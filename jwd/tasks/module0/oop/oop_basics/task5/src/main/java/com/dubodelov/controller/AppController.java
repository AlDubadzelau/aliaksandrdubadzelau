package com.dubodelov.controller;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.model.impl.Bouquet;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(Bouquet bouquet, String userCommand) {

        final AppCommand command = commandFactory.getCommand(userCommand);
        command.execute(bouquet);
    }

}
