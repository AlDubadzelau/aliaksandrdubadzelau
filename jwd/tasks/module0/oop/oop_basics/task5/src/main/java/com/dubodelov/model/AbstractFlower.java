package com.dubodelov.model;

public abstract class AbstractFlower {

    private double lengthOfStem;
    private String type;

    public AbstractFlower(double lengthOfStem) {
        this.lengthOfStem = lengthOfStem;
    }

    public double getLengthOfStem() {
        return lengthOfStem;
    }

    public void setLengthOfStem(double lengthOfStem) {
        this.lengthOfStem = lengthOfStem;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Length of stem=" + lengthOfStem +
                " Type= " + type;
    }
}
