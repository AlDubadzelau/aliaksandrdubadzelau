package com.dubodelov.util.creator;

import com.dubodelov.exceptions.FlowerCreatorException;
import com.dubodelov.model.AbstractFlower;
import com.dubodelov.model.impl.Begonia;
import com.dubodelov.model.impl.Rose;

import java.util.Scanner;

public class FlowerCreator {

    private final Scanner scan = new Scanner(System.in);
    private static final String BEGONIA = "-b";
    private static final String ROSE = "-r";
    private static final String MESSAGE_EXCEPTION = "FlowerCreator Exception";
    private static final double DEFAULT_STEM_LENGTH = 25;
    private static final int DEFAULT_AMOUNT_OF_SPIKES = 5;
    private static final int DEFAULT_AMOUNT_OF_BLOSSOMS = 3;

    public AbstractFlower create() throws FlowerCreatorException {

        AbstractFlower flower;

        System.out.println(BEGONIA);
        System.out.println(ROSE);
        String type = scan.next();

        if (type.equals(BEGONIA)) {
            flower = new Begonia(DEFAULT_STEM_LENGTH, DEFAULT_AMOUNT_OF_BLOSSOMS);
        } else if (type.equals(ROSE)) {
            flower = new Rose(DEFAULT_STEM_LENGTH, DEFAULT_AMOUNT_OF_SPIKES);
        } else {
            throw new FlowerCreatorException(MESSAGE_EXCEPTION);
        }

        return flower;
    }
}
