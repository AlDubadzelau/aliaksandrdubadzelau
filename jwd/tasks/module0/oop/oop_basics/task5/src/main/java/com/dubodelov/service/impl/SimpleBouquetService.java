package com.dubodelov.service.impl;

import com.dubodelov.model.AbstractFlower;
import com.dubodelov.model.impl.Bouquet;
import com.dubodelov.model.impl.Wrapper;
import com.dubodelov.service.api.BouquetService;

public class SimpleBouquetService implements BouquetService {

    @Override
    public void addWrapper(Bouquet bouquet, Wrapper wrapper) {
        bouquet.add(wrapper);
    }

    @Override
    public void addFlower(Bouquet bouquet, AbstractFlower flower) {
        bouquet.add(flower);
    }
}
