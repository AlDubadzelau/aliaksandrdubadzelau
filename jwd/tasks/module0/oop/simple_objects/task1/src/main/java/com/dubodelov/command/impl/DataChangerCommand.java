package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Test1;
import com.dubodelov.service.api.Test1Service;
import com.dubodelov.util.Printer;

import java.util.Scanner;


public class DataChangerCommand implements AppCommand {

    private final Test1Service test1Service;
    private final static String DEFAULT_MESSAGE = "Input number: ";

    public DataChangerCommand(Test1Service test1Service) {
        this.test1Service = test1Service;
    }

    @Override
    public void execute(Test1 object) {

        //???????????
        Printer.print(DEFAULT_MESSAGE);
        double firstItem = inputNumber();
        Printer.print(DEFAULT_MESSAGE);
        double secondItem = inputNumber();

        test1Service.changeData(object, firstItem, secondItem);
    }

    //???????
    private double inputNumber(){

        Scanner scan = new Scanner(System.in);
        return scan.nextDouble();
    }
}
