package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Test1;
import com.dubodelov.service.api.Test1Service;

public class SumCounterCommand implements AppCommand {

    private final Test1Service sumTest1Service;

    public SumCounterCommand(Test1Service sumTest1Service) {
        this.sumTest1Service = sumTest1Service;
    }

    @Override
    public void execute(Test1 object) {

        double sum = sumTest1Service.findSum(object);
        System.out.println(sum);
    }
}
