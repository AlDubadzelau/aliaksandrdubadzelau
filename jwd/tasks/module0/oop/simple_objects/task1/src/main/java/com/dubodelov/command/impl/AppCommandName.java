package com.dubodelov.command.impl;

public enum AppCommandName {

    FIND_MAX_NUMBER("-m"),
    COUNT_SUM("-s"),
    CHANGE("-c"),
    PRINT("-p");

    private final static AppCommandName DEFAULT_VALUE = null;
    private final String shortCommand;

    AppCommandName(String shortCommand) {
        this.shortCommand = shortCommand;
    }

    public static AppCommandName fromString(String name){

        AppCommandName command = DEFAULT_VALUE;
        final AppCommandName[] values = AppCommandName.values();

        for (AppCommandName commandName : values){
            if(commandName.shortCommand.equals(name) || commandName.name().equals(name)){
                command = commandName;
            }
        }

        return command;
    }

    @Override
    public String toString() {
        return shortCommand;
    }
}
