package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Test1;
import com.dubodelov.service.api.InfoTest1Service;

public class InfoPrinterCommand implements AppCommand {

    private final InfoTest1Service infoTest1Service;

    public InfoPrinterCommand(InfoTest1Service infoTest1Service) {
        this.infoTest1Service = infoTest1Service;
    }

    @Override
    public void execute(Test1 object) {
        infoTest1Service.printInfo(object);
    }
}

