package com.dubodelov.service.impl;

import com.dubodelov.model.Test1;
import com.dubodelov.service.api.InfoTest1Service;

public class SimpleInfoTest1Service implements InfoTest1Service {

    private final static String DEFAULT_FIRST_MESSAGE = "First item = ";
    private final static String DEFAULT_SECOND_MESSAGE = "Second item = ";

    @Override
    public void printInfo(Test1 object) {

        double firstItem = object.getFirstItem();
        double secondItem = object.getSecondItem();

        System.out.println(DEFAULT_FIRST_MESSAGE + firstItem);
        System.out.println(DEFAULT_SECOND_MESSAGE + secondItem);
    }
}
