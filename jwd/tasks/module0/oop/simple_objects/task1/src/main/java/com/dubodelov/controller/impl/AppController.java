package com.dubodelov.controller.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.model.Test1;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(Test1 object, String userCommand){

        final AppCommand command = commandFactory.getCommand(userCommand);
        command.execute(object);
    }
}
