package com.dubodelov.model;

import java.util.Objects;

public class Test1 {

    private static final double DEFAULT_FIRST_ITEM = 10;
    private static final double DEFAULT_SECOND_ITEM = 5;

    private double firstItem = DEFAULT_FIRST_ITEM;
    private double secondItem = DEFAULT_SECOND_ITEM;

    public double getFirstItem() {
        return firstItem;
    }

    public void setFirstItem(double firstItem) {
        this.firstItem = firstItem;
    }

    public double getSecondItem() {
        return secondItem;
    }

    public void setSecondItem(double secondItem) {
        this.secondItem = secondItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test1 test1 = (Test1) o;
        return Double.compare(test1.getFirstItem(), getFirstItem()) == 0 &&
                Double.compare(test1.getSecondItem(), getSecondItem()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstItem(), getSecondItem());
    }
}
