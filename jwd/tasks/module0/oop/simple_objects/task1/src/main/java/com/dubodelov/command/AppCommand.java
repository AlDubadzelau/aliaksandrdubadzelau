package com.dubodelov.command;

import com.dubodelov.model.Test1;

public interface AppCommand {

    void execute(Test1 object);
}
