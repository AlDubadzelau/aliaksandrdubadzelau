package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Test1;
import com.dubodelov.service.api.Test1Service;

public class MaxCheckerCommand implements AppCommand {

    private final Test1Service maxTest1Service;

    public MaxCheckerCommand(Test1Service maxTest1Service) {
        this.maxTest1Service = maxTest1Service;
    }

    @Override
    public void execute(Test1 object) {

        double maxNumber = maxTest1Service.findMaxNumber(object);
        System.out.println(maxNumber);
    }
}
