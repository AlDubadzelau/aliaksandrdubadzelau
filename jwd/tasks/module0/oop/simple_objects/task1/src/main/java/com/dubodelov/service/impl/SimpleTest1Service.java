package com.dubodelov.service.impl;

import com.dubodelov.model.Test1;
import com.dubodelov.service.api.Test1Service;

public class SimpleTest1Service implements Test1Service {

    @Override
    public void changeData(Test1 object, double firstItem, double secondItem) {
        object.setFirstItem(firstItem);
        object.setSecondItem(secondItem);
    }

    @Override
    public double findMaxNumber(Test1 object) {

        double firstItem = object.getFirstItem();
        double secondItem = object.getSecondItem();

        return Math.max(firstItem, secondItem);
    }

    @Override
    public double findSum(Test1 object) {

        double firstItem = object.getFirstItem();
        double secondItem = object.getSecondItem();

        return firstItem + secondItem;
    }
}
