package com.dubodelov;

import com.dubodelov.command.*;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impls.SimpleAppCommandFactory;
import com.dubodelov.command.impl.*;
import com.dubodelov.controller.impl.*;
import com.dubodelov.model.Test1;
import com.dubodelov.service.api.Test1Service;
import com.dubodelov.service.api.InfoTest1Service;
import com.dubodelov.service.impl.SimpleTest1Service;
import com.dubodelov.service.impl.SimpleInfoTest1Service;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {

        Test1Service test1Service = new SimpleTest1Service();
        InfoTest1Service infoTest1Service = new SimpleInfoTest1Service();
        Test1Service maxTest1Service = new SimpleTest1Service();
        Test1Service sumTest1Service = new SimpleTest1Service();

        AppCommand changeCommand = new DataChangerCommand(test1Service);
        AppCommand printCommand = new InfoPrinterCommand(infoTest1Service);
        AppCommand findMaxCommand = new MaxCheckerCommand(maxTest1Service);
        AppCommand countSumCommand = new SumCounterCommand(sumTest1Service);

        Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
        commands.put(AppCommandName.CHANGE, changeCommand);
        commands.put(AppCommandName.PRINT, printCommand);
        commands.put(AppCommandName.COUNT_SUM, countSumCommand);
        commands.put(AppCommandName.FIND_MAX_NUMBER, findMaxCommand);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        Test1 object = new Test1();

        while (isRunning) {

            System.out.println(AppCommandName.CHANGE);
            System.out.println(AppCommandName.COUNT_SUM);
            System.out.println(AppCommandName.FIND_MAX_NUMBER);
            System.out.println(AppCommandName.PRINT);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(object, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}

