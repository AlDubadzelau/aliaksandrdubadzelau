package com.dubodelov.service.api;

import com.dubodelov.model.Test1;

public interface Test1Service {

    void changeData(Test1 object, double firstItem, double secondItem);
    double findMaxNumber(Test1 object);
    double findSum(Test1 object);
}
