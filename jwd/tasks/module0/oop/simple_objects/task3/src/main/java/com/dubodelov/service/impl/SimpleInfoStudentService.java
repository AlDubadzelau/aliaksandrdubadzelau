package com.dubodelov.service.impl;

import com.dubodelov.model.Student;
import com.dubodelov.service.api.InfoStudentService;

import java.util.Arrays;
import java.util.List;

public class SimpleInfoStudentService implements InfoStudentService {

    private final static String DEFAULT_DIVIDER = " ";

    @Override
    public void printInfo(List<Student> students) {

        for (Student student : students) {
            System.out.println(student.getFIO() + DEFAULT_DIVIDER + student.getNumberOfGroup() + DEFAULT_DIVIDER + Arrays.toString(student.getMarks()));
        }
    }
}
