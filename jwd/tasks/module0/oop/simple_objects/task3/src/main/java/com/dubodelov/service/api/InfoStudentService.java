package com.dubodelov.service.api;

import com.dubodelov.model.Student;

import java.util.List;

public interface InfoStudentService {

    void printInfo(List<Student> students);
}
