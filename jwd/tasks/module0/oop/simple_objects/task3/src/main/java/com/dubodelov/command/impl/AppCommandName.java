package com.dubodelov.command.impl;

public enum AppCommandName {

    PRINT("-p"),
    FIND_EXCELLENT_STUDENTS("-f");

    private final static AppCommandName DEFAULT_VALUE = null;
    private final String shortCommand;

    AppCommandName(String shortCommnad) {
        this.shortCommand = shortCommnad;
    }

    public static AppCommandName fromString(String name) {

        AppCommandName command = DEFAULT_VALUE;
        final AppCommandName[] values = AppCommandName.values();

        for (AppCommandName commandName : values) {
            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
                command = commandName;
            }
        }

        return command;
    }

    @Override
    public String toString() {
        return shortCommand;
    }
}
