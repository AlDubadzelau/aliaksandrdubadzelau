package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Student;
import com.dubodelov.service.api.InfoStudentService;
import com.dubodelov.service.api.StudentService;

import java.util.List;

public class ExcellentStudentCheckerCommand implements AppCommand {

    private final StudentService studentService;
    private final InfoStudentService infoStudentService;

    public ExcellentStudentCheckerCommand(StudentService studentService, InfoStudentService infoStudentService) {
        this.studentService = studentService;
        this.infoStudentService = infoStudentService;
    }

    @Override
    public void execute(List<Student> students) {

        List<Student> excellentStudents = studentService.findStudents(students);
        infoStudentService.printInfo(excellentStudents);
    }
}
