package com.dubodelov;

import com.dubodelov.command.*;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.AppCommandName;
import com.dubodelov.command.impl.ExcellentStudentCheckerCommand;
import com.dubodelov.command.impl.InfoPrinterCommand;
import com.dubodelov.controller.impl.*;
import com.dubodelov.creator.StudentsCreator;
import com.dubodelov.model.Student;
import com.dubodelov.service.api.InfoStudentService;
import com.dubodelov.service.api.StudentService;
import com.dubodelov.service.impl.ExcellentStudentService;
import com.dubodelov.service.impl.SimpleInfoStudentService;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        InfoStudentService infoStudentService = new SimpleInfoStudentService();
        StudentService studentService = new ExcellentStudentService();

        AppCommand printCommand = new InfoPrinterCommand(infoStudentService);
        AppCommand checkExcellentStudents = new ExcellentStudentCheckerCommand(studentService, infoStudentService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT, printCommand);
        commands.put(AppCommandName.FIND_EXCELLENT_STUDENTS, checkExcellentStudents);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        StudentsCreator creator = new StudentsCreator();
        List<Student> students = creator.createStudentsList();

        while (isRunning) {
            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.FIND_EXCELLENT_STUDENTS);
            System.out.println("-q");
            System.out.println("Enter command: ");
            String command = scan.next();

            controller.handleUserData(students, command);

            if (command.equals("-q")) {
                isRunning = false;
            }
        }
    }
}
