package com.dubodelov.service.api;

import com.dubodelov.model.Student;

import java.util.List;

public interface StudentService {

    public List<Student> findStudents(List<Student> students);
}
