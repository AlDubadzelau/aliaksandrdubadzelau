package com.dubodelov.service.impl;

import com.dubodelov.model.Student;
import com.dubodelov.service.api.StudentService;

import java.util.ArrayList;
import java.util.List;

public class ExcellentStudentService implements StudentService {

    private final static int DEFAULT_EXCELLENT_MARK = 9;
    private final static boolean DEFAULT_NEGATIVE_VALUE = false;
    private final static boolean DEFAULT_POSITIVE_VALUE = true;

    @Override
    public List<Student> findStudents(List<Student> students) {

        List<Student> excellentStudents = new ArrayList<Student>();

        for (Student student : students) {

            boolean isExcellentStudent = DEFAULT_POSITIVE_VALUE;
            int[] marks = student.getMarks();

            for (int mark : marks) {
                if (mark < DEFAULT_EXCELLENT_MARK) {
                    isExcellentStudent = DEFAULT_NEGATIVE_VALUE;
                    break;
                }
            }

            if (isExcellentStudent) {
                excellentStudents.add(student);
            }
        }

        return excellentStudents;
    }
}
