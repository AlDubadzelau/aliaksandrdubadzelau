package com.dubodelov.model;

public class Student {

    private String FIO;
    private int numberOfGroup;
    private int[] marks;

    public Student(String FIO, int numberOfGroup, int[] marks) {
        this.FIO = FIO;
        this.numberOfGroup = numberOfGroup;
        this.marks = marks;
    }

    public String getFIO() {
        return FIO;
    }

    protected void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public int getNumberOfGroup() {
        return numberOfGroup;
    }

    public void setNumberOfGroup(int numberOfGroup) {
        this.numberOfGroup = numberOfGroup;
    }

    public int[] getMarks() {
        return marks;
    }

    public void setMarks(int[] marks) {
        this.marks = marks;
    }
}
