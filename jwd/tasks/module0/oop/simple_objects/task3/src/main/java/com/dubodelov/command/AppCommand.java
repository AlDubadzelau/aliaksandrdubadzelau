package com.dubodelov.command;

import com.dubodelov.model.Student;

import java.util.List;

public interface AppCommand {

    void execute(List<Student> students);
}
