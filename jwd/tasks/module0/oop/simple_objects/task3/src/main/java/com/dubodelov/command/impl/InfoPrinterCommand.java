package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Student;
import com.dubodelov.service.api.InfoStudentService;

import java.util.List;

public class InfoPrinterCommand implements AppCommand {

    private final InfoStudentService infoStudentService;

    public InfoPrinterCommand(InfoStudentService infoStudentService) {
        this.infoStudentService = infoStudentService;
    }

    @Override
    public void execute(List<Student> students) {
        infoStudentService.printInfo(students);
    }
}
