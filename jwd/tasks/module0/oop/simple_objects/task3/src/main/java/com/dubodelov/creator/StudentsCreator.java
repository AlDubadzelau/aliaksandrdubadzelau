package com.dubodelov.creator;

import com.dubodelov.model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentsCreator {

    private final static int DEFAULT_AMOUNT_OF_STUDENTS = 10;
    private final String[] DEFAULT_FIO = new String[]
            {"Dvik A.V.", "Serguk D.L.", "Ganzer A.K.",
                    "Lovir L.T.", "Mrotos B.B.", "Mrotos I.B.",
                    "Drand K.H.", "Remington V.A.", "Lebran I.I.",
                    "Savalgik J.R.",};
    private final int[] DEFAULT_GROUP_NUMBERS = new int[]
            {10701118, 10701118,
                    10701118, 10801123,
                    10801223, 11104119,
                    10901119, 10901118,
                    10701218, 10703218};
    private final int[][] DEFAULT_MARKS_SET = new int[][]
            {{4, 5, 6, 7, 8}, {6, 6, 6, 7, 8},
                    {9, 9, 6, 5, 9}, {9, 9, 10, 10, 9},
                    {7, 6, 5, 6, 6}, {7, 7, 7, 8, 8},
                    {8, 8, 9, 9, 9}, {10, 10, 10, 10, 10},
                    {9, 9, 8, 9, 10}, {4, 4, 5, 5, 4}};

    public List<Student> createStudentsList() {

        List<Student> students = new ArrayList<>();

        for (int i = 0; i < DEFAULT_AMOUNT_OF_STUDENTS; i++) {

            String FIO = createFIO(i);
            int numberOfGroup = createGroupsNumber(i);
            int[] marks = createMarks(i);
            Student student = new Student(FIO, numberOfGroup, marks);

            students.add(student);
        }

        return students;
    }

    private String createFIO(int number) {

        return DEFAULT_FIO[number];
    }

    private int createGroupsNumber(int number) {

        return DEFAULT_GROUP_NUMBERS[number];
    }

    private int[] createMarks(int number) {

        return DEFAULT_MARKS_SET[number];
    }
}
