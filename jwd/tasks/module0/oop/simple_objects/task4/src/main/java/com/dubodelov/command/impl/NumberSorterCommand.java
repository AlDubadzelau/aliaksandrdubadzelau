package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Train;
import com.dubodelov.service.api.SortTrainsService;

import java.util.List;

public class NumberSorterCommand implements AppCommand {

    private final SortTrainsService sortTrainsService;

    public NumberSorterCommand(SortTrainsService sortTrainsService) {
        this.sortTrainsService = sortTrainsService;
    }

    @Override
    public void execute(List<Train> trains) {
        sortTrainsService.sortNumbers(trains);
    }
}
