package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Train;
import com.dubodelov.service.api.InfoTrainService;

import java.util.List;

public class InfoPrinterCommand implements AppCommand {

    private final InfoTrainService infoTrainService;

    public InfoPrinterCommand(InfoTrainService infoTrainService) {
        this.infoTrainService = infoTrainService;
    }

    @Override
    public void execute(List<Train> trains) {
        infoTrainService.printTrains(trains);
    }
}
