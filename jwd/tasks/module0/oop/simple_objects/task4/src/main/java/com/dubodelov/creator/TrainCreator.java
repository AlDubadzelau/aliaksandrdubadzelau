package com.dubodelov.creator;

import com.dubodelov.model.Train;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TrainCreator {

    private final static int DEFAULT_AMOUNT_OF_TRAINS = 5;
    private final static String[] DEFAULT_DESTINATIONS = new String[]{"Lviv", "Dnepr", "Babruisk", "Lviv", "Minsk"};
    private final static int[] DEFAULT_NUMBERS = new int[]{921, 754, 444, 999, 874};
    private final static Date[] DEFAULT_DATES = new Date[]{
            new Date(1582896245627L),
            new Date(1582896240000L),
            new Date(1582896270000L),
            new Date(1582896249000L),
            new Date(1582896290000L),
    };

    public List<Train> createTrains() {

        List<Train> trains = new ArrayList<>();

        for (int i = 0; i < DEFAULT_AMOUNT_OF_TRAINS; i++) {

            String destination = createDestination(i);
            int number = createNumber(i);
            Date date = createDate(i);

            Train train = new Train(destination, number, date);

            trains.add(train);
        }

        return trains;
    }

    private String createDestination(int number) {
        return DEFAULT_DESTINATIONS[number];
    }

    private int createNumber(int number) {
        return DEFAULT_NUMBERS[number];
    }

    private Date createDate(int number) {
        return DEFAULT_DATES[number];
    }
}
