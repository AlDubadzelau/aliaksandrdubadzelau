package com.dubodelov.model;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

public class Train implements Comparable<Train> {

    private String destination;
    private int number;
    private Date time;

    public Train(String destination, int number, Date time) {
        this.destination = destination;
        this.number = number;
        this.time = time;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public int compareTo(Train o) {
        return o.getNumber() - this.getNumber();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Train train = (Train) o;
        return getNumber() == train.getNumber() &&
                getDestination().equals(train.getDestination()) &&
                getTime().equals(train.getTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDestination(), getNumber(), getTime());
    }


    @Override
    public String toString() {
        return "Train " +
                "destination= " + destination +
                ", number= " + number +
                ", time= " + time;
    }
}
