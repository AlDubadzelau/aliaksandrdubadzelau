package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Train;
import com.dubodelov.service.api.InfoTrainService;
import com.dubodelov.util.Printer;

import java.util.List;
import java.util.Scanner;

public class CertainInfoPrinterCommand implements AppCommand {

    private final Scanner scan = new Scanner(System.in);
    private final InfoTrainService certainInfoTrainService;
    private final static String DEFAULT_MESSAGE = "Input train number: ";

    public CertainInfoPrinterCommand(InfoTrainService certainInfoTrainService) {
        this.certainInfoTrainService = certainInfoTrainService;
    }

    @Override
    public void execute(List<Train> trains) {

        Printer.print(DEFAULT_MESSAGE);
        int number = scan.nextInt();

        certainInfoTrainService.printCertainTrains(trains, number);
    }
}
