package com.dubodelov;

import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.*;
import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.controller.impl.*;
import com.dubodelov.creator.TrainCreator;
import com.dubodelov.model.Train;
import com.dubodelov.service.api.InfoTrainService;
import com.dubodelov.service.api.SortTrainsService;
import com.dubodelov.service.impl.SimpleSortTrainsService;
import com.dubodelov.service.impl.SimpleInfoTrainService;

import java.util.*;

public class Application {

    public static void main(String[] args) {

        InfoTrainService certainInfoTrainService = new SimpleInfoTrainService();
        InfoTrainService infoTrainService = new SimpleInfoTrainService();
        SortTrainsService sortTrainsServiceNumber = new SimpleSortTrainsService();
        SortTrainsService sortTrainsServiceDestination = new SimpleSortTrainsService();

        AppCommand sortDestination = new DestinationSorterCommand(sortTrainsServiceDestination);
        AppCommand sortNumber = new NumberSorterCommand(sortTrainsServiceNumber);
        AppCommand printInfo = new InfoPrinterCommand(infoTrainService);
        AppCommand printCertainInfo = new CertainInfoPrinterCommand(certainInfoTrainService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.DESTINATION_SORT, sortDestination);
        commands.put(AppCommandName.NUMBER_SORT, sortNumber);
        commands.put(AppCommandName.PRINT_CERTAIN_INFO, printCertainInfo);
        commands.put(AppCommandName.PRINT_INFO, printInfo);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        TrainCreator creator = new TrainCreator();
        List<Train> trains = creator.createTrains();

        while (isRunning){

            System.out.println(AppCommandName.NUMBER_SORT);
            System.out.println(AppCommandName.DESTINATION_SORT);
            System.out.println(AppCommandName.PRINT_CERTAIN_INFO);
            System.out.println(AppCommandName.PRINT_INFO);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(trains, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}
