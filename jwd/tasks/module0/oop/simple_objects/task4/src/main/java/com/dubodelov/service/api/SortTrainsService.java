package com.dubodelov.service.api;

import com.dubodelov.model.Train;

import java.util.List;

public interface SortTrainsService {

    public void sortNumbers(List<Train> trains);
    public void sortDestinations(List<Train> trains);
}
