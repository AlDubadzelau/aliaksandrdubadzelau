package com.dubodelov.service.api;

import com.dubodelov.model.Train;

import java.util.List;

public interface InfoTrainService {

    public void printTrains(List<Train> trains);

    public void printCertainTrains(List<Train> trains, int number);
}
