package com.dubodelov.controller.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.model.Train;

import java.util.List;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(List<Train> trains, String userCommand){

        final AppCommand command = commandFactory.getCommand(userCommand);
        command.execute(trains);
    }
}

