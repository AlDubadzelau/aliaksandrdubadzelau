package com.dubodelov.comparator;

import com.dubodelov.model.Train;

import java.util.Comparator;

public class TrainDestinationComparator implements Comparator<Train> {

    private final static int DEFAULT_EQUAL_ITEMS = 0;

    @Override
    public int compare(Train o1, Train o2) {

        int result = o1.getDestination().compareTo(o2.getDestination());

        if (result == DEFAULT_EQUAL_ITEMS) {
            result = o1.getTime().compareTo(o2.getTime());
        }

        return result;
    }
}
