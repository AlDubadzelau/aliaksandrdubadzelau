package com.dubodelov.service.impl;

import com.dubodelov.comparator.TrainDestinationComparator;
import com.dubodelov.model.Train;
import com.dubodelov.service.api.SortTrainsService;

import java.util.Collections;
import java.util.List;

public class SimpleSortTrainsService implements SortTrainsService {

    public void sortNumbers(List<Train> trains) {
        Collections.sort(trains);
    }

    @Override
    public void sortDestinations(List<Train> trains) {

        TrainDestinationComparator trainDestinationComparator = new TrainDestinationComparator();
        trains.sort(trainDestinationComparator);
    }
}
