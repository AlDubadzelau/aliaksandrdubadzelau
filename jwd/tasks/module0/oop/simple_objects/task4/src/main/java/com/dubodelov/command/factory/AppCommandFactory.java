package com.dubodelov.command.factory;

import com.dubodelov.command.AppCommand;

public interface AppCommandFactory {

    AppCommand getCommand(String commandName);

}
