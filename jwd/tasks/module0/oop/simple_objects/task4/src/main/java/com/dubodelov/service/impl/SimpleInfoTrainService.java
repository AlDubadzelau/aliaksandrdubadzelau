package com.dubodelov.service.impl;

import com.dubodelov.model.Train;
import com.dubodelov.service.api.InfoTrainService;

import java.util.List;

public class SimpleInfoTrainService implements InfoTrainService {

    @Override
    public void printTrains(List<Train> trains) {
        for (Train train : trains) {
            System.out.println(train);
        }
    }

    @Override
    public void printCertainTrains(List<Train> trains, int number) {

        for (Train train : trains) {
            if (train.getNumber() == number) {
                System.out.println(train);
            }
        }
    }
}
