package com.dubodelov.command.impl;

public enum AppCommandName {

    NUMBER_SORT("-ns"),
    DESTINATION_SORT("-ds"),
    PRINT_INFO("-p"),
    PRINT_CERTAIN_INFO("-cp");

    private final static AppCommandName DEFAULT_VALUE = null;
    private final String shortCommand;

    AppCommandName(String shortCommand) {
        this.shortCommand = shortCommand;
    }

    public static AppCommandName fromString(String name) {

        AppCommandName command = DEFAULT_VALUE;
        final AppCommandName[] values = AppCommandName.values();

        for (AppCommandName commandName : values) {
            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
                command = commandName;
            }
        }

        return command;
    }

    @Override
    public String toString() {
        return shortCommand;
    }
}
