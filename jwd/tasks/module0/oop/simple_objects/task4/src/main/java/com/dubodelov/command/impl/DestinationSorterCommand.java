package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Train;
import com.dubodelov.service.api.SortTrainsService;

import java.util.List;

public class DestinationSorterCommand implements AppCommand {

    private final SortTrainsService sortTrainsService;

    public DestinationSorterCommand(SortTrainsService sortTrainsService) {
        this.sortTrainsService = sortTrainsService;
    }

    @Override
    public void execute(List<Train> trains) {
        sortTrainsService.sortDestinations(trains);
    }
}
