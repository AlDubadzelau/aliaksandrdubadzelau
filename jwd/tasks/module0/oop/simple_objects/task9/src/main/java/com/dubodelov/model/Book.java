package com.dubodelov.model;


import java.math.BigDecimal;

public class Book {

    private String name;
    private String author;
    private String publisher;
    private String cover;
    private int id;
    private int publishedYear;
    private int amountOfPages;
    private BigDecimal cost;

    public Book(String name, String author, String publisher, int id, int publishedYear, int amountOfPages, BigDecimal cost) {
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.id = id;
        this.publishedYear = publishedYear;
        this.amountOfPages = amountOfPages;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(int publishedYear) {
        this.publishedYear = publishedYear;
    }

    public int getAmountOfPages() {
        return amountOfPages;
    }

    public void setAmountOfPages(int amountOfPages) {
        this.amountOfPages = amountOfPages;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Name= " + name +
                ", author= " + author +
                ", publishedYear= " + publishedYear +
                ", publisher= " + publisher +
                ", cost= " + cost;
    }
}
