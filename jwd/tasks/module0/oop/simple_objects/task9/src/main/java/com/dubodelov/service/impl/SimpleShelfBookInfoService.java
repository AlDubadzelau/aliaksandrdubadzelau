package com.dubodelov.service.impl;

import com.dubodelov.model.Book;
import com.dubodelov.model.BookShelf;
import com.dubodelov.service.api.ShelfBookInfoService;

public class SimpleShelfBookInfoService implements ShelfBookInfoService {

    @Override
    public void printInfo(BookShelf shelf) {

        for (Book book : shelf.getBooks()) {
            System.out.println(book);
        }
    }

    @Override
    public void printAuthorBooks(BookShelf shelf) {

        for (Book book : shelf.getCertainAuthorBooks()) {
            System.out.println(book);
        }
    }

    @Override
    public void printPublisherBooks(BookShelf shelf) {

        for (Book book : shelf.getCertainPublisherBooks()) {
            System.out.println(book);
        }
    }

    @Override
    public void printCertainYearBooks(BookShelf shelf) {

        for (Book book : shelf.getCertainYearBooks()) {
            System.out.println(book);
        }
    }
}
