package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.BookShelf;
import com.dubodelov.service.api.ShelfBookInfoService;

public class PrinterAuthorBookCommand implements AppCommand {

    private final ShelfBookInfoService shelfBookInfoService;

    public PrinterAuthorBookCommand(ShelfBookInfoService shelfBookInfoService) {
        this.shelfBookInfoService = shelfBookInfoService;
    }

    @Override
    public void execute(BookShelf shelf) {
        shelfBookInfoService.printAuthorBooks(shelf);
    }
}
