package com.dubodelov.service.impl;

import com.dubodelov.model.Book;
import com.dubodelov.model.BookShelf;
import com.dubodelov.service.api.ShelfBookService;

import java.util.ArrayList;
import java.util.List;

public class SimpleShelfBookService implements ShelfBookService {

    @Override
    public void findAuthorBooks(BookShelf shelf, String author) {

        List<Book> books = shelf.getBooks();
        List<Book> authorBooks = new ArrayList<Book>();

        for (Book book : books) {

            String currentAuthor = book.getAuthor();

            if (currentAuthor.equals(author)) {
                authorBooks.add(book);
            }
        }

        shelf.setCertainAuthorBooks(authorBooks);

    }

    @Override
    public void findPublisherBooks(BookShelf shelf, String publisher) {

        List<Book> books = shelf.getBooks();
        List<Book> publisherBooks = new ArrayList<Book>();

        for (Book book : books) {

            String currentPublisher = book.getPublisher();

            if (currentPublisher.equals(publisher)) {
                publisherBooks.add(book);
            }
        }

        shelf.setCertainPublisherBooks(publisherBooks);

    }

    @Override
    public void findBooksPublishedInYear(BookShelf shelf, int year) {

        List<Book> books = shelf.getBooks();
        List<Book> newBooks = new ArrayList<Book>();

        for (Book book : books) {

            int currentYear = book.getPublishedYear();

            if (currentYear > year) {
                newBooks.add(book);
            }
        }

        shelf.setCertainYearBooks(newBooks);

    }
}
