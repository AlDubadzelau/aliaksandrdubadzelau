package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.BookShelf;
import com.dubodelov.service.api.ShelfBookInfoService;

public class PrinterCertainYearBookCommand implements AppCommand {

    private final ShelfBookInfoService shelfBookInfoService;

    public PrinterCertainYearBookCommand(ShelfBookInfoService shelfBookInfoService) {
        this.shelfBookInfoService = shelfBookInfoService;
    }

    @Override
    public void execute(BookShelf shelf) {
        shelfBookInfoService.printCertainYearBooks(shelf);
    }
}
