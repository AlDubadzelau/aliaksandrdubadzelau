package com.dubodelov.model;

import java.util.ArrayList;
import java.util.List;

public class BookShelf {

    private List<Book> books;
    private List<Book> certainAuthorBooks = new ArrayList<Book>();
    private List<Book> certainPublisherBooks = new ArrayList<Book>();
    private List<Book> certainYearBooks = new ArrayList<Book>();

    public BookShelf(List<Book> books) {
        this.books = books;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public List<Book> getCertainAuthorBooks() {
        return certainAuthorBooks;
    }

    public void setCertainAuthorBooks(List<Book> certainAuthorBooks) {
        this.certainAuthorBooks = certainAuthorBooks;
    }

    public List<Book> getCertainPublisherBooks() {
        return certainPublisherBooks;
    }

    public void setCertainPublisherBooks(List<Book> certainPublisherBooks) {
        this.certainPublisherBooks = certainPublisherBooks;
    }

    public List<Book> getCertainYearBooks() {
        return certainYearBooks;
    }

    public void setCertainYearBooks(List<Book> certainYearBooks) {
        this.certainYearBooks = certainYearBooks;
    }
}
