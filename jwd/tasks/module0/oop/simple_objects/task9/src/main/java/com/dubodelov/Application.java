package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.*;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.Book;
import com.dubodelov.model.BookShelf;
import com.dubodelov.service.api.ShelfBookInfoService;
import com.dubodelov.service.api.ShelfBookService;
import com.dubodelov.service.impl.SimpleShelfBookInfoService;
import com.dubodelov.service.impl.SimpleShelfBookService;
import com.dubodelov.utils.creator.BookCreator;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        ShelfBookInfoService shelfBookInfoService = new SimpleShelfBookInfoService();
        ShelfBookService shelfBookService = new SimpleShelfBookService();

        AppCommand determinateAuthor = new AuthorDeterminationCommand(shelfBookService);
        AppCommand determinatePublisher = new PublisherDeterminationCommand(shelfBookService);
        AppCommand determinateYear = new YearDeterminationCommand(shelfBookService);
        AppCommand printInfo = new PrinterCommand(shelfBookInfoService);
        AppCommand printPublisherBooks = new PrinterPublisherBookCommand(shelfBookInfoService);
        AppCommand printAuthorBooks = new PrinterAuthorBookCommand(shelfBookInfoService);
        AppCommand printCertainYearBooks = new PrinterCertainYearBookCommand(shelfBookInfoService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT, printInfo);
        commands.put(AppCommandName.PRINT_AUTHORS_BOOKS, printAuthorBooks);
        commands.put(AppCommandName.PRINT_CERTAIN_YEAR_BOOKS, printCertainYearBooks);
        commands.put(AppCommandName.PRINT_PUBLISHERS_BOOKS, printPublisherBooks);
        commands.put(AppCommandName.DETERMINE_AUTHOR_BOOKS, determinateAuthor);
        commands.put(AppCommandName.DETERMINE_PUBLISHER_BOOKS, determinatePublisher);
        commands.put(AppCommandName.DETERMINE_YEAR_BOOKS, determinateYear);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        BookCreator creator = new BookCreator();
        List<Book> books = creator.createBooks();
        BookShelf shelf = new BookShelf(books);

        while (isRunning) {
            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.PRINT_AUTHORS_BOOKS);
            System.out.println(AppCommandName.PRINT_CERTAIN_YEAR_BOOKS);
            System.out.println(AppCommandName.PRINT_PUBLISHERS_BOOKS);
            System.out.println(AppCommandName.DETERMINE_AUTHOR_BOOKS);
            System.out.println(AppCommandName.DETERMINE_PUBLISHER_BOOKS);
            System.out.println(AppCommandName.DETERMINE_YEAR_BOOKS);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(shelf, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }

        }
    }
}
