package com.dubodelov.command.impl;

public enum AppCommandName {

    PRINT("-p"),
    PRINT_AUTHORS_BOOKS("-pa"),
    PRINT_PUBLISHERS_BOOKS("-pp"),
    PRINT_CERTAIN_YEAR_BOOKS("-py"),
    DETERMINE_AUTHOR_BOOKS("-da"),
    DETERMINE_PUBLISHER_BOOKS("-dp"),
    DETERMINE_YEAR_BOOKS("-dy");

    private static final AppCommandName DEFAULT_VALUE = null;
    private final String shortCommand;

    AppCommandName(String shortCommand) {
        this.shortCommand = shortCommand;
    }

    public static AppCommandName fromString(String name) {

        AppCommandName command = DEFAULT_VALUE;
        final AppCommandName[] values = AppCommandName.values();

        for (AppCommandName commandName : values) {
            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
                command = commandName;
            }
        }

        return command;
    }

    @Override
    public String toString() {
        return shortCommand;
    }
}

