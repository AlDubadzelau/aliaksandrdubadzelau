package com.dubodelov.command;

import com.dubodelov.model.BookShelf;

public interface AppCommand {

    public void execute(BookShelf shelf);

}
