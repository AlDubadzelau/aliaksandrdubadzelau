package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.BookShelf;
import com.dubodelov.service.api.ShelfBookService;
import com.dubodelov.utils.Printer;

import java.util.Scanner;

public class AuthorDeterminationCommand implements AppCommand {

    private final ShelfBookService shelfBookService;
    private final Scanner scan = new Scanner(System.in);
    private final static String DEFAULT_MESSAGE = "Input author: ";

    public AuthorDeterminationCommand(ShelfBookService shelfBookService) {
        this.shelfBookService = shelfBookService;
    }

    @Override
    public void execute(BookShelf shelf) {

        Printer.print(DEFAULT_MESSAGE);
        String author = scan.next();
        shelfBookService.findAuthorBooks(shelf, author);
    }
}
