package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.BookShelf;
import com.dubodelov.service.api.ShelfBookInfoService;

public class PrinterCommand implements AppCommand {

    private final ShelfBookInfoService shelfBookInfoService;

    public PrinterCommand(ShelfBookInfoService shelfBookInfoService) {
        this.shelfBookInfoService = shelfBookInfoService;
    }

    @Override
    public void execute(BookShelf shelf) {
        shelfBookInfoService.printInfo(shelf);
    }
}
