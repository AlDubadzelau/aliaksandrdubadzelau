package com.dubodelov.service.api;

import com.dubodelov.model.BookShelf;

public interface ShelfBookService {

    public void findAuthorBooks(BookShelf shelf, String author);

    public void findPublisherBooks(BookShelf shelf, String publisher);

    public void findBooksPublishedInYear(BookShelf shelf, int year);
}
