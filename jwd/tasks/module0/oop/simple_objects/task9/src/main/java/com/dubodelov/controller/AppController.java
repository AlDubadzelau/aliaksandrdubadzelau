package com.dubodelov.controller;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.model.BookShelf;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(BookShelf shelf, String userCommand) {

        final AppCommand command = commandFactory.getCommand(userCommand);
        command.execute(shelf);
    }
}
