package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.BookShelf;
import com.dubodelov.service.api.ShelfBookService;
import com.dubodelov.utils.Printer;

import java.util.Scanner;

public class PublisherDeterminationCommand implements AppCommand {

    private final ShelfBookService shelfBookService;
    private final Scanner scan = new Scanner(System.in);
    private final static String DEFAULT_MESSAGE = "Input publisher: ";

    public PublisherDeterminationCommand(ShelfBookService shelfBookService) {
        this.shelfBookService = shelfBookService;
    }

    @Override
    public void execute(BookShelf shelf) {

        Printer.print(DEFAULT_MESSAGE);
        String publisher = scan.next();
        shelfBookService.findPublisherBooks(shelf, publisher);
    }
}
