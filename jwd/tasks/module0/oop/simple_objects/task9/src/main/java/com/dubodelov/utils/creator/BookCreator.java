package com.dubodelov.utils.creator;

import com.dubodelov.model.Book;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BookCreator {

    private final static int DEFAULT_AMOUNT_OF_BOOKS = 8;
    private final static String[] DEFAULT_NAMES = {
            "A-name", "B-name", "C-name", "D-name",
            "E-name", "F-name", "G-name", "H-name"};
    private final static String[] DEFAULT_AUTHORS = {
            "Ibragim", "Emanual", "Adward", "Adward",
            "Isaak", "Albert", "Isaak", "Isaak"
    };
    private final static String[] DEFAULT_PUBLISHER = {
            "house1", "house1", "house1", "house9",
            "house3", "house3", "house1", "house11"
    };
    private final static int[] DEFAULT_ID = {1, 2, 3, 4, 5, 6, 7, 8};
    private final static int[] DEFAULT_YEAR = {2000, 2000, 1998, 1901, 2000, 2011, 2001, 2009};
    private final static int[] DEFAULT_AMOUNT_OF_PAGES = {100, 100, 300, 300, 500, 100, 99, 1099};
    private final static BigDecimal[] DEFAULT_COSTS = {
            new BigDecimal(100), new BigDecimal(58585), new BigDecimal(1235), new BigDecimal(9999),
            new BigDecimal(123), new BigDecimal(1444), new BigDecimal(1111), new BigDecimal(434)
    };

    public List<Book> createBooks() {

        List<Book> books = new ArrayList<>();

        for (int i = 0; i < DEFAULT_AMOUNT_OF_BOOKS; i++) {

            String name = DEFAULT_NAMES[i];
            String author = DEFAULT_AUTHORS[i];
            String publisher = DEFAULT_PUBLISHER[i];
            int id = DEFAULT_ID[i];
            int publisherYear = DEFAULT_YEAR[i];
            int amountOfPages = DEFAULT_AMOUNT_OF_PAGES[i];
            BigDecimal cost = DEFAULT_COSTS[i];

            Book book = new Book(name, author, publisher, id, publisherYear, amountOfPages, cost);

            books.add(book);

        }

        return books;
    }
}
