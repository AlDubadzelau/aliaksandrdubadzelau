package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.BookShelf;
import com.dubodelov.service.api.ShelfBookInfoService;

public class PrinterPublisherBookCommand implements AppCommand {

    private final ShelfBookInfoService shelfBookInfoService;

    public PrinterPublisherBookCommand(ShelfBookInfoService shelfBookInfoService) {
        this.shelfBookInfoService = shelfBookInfoService;
    }

    @Override
    public void execute(BookShelf shelf) {
        shelfBookInfoService.printPublisherBooks(shelf);
    }
}
