package com.dubodelov.service.api;

import com.dubodelov.model.BookShelf;

public interface ShelfBookInfoService {

    public void printInfo(BookShelf shelf);

    public void printAuthorBooks(BookShelf shelf);

    public void printPublisherBooks(BookShelf shelf);

    public void printCertainYearBooks(BookShelf shelf);
}
