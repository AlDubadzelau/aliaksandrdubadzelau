import com.dubodelov.model.Test2;

public class App {

    private static final double DEFAULT_FIRST_VALUE = 12;
    private static final double DEFAULT_SECOND_VALUE = 21;

    public static void main(String[] args) {

        Test2 defaultObject = new Test2();
        Test2 object = new Test2(DEFAULT_FIRST_VALUE, DEFAULT_SECOND_VALUE);

        System.out.println(defaultObject.getFirstItem() + " " + defaultObject.getSecondItem());
        System.out.println(object.getFirstItem() + " " + object.getSecondItem());
        defaultObject.setFirstItem(DEFAULT_FIRST_VALUE);
        System.out.println(defaultObject.getFirstItem() + " " + defaultObject.getSecondItem());

    }
}
