package com.dubodelov.model;

public class Test2 {

    private final double DEFAULT_VALUE = 10;

    private double firstItem;
    private double secondItem;

    public Test2(double firstItem, double secondItem) {
        this.firstItem = firstItem;
        this.secondItem = secondItem;
    }

    public Test2() {
        this.firstItem = DEFAULT_VALUE;
        this.secondItem = DEFAULT_VALUE;
    }

    public double getFirstItem() {
        return firstItem;
    }

    public void setFirstItem(double firstItem) {
        this.firstItem = firstItem;
    }

    public double getSecondItem() {
        return secondItem;
    }

    public void setSecondItem(double secondItem) {
        this.secondItem = secondItem;
    }
}
