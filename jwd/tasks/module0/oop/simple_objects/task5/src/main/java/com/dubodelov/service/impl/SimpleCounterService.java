package com.dubodelov.service.impl;

import com.dubodelov.model.Counter;
import com.dubodelov.service.api.CounterService;

public class SimpleCounterService implements CounterService {

    private final static int DEFAULT_CHANGING = 1;

    @Override
    public void increase(Counter counter) {

        int newValue = counter.getValue() + DEFAULT_CHANGING;

        counter.setValue(newValue);
    }

    @Override
    public void decrease(Counter counter) {

        int newValue = counter.getValue() - DEFAULT_CHANGING;

        counter.setValue(newValue);
    }
}
