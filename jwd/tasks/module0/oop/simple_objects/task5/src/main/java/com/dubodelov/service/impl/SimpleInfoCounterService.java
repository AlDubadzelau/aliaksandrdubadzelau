package com.dubodelov.service.impl;

import com.dubodelov.model.Counter;
import com.dubodelov.service.api.InfoCounterService;

public class SimpleInfoCounterService implements InfoCounterService {

    @Override
    public void checkCondition(Counter counter) {

        System.out.println(counter);
    }
}
