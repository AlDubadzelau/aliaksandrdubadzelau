package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Counter;
import com.dubodelov.service.api.InfoCounterService;

public class InfoPrinterCommand implements AppCommand {

    private final InfoCounterService infoCounterService;

    public InfoPrinterCommand(InfoCounterService infoCounterService) {
        this.infoCounterService = infoCounterService;
    }

    @Override
    public void execute(Counter counter) {
        infoCounterService.checkCondition(counter);
    }
}
