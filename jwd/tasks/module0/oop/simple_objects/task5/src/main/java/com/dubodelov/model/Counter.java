package com.dubodelov.model;

public class Counter {

    private static final int DEFAULT_VALUE = 10;
    private int value;

    public Counter() {
        this.value = DEFAULT_VALUE;
    }

    public Counter(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "value: " + value;
    }
}
