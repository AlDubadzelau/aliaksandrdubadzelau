package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Counter;
import com.dubodelov.service.api.CounterService;

public class AppUpgraderCommand implements AppCommand {

    private final CounterService counterService;

    public AppUpgraderCommand(CounterService counterService) {
        this.counterService = counterService;
    }

    @Override
    public void execute(Counter counter) {
        counterService.increase(counter);
    }
}
