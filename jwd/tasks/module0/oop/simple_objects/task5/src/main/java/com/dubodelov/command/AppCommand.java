package com.dubodelov.command;

import com.dubodelov.model.Counter;

public interface AppCommand {

    public void execute(Counter counter);
}

