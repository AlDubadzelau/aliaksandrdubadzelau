package com.dubodelov;

import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.AppCommandName;
import com.dubodelov.command.impl.AppDegraderCommand;
import com.dubodelov.command.impl.AppUpgraderCommand;
import com.dubodelov.command.impl.InfoPrinterCommand;
import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.controller.impl.*;
import com.dubodelov.model.Counter;
import com.dubodelov.service.api.CounterService;
import com.dubodelov.service.api.InfoCounterService;
import com.dubodelov.service.impl.SimpleCounterService;
import com.dubodelov.service.impl.SimpleInfoCounterService;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        CounterService counterService = new SimpleCounterService();
        InfoCounterService infoCounterService = new SimpleInfoCounterService();

        AppCommand increase = new AppUpgraderCommand(counterService);
        AppCommand decrease = new AppDegraderCommand(counterService);
        AppCommand printInfo = new InfoPrinterCommand(infoCounterService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT, printInfo);
        commands.put(AppCommandName.DEGRADE, decrease);
        commands.put(AppCommandName.UPGRADE, increase);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        Counter counter = new Counter();

        while (isRunning) {

            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.DEGRADE);
            System.out.println(AppCommandName.UPGRADE);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(counter, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }

        }
    }
}
