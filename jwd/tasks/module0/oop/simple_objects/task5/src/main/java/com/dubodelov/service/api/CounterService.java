package com.dubodelov.service.api;

import com.dubodelov.model.Counter;

public interface CounterService {

    public void increase(Counter counter);
    public void decrease(Counter counter);
}
