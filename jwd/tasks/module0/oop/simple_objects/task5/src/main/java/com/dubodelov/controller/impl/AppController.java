package com.dubodelov.controller.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.model.Counter;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(Counter counter, String userCommand){

        final AppCommand command = commandFactory.getCommand(userCommand);
        command.execute(counter);
    }

}
