package com.dubodelov.service.api;

import com.dubodelov.model.Counter;

public interface InfoCounterService {

    public void checkCondition(Counter counter);
}
