package com.dubodelov.exception;

public class IncorrectTimeException extends Exception {

    public IncorrectTimeException() {
    }

    public IncorrectTimeException(String message) {
        super(message);
    }

    public IncorrectTimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
