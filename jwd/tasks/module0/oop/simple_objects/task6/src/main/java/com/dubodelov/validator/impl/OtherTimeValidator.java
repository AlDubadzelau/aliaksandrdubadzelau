package com.dubodelov.validator.impl;

import com.dubodelov.validator.TimeValidator;

public class OtherTimeValidator implements TimeValidator {

    private final static int MAX_DATE = 59;
    private final static int MIN_DATE = 0;
    private final static boolean DEFAULT_POSITIVE_RESULT = true;
    private final static boolean DEFAULT_NEGATIVE_RESULT = false;

    @Override
    public boolean checkTime(int time) {

        boolean result = DEFAULT_NEGATIVE_RESULT;

        if (time >= MIN_DATE && time <= MAX_DATE) {
            result = DEFAULT_POSITIVE_RESULT;
        }

        return result;

    }
}
