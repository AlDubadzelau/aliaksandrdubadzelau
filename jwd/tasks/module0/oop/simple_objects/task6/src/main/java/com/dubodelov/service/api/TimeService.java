package com.dubodelov.service.api;


import com.dubodelov.model.Time;

public interface TimeService {

    public void changeHours(Time time, int certainHours);

    public void changeMinutes(Time time, int certainMinutes);

    public void changeSeconds(Time time, int certainSeconds);
}

