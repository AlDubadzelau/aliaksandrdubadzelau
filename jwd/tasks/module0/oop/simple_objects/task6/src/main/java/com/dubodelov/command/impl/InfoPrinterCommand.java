package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Time;
import com.dubodelov.service.api.InfoTimeService;

public class InfoPrinterCommand implements AppCommand {

    private final InfoTimeService infoTimeService;

    public InfoPrinterCommand(InfoTimeService infoTimeService) {
        this.infoTimeService = infoTimeService;
    }

    @Override
    public void execute(Time time) {
        infoTimeService.printInfo(time);
    }
}
