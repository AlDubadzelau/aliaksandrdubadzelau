package com.dubodelov.service.impl;

import com.dubodelov.model.Time;
import com.dubodelov.service.api.TimeService;

public class SimpleTimeService implements TimeService {

    @Override
    public void changeHours(Time time, int certainHours) {

        time.setHour(certainHours);
    }

    @Override
    public void changeMinutes(Time time, int certainMinutes) {

        time.setMinutes(certainMinutes);

    }

    @Override
    public void changeSeconds(Time time, int certainSeconds) {

        time.setSeconds(certainSeconds);

    }
}
