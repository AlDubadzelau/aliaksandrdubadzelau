package com.dubodelov.service.api;

import com.dubodelov.model.Time;

public interface InfoTimeService {

    public void printInfo(Time time);
}
