package com.dubodelov.service.impl;

import com.dubodelov.model.Time;
import com.dubodelov.service.api.InfoTimeService;

public class SimpleInfoTimeService implements InfoTimeService {

    @Override
    public void printInfo(Time time) {
        System.out.println(time);
    }
}
