package com.dubodelov.controller;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.exception.ControllerException;
import com.dubodelov.exception.IncorrectTimeException;
import com.dubodelov.model.Time;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(Time time, String userCommand) throws ControllerException {

        try {
            final AppCommand command = commandFactory.getCommand(userCommand);
            command.execute(time);
        } catch (IncorrectTimeException ex) {
            throw new ControllerException(ex.getMessage(), ex);
        }
    }
}
