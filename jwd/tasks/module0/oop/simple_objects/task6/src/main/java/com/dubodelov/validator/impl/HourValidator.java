package com.dubodelov.validator.impl;

import com.dubodelov.validator.TimeValidator;

public class HourValidator implements TimeValidator {

    private final static int MAX_HOUR = 23;
    private final static int MIN_HOUR = 0;
    private final static boolean DEFAULT_POSITIVE_RESULT = true;
    private final static boolean DEFAULT_NEGATIVE_RESULT = false;

    @Override
    public boolean checkTime(int hour) {

        boolean result = DEFAULT_NEGATIVE_RESULT;

        if (hour >= MIN_HOUR && hour <= MAX_HOUR) {
            result = DEFAULT_POSITIVE_RESULT;
        }

        return result;

    }
}
