package com.dubodelov.validator;

public interface TimeValidator {

    public boolean checkTime(int data);
}
