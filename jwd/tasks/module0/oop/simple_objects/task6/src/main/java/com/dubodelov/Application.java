package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.*;
import com.dubodelov.controller.AppController;
import com.dubodelov.exception.ControllerException;
import com.dubodelov.exception.IncorrectTimeException;
import com.dubodelov.model.Time;
import com.dubodelov.service.api.InfoTimeService;
import com.dubodelov.service.api.TimeService;
import com.dubodelov.service.impl.SimpleInfoTimeService;
import com.dubodelov.service.impl.SimpleTimeService;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        TimeService timeService = new SimpleTimeService();
        InfoTimeService infoTimeService = new SimpleInfoTimeService();

        AppCommand printInfo = new InfoPrinterCommand(infoTimeService);
        AppCommand changeHours = new HourChangerCommand(timeService);
        AppCommand changeMinutes = new MinutesChangerCommand(timeService);
        AppCommand changeSeconds = new SecondsChangerCommand(timeService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.CHANGE_HOUR, changeHours);
        commands.put(AppCommandName.CHANGE_MINUTES, changeMinutes);
        commands.put(AppCommandName.CHANGE_SECONDS, changeSeconds);
        commands.put(AppCommandName.PRINT, printInfo);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        Time time = new Time();

        while (isRunning) {

            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.CHANGE_HOUR);
            System.out.println(AppCommandName.CHANGE_MINUTES);
            System.out.println(AppCommandName.CHANGE_SECONDS);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            try {
                controller.handleUserData(time, command);
            } catch (ControllerException e) {
                System.err.println(e);
            }
            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}
