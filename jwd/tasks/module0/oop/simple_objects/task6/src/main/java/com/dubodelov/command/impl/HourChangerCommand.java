package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.exception.IncorrectTimeException;
import com.dubodelov.model.Time;
import com.dubodelov.service.api.TimeService;
import com.dubodelov.util.Printer;
import com.dubodelov.validator.TimeValidator;
import com.dubodelov.validator.impl.HourValidator;

import java.util.Scanner;

public class HourChangerCommand implements AppCommand {

    private final Scanner scan = new Scanner(System.in);
    private final TimeService timeService;
    private final static String HOUR_MESSAGE = "Input amount of hours: ";

    public HourChangerCommand(TimeService timeService) {
        this.timeService = timeService;
    }

    @Override
    public void execute(Time time) throws IncorrectTimeException {

        TimeValidator validator = new HourValidator();
        Printer.print(HOUR_MESSAGE);
        int hour = scan.nextInt();

        boolean isCorrect = validator.checkTime(hour);

        if (!isCorrect) {
            throw new IncorrectTimeException();
        } else {
            timeService.changeHours(time, hour);
        }
    }
}
