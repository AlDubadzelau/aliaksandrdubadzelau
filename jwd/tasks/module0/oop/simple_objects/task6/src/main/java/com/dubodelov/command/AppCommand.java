package com.dubodelov.command;

import com.dubodelov.exception.IncorrectTimeException;
import com.dubodelov.model.Time;

public interface AppCommand {

    public void execute(Time time) throws IncorrectTimeException;
}
