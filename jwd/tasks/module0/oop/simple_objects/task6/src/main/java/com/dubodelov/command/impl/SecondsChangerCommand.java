package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.exception.IncorrectTimeException;
import com.dubodelov.model.Time;
import com.dubodelov.service.api.TimeService;
import com.dubodelov.util.Printer;
import com.dubodelov.validator.TimeValidator;
import com.dubodelov.validator.impl.OtherTimeValidator;

import java.util.Scanner;

public class SecondsChangerCommand implements AppCommand {

    private final Scanner scan = new Scanner(System.in);
    private final TimeService timeService;
    private final static String SECOND_MESSAGE = "Input amount of seconds: ";

    public SecondsChangerCommand(TimeService timeService) {
        this.timeService = timeService;
    }

    @Override
    public void execute(Time time) throws IncorrectTimeException {

        TimeValidator validator = new OtherTimeValidator();
        Printer.print(SECOND_MESSAGE);
        int seconds = scan.nextInt();
        boolean isCorrect = validator.checkTime(seconds);

        if (!isCorrect) {
            throw new IncorrectTimeException();
        } else {
            timeService.changeSeconds(time, seconds);
        }
    }
}
