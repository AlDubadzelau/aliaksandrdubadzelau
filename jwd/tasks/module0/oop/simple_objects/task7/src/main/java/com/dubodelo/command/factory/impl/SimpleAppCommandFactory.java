package com.dubodelo.command.factory.impl;

import com.dubodelo.command.AppCommand;
import com.dubodelo.command.factory.AppCommandFactory;
import com.dubodelo.command.impl.AppCommandName;

import java.util.Map;

public class SimpleAppCommandFactory implements AppCommandFactory {

    private final String DEFAULT_ERROR_MESSAGE = "BAD COMMAND";
    private final Map<AppCommandName, AppCommand> commandMap;

    public SimpleAppCommandFactory(Map<AppCommandName, AppCommand> commandMap) {
        this.commandMap = commandMap;
    }

    @Override
    public AppCommand getCommand(String commandName) {

        final AppCommandName appCommandName = AppCommandName.fromString(commandName);
        final AppCommand command = commandMap.getOrDefault(appCommandName, userData -> System.out.println(DEFAULT_ERROR_MESSAGE));

        return command;
    }
}
