package com.dubodelo.service.api;

import com.dubodelo.model.Triangle;

public interface InfoTriangleService {

    public void printInfo(Triangle triangle);
}
