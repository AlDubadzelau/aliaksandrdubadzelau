package com.dubodelo;

import com.dubodelo.command.AppCommand;
import com.dubodelo.command.factory.AppCommandFactory;
import com.dubodelo.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelo.command.impl.AppCommandName;
import com.dubodelo.command.impl.DeterminationPerimeterCommand;
import com.dubodelo.command.impl.DeterminationSquareCommand;
import com.dubodelo.command.impl.PrinterInfoCommand;
import com.dubodelo.controller.AppController;
import com.dubodelo.model.Triangle;
import com.dubodelo.service.api.InfoTriangleService;
import com.dubodelo.service.api.TriangleService;
import com.dubodelo.service.impl.SimpleInfoTriangleService;
import com.dubodelo.service.impl.SimpleTriangleService;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class Application {

    private final static int DEFAULT_FIRST_SIDE = 3;
    private final static int DEFAULT_SECOND_SIDE = 4;
    private final static int DEFAULT_THIRD_SIDE = 5;

    public static void main(String[] args) {

        InfoTriangleService infoTriangleService = new SimpleInfoTriangleService();
        TriangleService triangleService = new SimpleTriangleService();

        AppCommand determinateSquare = new DeterminationSquareCommand(triangleService);
        AppCommand determinatePerimeter = new DeterminationPerimeterCommand(triangleService);
        AppCommand printInfo = new PrinterInfoCommand(infoTriangleService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.DETERMINATE_PERIMETER, determinatePerimeter);
        commands.put(AppCommandName.DETERMINATE_SQUARE, determinateSquare);
        commands.put(AppCommandName.PRINT, printInfo);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        Triangle triangle = new Triangle(DEFAULT_FIRST_SIDE, DEFAULT_SECOND_SIDE, DEFAULT_THIRD_SIDE);

        while (isRunning) {

            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.DETERMINATE_PERIMETER);
            System.out.println(AppCommandName.DETERMINATE_SQUARE);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(triangle, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }

}
