package com.dubodelo.command.impl;

import com.dubodelo.command.AppCommand;
import com.dubodelo.model.Triangle;
import com.dubodelo.service.api.InfoTriangleService;

public class PrinterInfoCommand implements AppCommand {

    private final InfoTriangleService infoTriangleService;

    public PrinterInfoCommand(InfoTriangleService infoTriangleService) {
        this.infoTriangleService = infoTriangleService;
    }

    @Override
    public void execute(Triangle triangle) {
        infoTriangleService.printInfo(triangle);
    }
}
