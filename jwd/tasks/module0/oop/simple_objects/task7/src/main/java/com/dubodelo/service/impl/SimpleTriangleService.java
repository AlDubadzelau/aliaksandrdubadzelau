package com.dubodelo.service.impl;

import com.dubodelo.model.Triangle;
import com.dubodelo.service.api.TriangleService;

public class SimpleTriangleService implements TriangleService {

    private final static double DEFAULT_DIVIDER = 2;

    @Override
    public void findPerimeter(Triangle triangle) {

        double firstSide = triangle.getFirstSide();
        double secondSide = triangle.getSecondSide();
        double thirdSide = triangle.getThirdSide();

        double perimeter = firstSide + secondSide + thirdSide;
        triangle.setPerimeter(perimeter);
    }

    @Override
    public void findSquare(Triangle triangle) {

        double firstSide = triangle.getFirstSide();
        double secondSide = triangle.getSecondSide();
        double thirdSide = triangle.getThirdSide();

        findPerimeter(triangle);
        double semiPerimeter = triangle.getPerimeter() / DEFAULT_DIVIDER;
        double square = Math.sqrt(semiPerimeter * (semiPerimeter - firstSide) * (semiPerimeter - secondSide) * (semiPerimeter - thirdSide));
        triangle.setSquare(square);

    }
}
