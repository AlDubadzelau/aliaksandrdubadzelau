package com.dubodelo.command.impl;

import com.dubodelo.command.AppCommand;
import com.dubodelo.model.Triangle;
import com.dubodelo.service.api.TriangleService;

public class DeterminationSquareCommand implements AppCommand {

    private final TriangleService triangleService;

    public DeterminationSquareCommand(TriangleService triangleService) {
        this.triangleService = triangleService;
    }

    @Override
    public void execute(Triangle triangle) {
        triangleService.findSquare(triangle);
    }
}
