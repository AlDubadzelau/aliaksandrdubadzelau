package com.dubodelo.controller;

import com.dubodelo.command.AppCommand;
import com.dubodelo.command.factory.AppCommandFactory;
import com.dubodelo.model.Triangle;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(Triangle triangle, String userCommand) {

        final AppCommand command = commandFactory.getCommand(userCommand);
        command.execute(triangle);
    }

}
