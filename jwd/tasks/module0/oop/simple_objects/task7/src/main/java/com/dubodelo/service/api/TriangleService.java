package com.dubodelo.service.api;

import com.dubodelo.model.Triangle;

public interface TriangleService {

    public void findPerimeter(Triangle triangle);

    public void findSquare(Triangle triangle);

}
