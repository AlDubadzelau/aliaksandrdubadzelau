package com.dubodelo.command.factory;

import com.dubodelo.command.AppCommand;

public interface AppCommandFactory {

    public AppCommand getCommand(String commandName);
}
