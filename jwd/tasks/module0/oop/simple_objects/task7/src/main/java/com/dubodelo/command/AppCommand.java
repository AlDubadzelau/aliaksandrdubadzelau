package com.dubodelo.command;

import com.dubodelo.model.Triangle;

public interface AppCommand {

    public void execute(Triangle triangle);
}
