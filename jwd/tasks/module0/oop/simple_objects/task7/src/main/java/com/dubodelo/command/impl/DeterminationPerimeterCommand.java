package com.dubodelo.command.impl;

import com.dubodelo.command.AppCommand;
import com.dubodelo.model.Triangle;
import com.dubodelo.service.api.TriangleService;

public class DeterminationPerimeterCommand implements AppCommand {

    private final TriangleService triangleService;

    public DeterminationPerimeterCommand(TriangleService triangleService) {
        this.triangleService = triangleService;
    }

    @Override
    public void execute(Triangle triangle) {
        triangleService.findPerimeter(triangle);
    }
}
