package com.dubodelo.service.impl;

import com.dubodelo.model.Triangle;
import com.dubodelo.service.api.InfoTriangleService;

public class SimpleInfoTriangleService implements InfoTriangleService {

    @Override
    public void printInfo(Triangle triangle) {
        System.out.println(triangle);
    }
}
