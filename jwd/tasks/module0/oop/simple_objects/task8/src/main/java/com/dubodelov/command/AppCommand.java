package com.dubodelov.command;

import com.dubodelov.model.Group;

public interface AppCommand {

    public void execute(Group group);
}
