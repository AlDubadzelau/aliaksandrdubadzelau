package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Group;
import com.dubodelov.service.api.GroupService;
import com.dubodelov.utils.Printer;

import java.util.Scanner;

public class DeterminationCommand implements AppCommand {

    private final GroupService groupService;
    private final Scanner scan = new Scanner(System.in);
    private final static String MIN_BORDER_MESSAGE = "Input min card number(XXXX): ";
    private final static String MAX_BORDER_MESSAGE = "Input max card number(XXXX): ";

    public DeterminationCommand(GroupService groupService) {
        this.groupService = groupService;
    }

    @Override
    public void execute(Group group) {

        Printer.print(MIN_BORDER_MESSAGE);
        int minBorder = scan.nextInt();
        Printer.print(MAX_BORDER_MESSAGE);
        int maxBorder = scan.nextInt();

        groupService.findCertainCustomers(group, minBorder, maxBorder);
    }
}
