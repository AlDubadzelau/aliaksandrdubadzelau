package com.dubodelov.service.impl;

import com.dubodelov.model.Customer;
import com.dubodelov.service.api.InfoGroupService;

public class SimpleInfoGroupService implements InfoGroupService {

    @Override
    public void printInfo(Customer customer) {
        System.out.println(customer);
    }

    public void printCertainInfo(Customer customer) {
        System.out.println(customer);
    }
}
