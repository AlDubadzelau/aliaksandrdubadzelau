package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Customer;
import com.dubodelov.model.Group;
import com.dubodelov.service.api.InfoGroupService;

public class PrinterCommand implements AppCommand {

    private final InfoGroupService infoGroupService;

    public PrinterCommand(InfoGroupService infoGroupService) {
        this.infoGroupService = infoGroupService;
    }

    @Override
    public void execute(Group group) {

        for (Customer customer : group.getCustomers()) {
            infoGroupService.printInfo(customer);
        }
    }
}
