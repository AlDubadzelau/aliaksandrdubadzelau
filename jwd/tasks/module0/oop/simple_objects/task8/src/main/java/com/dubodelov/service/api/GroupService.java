package com.dubodelov.service.api;

import com.dubodelov.model.Group;

public interface GroupService {

    public void findCertainCustomers(Group group, int minNumberOfCreditCard, int maxNumberOfCreditCard);

    public void sortBySurnames(Group group);
}
