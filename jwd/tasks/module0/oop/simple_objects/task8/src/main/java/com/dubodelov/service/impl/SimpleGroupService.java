package com.dubodelov.service.impl;

import com.dubodelov.model.Customer;
import com.dubodelov.model.Group;
import com.dubodelov.service.api.GroupService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SimpleGroupService implements GroupService {

    @Override
    public void sortBySurnames(Group group) {
        List<Customer> customers = group.getCustomers();
        Collections.sort(customers);
    }

    @Override
    public void findCertainCustomers(Group group, int minNumberOfCreditCard, int maxNumberOfCreditCard) {

        List<Customer> certainCustomers = new ArrayList<Customer>();

        for (Customer customer : group.getCustomers()) {
            int numberOfCreditCard = customer.getNumberOfCreditsCard();

            if (numberOfCreditCard <= maxNumberOfCreditCard && numberOfCreditCard >= minNumberOfCreditCard) {
                certainCustomers.add(customer);
            }
        }

        group.setCertainCustomers(certainCustomers);
    }

}
