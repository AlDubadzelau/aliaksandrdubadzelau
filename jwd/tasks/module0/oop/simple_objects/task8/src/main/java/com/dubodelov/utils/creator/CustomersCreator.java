package com.dubodelov.utils.creator;

import com.dubodelov.model.Customer;
import com.sun.deploy.ui.DeployEmbeddedFrameIf;

import java.util.ArrayList;
import java.util.List;

public class CustomersCreator {

    private final static int DEFAULT_AMOUNT_OF_CUSTOMERS = 7;
    private final static String[] DEFAULT_NAMES = new String[]{
            "Sergei", "Edward", "Klim", "Ales",
            "Alex", "Viktor", "Petr"
    };
    private final static String[] DEFAULT_SURNAMES = new String[]{
            "Bornov", "Seledkin", "Gruzd", "Bobkin",
            "Livancevich", "Strahanov", "Bortnik"
    };
    private final static String[] DEFAULT_ADDRESS = new String[]{
            "st.Ghigimona 17", "st.Ghigimona 20", "st.Poniatovskogo 11",
            "st.Kastushko 99", "st.Shushkevicha 31", "st.Vilinskaia 3",
            "st.Bagdanovicha 1"
    };
    private final static int[] DEFAULT_CREDIT_CARDS_NUMBERS = new int[]{
            1234, 9000,
            5678, 6656,
            4443, 2344,
            1199
    };

    public List<Customer> createCustomer() {

        List<Customer> customers = new ArrayList<>();

        for (int i = 0; i < DEFAULT_AMOUNT_OF_CUSTOMERS; i++) {

            String name = DEFAULT_NAMES[i];
            String surname = DEFAULT_SURNAMES[i];
            String address = DEFAULT_ADDRESS[i];
            int numberOfCreditCard = DEFAULT_CREDIT_CARDS_NUMBERS[i];

            Customer customer = new Customer(name, surname, address, numberOfCreditCard);
            customers.add(customer);
        }

        return customers;
    }

}
