package com.dubodelov.model;

public class Customer implements Comparable<Customer> {

    private int id;
    private String name;
    private String surname;
    private String address;
    private int numberOfCreditsCard;
    private int bankAccountNumber;

    public Customer(String name, String surname, String address, int numberOfCreditsCard) {
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.numberOfCreditsCard = numberOfCreditsCard;
    }

    public Customer(int id, String name, String surname, String address, int numberOfCreditsCard, int bankAccountNumber) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.numberOfCreditsCard = numberOfCreditsCard;
        this.bankAccountNumber = bankAccountNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumberOfCreditsCard() {
        return numberOfCreditsCard;
    }

    public void setNumberOfCreditsCard(int numberOfCreditsCard) {
        this.numberOfCreditsCard = numberOfCreditsCard;
    }

    public int getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(int bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public int compareTo(Customer o) {
        return this.getSurname().compareTo(o.getSurname());
    }

    @Override
    public String toString() {
        return " Surname= " + surname +
                ", name= " + name +
                ", address= " + address +
                ", numberOfCreditsCard= " + numberOfCreditsCard;
    }
}
