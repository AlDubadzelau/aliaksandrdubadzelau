package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.*;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.Customer;
import com.dubodelov.model.Group;
import com.dubodelov.service.api.GroupService;
import com.dubodelov.service.api.InfoGroupService;
import com.dubodelov.service.impl.SimpleGroupService;
import com.dubodelov.service.impl.SimpleInfoGroupService;
import com.dubodelov.utils.creator.CustomersCreator;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        GroupService groupService = new SimpleGroupService();
        InfoGroupService infoGroupService = new SimpleInfoGroupService();

        AppCommand determinate = new DeterminationCommand(groupService);
        AppCommand sort = new SorterCommand(groupService);
        AppCommand print = new PrinterCommand(infoGroupService);
        AppCommand printCertain = new PrinterCertainCommand(infoGroupService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT, print);
        commands.put(AppCommandName.PRINT_CERTAIN, printCertain);
        commands.put(AppCommandName.DETERMINATE, determinate);
        commands.put(AppCommandName.SORT, sort);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        CustomersCreator customersCreator = new CustomersCreator();
        List<Customer> customers = customersCreator.createCustomer();
        Group group = new Group(customers);

        while (isRunning) {

            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.PRINT_CERTAIN);
            System.out.println(AppCommandName.DETERMINATE);
            System.out.println(AppCommandName.SORT);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(group, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}
