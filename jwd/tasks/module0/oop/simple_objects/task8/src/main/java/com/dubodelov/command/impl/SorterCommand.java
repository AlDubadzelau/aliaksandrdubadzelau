package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Group;
import com.dubodelov.service.api.GroupService;

public class SorterCommand implements AppCommand {

    private final GroupService groupService;

    public SorterCommand(GroupService groupService) {
        this.groupService = groupService;
    }

    @Override
    public void execute(Group group) {
        groupService.sortBySurnames(group);
    }
}
