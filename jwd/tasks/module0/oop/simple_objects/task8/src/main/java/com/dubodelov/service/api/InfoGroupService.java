package com.dubodelov.service.api;

import com.dubodelov.model.Customer;

public interface InfoGroupService {

    public void printInfo(Customer customer);

    public void printCertainInfo(Customer customer);
}
