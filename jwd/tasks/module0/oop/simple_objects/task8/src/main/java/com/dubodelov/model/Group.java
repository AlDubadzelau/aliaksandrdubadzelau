package com.dubodelov.model;

import java.util.ArrayList;
import java.util.List;

public class Group {

    private List<Customer> customers;
    private List<Customer> certainCustomers = new ArrayList<Customer>();

    public Group(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Customer> getCertainCustomers() {
        return certainCustomers;
    }

    public void setCertainCustomers(List<Customer> certainCustomers) {
        this.certainCustomers = certainCustomers;
    }
}
