package com.dubodelov.service.api;

import com.dubodelov.model.Car;

public interface CarInfoService {

    public void printInfo(Car car);

    public void printCarModel(String model);
}
