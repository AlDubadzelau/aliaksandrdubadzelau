package com.dubodelov.util;

import java.util.Scanner;

public class UserInput {

    private final Scanner scan = new Scanner(System.in);

    public double inputDouble() {
        return scan.nextDouble();
    }
}
