package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Car;
import com.dubodelov.service.api.CarInfoService;

public class PrinterModelCommand implements AppCommand {

    private final CarInfoService carInfoService;

    public PrinterModelCommand(CarInfoService carInfoService) {
        this.carInfoService = carInfoService;
    }

    @Override
    public void execute(Car car) {

        String model = car.getModel();

        carInfoService.printCarModel(model);
    }
}
