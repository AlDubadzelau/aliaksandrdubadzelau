package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Car;
import com.dubodelov.model.Wheel;
import com.dubodelov.service.api.CarService;
import com.dubodelov.util.creator.WheelCreator;

import java.util.List;

public class WheelsChangerCommand implements AppCommand {

    private final CarService carService;

    public WheelsChangerCommand(CarService carService) {
        this.carService = carService;
    }

    @Override
    public void execute(Car car) {

        List<Wheel> oldWheels = car.getWheels();
        WheelCreator creator = new WheelCreator();
        List<Wheel> newWheels = creator.create();

        carService.changeWheels(oldWheels, newWheels);
    }
}
