package com.dubodelov.util.creator;

import com.dubodelov.model.Wheel;

import java.util.ArrayList;
import java.util.List;

public class WheelCreator {

    //example
    private final static String DEFAULT_NAME = "CHANGED-WHEELS-MODEL";
    private final static int DEFAULT_AMOUNT_OF_WHEELS = 4;

    public List<Wheel> create(){

        List<Wheel> wheels = new ArrayList<>();

        for(int i = 0; i < DEFAULT_AMOUNT_OF_WHEELS; i++){

            Wheel wheel = new Wheel(DEFAULT_NAME);
            wheels.add(wheel);
        }

        return wheels;
    }
}
