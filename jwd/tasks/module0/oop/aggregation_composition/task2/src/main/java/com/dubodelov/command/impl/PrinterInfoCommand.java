package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Car;
import com.dubodelov.service.api.CarInfoService;

public class PrinterInfoCommand implements AppCommand {

    private final CarInfoService carInfoService;

    public PrinterInfoCommand(CarInfoService carInfoService) {
        this.carInfoService = carInfoService;
    }

    @Override
    public void execute(Car car) {

        carInfoService.printInfo(car);
    }
}
