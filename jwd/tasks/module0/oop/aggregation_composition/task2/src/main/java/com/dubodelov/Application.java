package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.*;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.Car;
import com.dubodelov.model.Engine;
import com.dubodelov.model.Wheel;
import com.dubodelov.service.api.CarInfoService;
import com.dubodelov.service.api.CarService;
import com.dubodelov.service.impl.SimpleCarInfoService;
import com.dubodelov.service.impl.SimpleCarService;

import java.util.*;

public class Application {

    public static void main(String[] args) {

        CarInfoService textInfoService = new SimpleCarInfoService();
        CarService textService = new SimpleCarService();

        AppCommand change = new WheelsChangerCommand(textService);
        AppCommand move = new MovementCommand(textService);
        AppCommand refuel = new UpgradeFuelCommand(textService);
        AppCommand printModel = new PrinterModelCommand(textInfoService);
        AppCommand printInfo = new PrinterInfoCommand(textInfoService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT_CAR_INFO, printInfo);
        commands.put(AppCommandName.PRINT_MODEL, printModel);
        commands.put(AppCommandName.REFUEL, refuel);
        commands.put(AppCommandName.MOVE, move);
        commands.put(AppCommandName.CHANGE_WHEELS, change);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        List<Wheel> wheels = new ArrayList<>();
        Wheel wheel = new Wheel("DEFAULT MODEL WHEEL");
        wheels.add(wheel);
        wheels.add(wheel);
        wheels.add(wheel);
        wheels.add(wheel);
        String model = "Bi-bi Car";
        double fuel = 0;
        Car car = new Car(wheels, model, fuel);

        while (isRunning) {
            System.out.println(AppCommandName.PRINT_CAR_INFO);
            System.out.println(AppCommandName.PRINT_MODEL);
            System.out.println(AppCommandName.REFUEL);
            System.out.println(AppCommandName.MOVE);
            System.out.println(AppCommandName.CHANGE_WHEELS);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(car, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}
