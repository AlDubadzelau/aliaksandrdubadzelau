package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Car;
import com.dubodelov.service.api.CarService;

public class MovementCommand implements AppCommand {

    private final CarService carService;

    public MovementCommand(CarService carService) {
        this.carService = carService;
    }

    @Override
    public void execute(Car car) {
        carService.isGo(car);
    }
}
