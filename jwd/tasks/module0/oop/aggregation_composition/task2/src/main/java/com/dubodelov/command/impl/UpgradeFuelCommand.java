package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Car;
import com.dubodelov.service.api.CarService;
import com.dubodelov.util.Printer;
import com.dubodelov.util.UserInput;

import java.util.InputMismatchException;

public class UpgradeFuelCommand implements AppCommand {

    private final static String DEFAULT_MESSAGE = "Enter amount of fuel: ";
    private final CarService carService;

    public UpgradeFuelCommand(CarService carService) {
        this.carService = carService;
    }

    @Override
    public void execute(Car car) {

        Printer.print(DEFAULT_MESSAGE);
        UserInput input = new UserInput();

        try {
            double amountOfFuel = input.inputDouble();
            carService.refuel(car, amountOfFuel);
        } catch (InputMismatchException e) {
            System.err.println(e);
        }
    }
}
