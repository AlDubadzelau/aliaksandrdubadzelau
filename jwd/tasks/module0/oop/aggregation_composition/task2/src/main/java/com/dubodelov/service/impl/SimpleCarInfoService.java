package com.dubodelov.service.impl;

import com.dubodelov.model.Car;
import com.dubodelov.service.api.CarInfoService;

public class SimpleCarInfoService implements CarInfoService {

    @Override
    public void printInfo(Car car) {
        System.out.println(car);
    }

    @Override
    public void printCarModel(String model) {
        System.out.println(model);
    }
}
