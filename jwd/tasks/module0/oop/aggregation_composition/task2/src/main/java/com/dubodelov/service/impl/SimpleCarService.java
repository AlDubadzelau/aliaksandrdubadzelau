package com.dubodelov.service.impl;

import com.dubodelov.model.Car;
import com.dubodelov.model.Wheel;
import com.dubodelov.service.api.CarService;

import java.util.List;

public class SimpleCarService implements CarService {

    private static final boolean IS_GOING = true;

    @Override
    public void isGo(Car car) {

        car.setGoing(IS_GOING);
    }

    @Override
    public void refuel(Car car, double amountOfFuel) {

        car.setFuel(amountOfFuel);
    }

    @Override
    public void changeWheels(List<Wheel> wheels, List<Wheel> newWheels) {

        wheels.clear();
        wheels.addAll(newWheels);
    }
}
