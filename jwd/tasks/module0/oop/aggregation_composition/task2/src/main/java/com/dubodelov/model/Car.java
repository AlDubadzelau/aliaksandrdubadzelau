package com.dubodelov.model;

import java.util.List;

public class Car {

    private final static boolean DEFAULT_VALUE = false;
    private final static String DEFAULT_ENGINE_MODEL = "Bi-bi Engine";

    private List<Wheel> wheels;
    private Engine engine;
    private String model;
    private double fuel;
    private boolean isGoing;

    {
        engine = new Engine(DEFAULT_ENGINE_MODEL);
        isGoing = DEFAULT_VALUE;
    }

    public Car(List<Wheel> wheels, String model, double fuel) {
        this.wheels = wheels;
        this.model = model;
        this.fuel = fuel;
    }

    public Car(List<Wheel> wheels, Engine engine, String model, double fuel) {
        this.wheels = wheels;
        this.engine = engine;
        this.model = model;
        this.fuel = fuel;
    }

    public Car(List<Wheel> wheels, Engine engine, String model, double fuel, boolean isGoing) {
        this.wheels = wheels;
        this.engine = engine;
        this.model = model;
        this.fuel = fuel;
        this.isGoing = isGoing;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public void setWheels(List<Wheel> wheels) {
        this.wheels = wheels;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getFuel() {
        return fuel;
    }

    public void setFuel(double fuel) {
        this.fuel = fuel;
    }

    public boolean isGoing() {
        return isGoing;
    }

    public void setGoing(boolean going) {
        isGoing = going;
    }

    @Override
    public String toString() {
        return wheels.toString() + "\n" +
                engine.toString() + "\n" +
                "Model= " + model +
                " Fuel= " + fuel +
                " IsGoing= " + isGoing;
    }
}
