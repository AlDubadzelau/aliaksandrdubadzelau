package com.dubodelov.service.api;

import com.dubodelov.model.Car;
import com.dubodelov.model.Wheel;

import java.util.List;

public interface CarService {

    public void isGo(Car car);

    public void refuel(Car car, double amountOfFuel);

    public void changeWheels(List<Wheel> wheels, List<Wheel> newWheels);
}
