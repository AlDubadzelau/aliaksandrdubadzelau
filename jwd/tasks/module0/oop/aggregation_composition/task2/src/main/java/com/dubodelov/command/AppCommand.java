package com.dubodelov.command;

import com.dubodelov.model.Car;

public interface AppCommand {

    public void execute(Car car);
}
