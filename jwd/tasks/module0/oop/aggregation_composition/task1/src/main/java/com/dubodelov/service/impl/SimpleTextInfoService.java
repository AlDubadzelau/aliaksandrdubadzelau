package com.dubodelov.service.impl;

import com.dubodelov.model.Text;
import com.dubodelov.service.api.TextInfoService;

public class SimpleTextInfoService implements TextInfoService {

    @Override
    public void printText(Text text) {
        System.out.println(text);
    }

    @Override
    public void printName(String name) {
        System.out.println(name);
    }
}
