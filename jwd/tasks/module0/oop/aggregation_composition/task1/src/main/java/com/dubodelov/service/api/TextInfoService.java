package com.dubodelov.service.api;

import com.dubodelov.model.Text;

public interface TextInfoService {

    public void printText(Text text);

    public void printName(String name);
}
