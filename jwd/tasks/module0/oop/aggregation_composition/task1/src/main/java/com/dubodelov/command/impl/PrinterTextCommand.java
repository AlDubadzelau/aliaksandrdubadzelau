package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Text;
import com.dubodelov.service.api.TextInfoService;

public class PrinterTextCommand implements AppCommand {

    private final TextInfoService textInfoService;

    public PrinterTextCommand(TextInfoService textInfoService) {
        this.textInfoService = textInfoService;
    }

    @Override
    public void execute(Text text) {

        textInfoService.printText(text);
    }
}
