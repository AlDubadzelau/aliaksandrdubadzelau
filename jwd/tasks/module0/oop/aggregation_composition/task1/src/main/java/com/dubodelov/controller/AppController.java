package com.dubodelov.controller;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.model.Text;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(Text text, String userCommand) {

        final AppCommand command = commandFactory.getCommand(userCommand);
        command.execute(text);
    }

}
