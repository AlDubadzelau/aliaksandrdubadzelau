package com.dubodelov.service.api;

import com.dubodelov.model.Sentence;
import com.dubodelov.model.Text;

import java.util.List;

public interface TextService {

    public void addSentence(List<Sentence> sentences, Sentence sentence);
}
