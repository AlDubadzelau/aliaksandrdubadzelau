package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Text;
import com.dubodelov.service.api.TextInfoService;

public class PrinterNameCommand implements AppCommand {

    private final TextInfoService textInfoService;

    public PrinterNameCommand(TextInfoService textInfoService) {
        this.textInfoService = textInfoService;
    }

    @Override
    public void execute(Text text) {

        String name = text.getName();

        textInfoService.printName(name);
    }
}
