package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Sentence;
import com.dubodelov.model.Text;
import com.dubodelov.service.api.TextService;
import com.dubodelov.util.creator.SentenceCreator;

import java.util.List;

public class AdderCommand implements AppCommand {

    private final TextService textService;

    public AdderCommand(TextService textService) {
        this.textService = textService;
    }

    @Override
    public void execute(Text text) {

        List<Sentence> sentences = text.getText();
        SentenceCreator creator = new SentenceCreator();
        Sentence sentence = creator.create();

        textService.addSentence(sentences, sentence);
    }
}
