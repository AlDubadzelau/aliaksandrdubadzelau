package com.dubodelov.util.creator;

import com.dubodelov.model.Sentence;
import com.dubodelov.model.Word;
import com.dubodelov.util.Printer;
import com.dubodelov.util.UserInput;

import java.util.ArrayList;
import java.util.List;

public class SentenceCreator {

    //example
    private final static int DEFAULT_AMOUNT_OF_WORDS = 5;
    private final static String DEFAULT_MESSAGE = "Enter words: ";

    public Sentence create(){

        List<Word> words = new ArrayList<>();
        Sentence sentence = new Sentence();
        UserInput input = new UserInput();
        Printer.print(DEFAULT_MESSAGE);

        for (int i = 0; i < DEFAULT_AMOUNT_OF_WORDS; i++){

            String word = input.inputWord();
            Word resultWord = new Word(word);
            words.add(resultWord);
        }

        sentence.setSentence(words);

        return sentence;
    }
}
