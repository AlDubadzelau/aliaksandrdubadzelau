package com.dubodelov.model;

import java.util.List;

public class Sentence {

    private List<Word> sentence;

    public List<Word> getSentence() {
        return sentence;
    }

    public void setSentence(List<Word> sentence) {
        this.sentence = sentence;
    }

    @Override
    public String toString() {
        return "Sentence= " + sentence;
    }
}
