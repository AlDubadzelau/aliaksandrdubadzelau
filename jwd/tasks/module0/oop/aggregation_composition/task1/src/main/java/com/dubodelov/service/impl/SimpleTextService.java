package com.dubodelov.service.impl;

import com.dubodelov.model.Sentence;
import com.dubodelov.model.Text;
import com.dubodelov.service.api.TextService;

import java.util.List;

public class SimpleTextService implements TextService {

    @Override
    public void addSentence(List<Sentence> sentences, Sentence sentence) {
        sentences.add(sentence);
    }
}
