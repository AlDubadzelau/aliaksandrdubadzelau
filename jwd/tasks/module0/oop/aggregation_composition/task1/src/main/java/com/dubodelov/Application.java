package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.AdderCommand;
import com.dubodelov.command.impl.AppCommandName;
import com.dubodelov.command.impl.PrinterNameCommand;
import com.dubodelov.command.impl.PrinterTextCommand;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.Sentence;
import com.dubodelov.model.Text;
import com.dubodelov.model.Word;
import com.dubodelov.service.api.TextInfoService;
import com.dubodelov.service.api.TextService;
import com.dubodelov.service.impl.SimpleTextInfoService;
import com.dubodelov.service.impl.SimpleTextService;

import java.util.*;

public class Application {

    public static void main(String[] args) {

        TextInfoService textInfoService = new SimpleTextInfoService();
        TextService textService = new SimpleTextService();

        AppCommand add = new AdderCommand(textService);
        AppCommand printName = new PrinterNameCommand(textInfoService);
        AppCommand printText = new PrinterTextCommand(textInfoService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT_NAME, printName);
        commands.put(AppCommandName.PRINT_TEXT, printText);
        commands.put(AppCommandName.ADD, add);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        String name = "Task1";
        Text text = new Text(name);

        while (isRunning) {
            System.out.println(AppCommandName.PRINT_NAME);
            System.out.println(AppCommandName.PRINT_TEXT);
            System.out.println(AppCommandName.ADD);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(text, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}
