package com.dubodelov.util;

import java.util.Scanner;

public class UserInput {

    private final Scanner scan = new Scanner(System.in);

    public String inputWord(){
        return scan.next();
    }
}
