package com.dubodelov.command;

import com.dubodelov.model.Text;

public interface AppCommand {

    public void execute(Text text);
}
