package com.dubodelov.model;

import java.util.ArrayList;
import java.util.List;

public class Text {

    private String name;
    private List<Sentence> text = new ArrayList<>();

    public Text(String name) {
        this.name = name;
    }

    public List<Sentence> getText() {
        return text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setText(List<Sentence> text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Name: " + name + "\nText= " + text;
    }

}
