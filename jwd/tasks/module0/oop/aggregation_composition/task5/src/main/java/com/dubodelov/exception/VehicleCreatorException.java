package com.dubodelov.exception;

public class VehicleCreatorException extends Exception {

    public VehicleCreatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public VehicleCreatorException(String message) {
        super(message);
    }
}
