package com.dubodelov.command;

import com.dubodelov.model.Voucher;

import java.util.List;

public interface AppCommand {

    public void execute(List<Voucher> vouchers);
}
