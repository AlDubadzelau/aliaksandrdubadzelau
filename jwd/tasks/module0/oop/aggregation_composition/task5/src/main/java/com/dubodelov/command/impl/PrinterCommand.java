package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Voucher;
import com.dubodelov.service.api.VoucherInfoService;

import java.util.List;

public class PrinterCommand implements AppCommand {

    private final VoucherInfoService voucherInfoService;

    public PrinterCommand(VoucherInfoService voucherInfoService) {
        this.voucherInfoService = voucherInfoService;
    }

    @Override
    public void execute(List<Voucher> vouchers) {

        for (Voucher voucher : vouchers) {
            voucherInfoService.printVoucher(voucher);
        }

    }
}
