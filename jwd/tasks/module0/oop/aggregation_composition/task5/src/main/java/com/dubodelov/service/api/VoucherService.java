package com.dubodelov.service.api;

import com.dubodelov.model.MealType;
import com.dubodelov.model.VehicleType;
import com.dubodelov.model.Voucher;

import java.util.List;

public interface VoucherService {

    public List<Voucher> findVouchersByTransport(List<Voucher> vouchers, VehicleType vehicleType);

    public List<Voucher> findVouchersByMeal(List<Voucher> vouchers, MealType mealType);

    public List<Voucher> findVoucherByDays(List<Voucher> vouchers, int amountOfDays);

    public void sortByDays(List<Voucher> vouchers);
}
