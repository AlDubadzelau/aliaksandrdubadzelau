package com.dubodelov.service.impl;

import com.dubodelov.model.MealType;
import com.dubodelov.model.VehicleType;
import com.dubodelov.model.Voucher;
import com.dubodelov.service.api.VoucherService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SimpleVoucherService implements VoucherService {

    @Override
    public List<Voucher> findVouchersByTransport(List<Voucher> vouchers, VehicleType vehicleType) {

        List<Voucher> newVouchers = new ArrayList<>();

        for (Voucher voucher : vouchers) {
            VehicleType currentVehicle = voucher.getVehicleType();

            if (currentVehicle.equals(vehicleType)) {
                newVouchers.add(voucher);
            }
        }

        return newVouchers;
    }

    @Override
    public List<Voucher> findVouchersByMeal(List<Voucher> vouchers, MealType mealType) {

        List<Voucher> newVouchers = new ArrayList<>();

        for (Voucher voucher : vouchers) {
            MealType currentMeal = voucher.getMealType();

            if (currentMeal.equals(mealType)) {
                newVouchers.add(voucher);
            }
        }

        return newVouchers;
    }

    @Override
    public List<Voucher> findVoucherByDays(List<Voucher> vouchers, int amountOfDays) {

        List<Voucher> newVouchers = new ArrayList<>();

        for (Voucher voucher : vouchers) {
            int currentAmountOfDays = voucher.getAmountOfDays();

            if (amountOfDays <= currentAmountOfDays) {
                newVouchers.add(voucher);
            }
        }

        return newVouchers;
    }

    @Override
    public void sortByDays(List<Voucher> vouchers) {
        Collections.sort(vouchers);
    }
}
