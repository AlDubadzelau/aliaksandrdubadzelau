package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.exception.MealCreatorException;
import com.dubodelov.model.MealType;
import com.dubodelov.model.Voucher;
import com.dubodelov.service.api.VoucherInfoService;
import com.dubodelov.service.api.VoucherService;
import com.dubodelov.utils.creator.MealCreator;

import java.util.List;

public class ChoicerVoucherByMealCommand implements AppCommand {

    private final VoucherService voucherService;
    private final VoucherInfoService voucherInfoService;

    public ChoicerVoucherByMealCommand(VoucherService voucherService, VoucherInfoService voucherInfoService) {
        this.voucherService = voucherService;
        this.voucherInfoService = voucherInfoService;
    }

    @Override
    public void execute(List<Voucher> vouchers) {

        try {
            MealCreator creator = new MealCreator();
            MealType mealType = creator.create();
            List<Voucher> certainVouchers = voucherService.findVouchersByMeal(vouchers, mealType);

            for (Voucher voucher : certainVouchers) {
                voucherInfoService.printVoucher(voucher);
            }

        } catch (MealCreatorException e) {
            System.err.println(e);
        }
    }

}

