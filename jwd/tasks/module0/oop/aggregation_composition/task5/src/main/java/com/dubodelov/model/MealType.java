package com.dubodelov.model;

public enum MealType {

    ALL_INCLUSIVE("AI"),
    BED_BREAKFAST("BB"),
    HALF_BOARD("HB"),
    FULL_BOARD("FB");

    private String mealType;

    MealType(String type) {
        this.mealType = type;
    }

    public String getMealType() {
        return mealType;
    }

    @Override
    public String toString() {
        return " Meal type= " + mealType;
    }
}
