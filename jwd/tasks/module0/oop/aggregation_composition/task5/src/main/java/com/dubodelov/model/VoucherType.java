package com.dubodelov.model;

public enum VoucherType {

    EXCURSION("Excursion"),
    SANATORIUM_VOUCHER("Sanatorium"),
    SHOPPING("Shopping");

    private String voucherType;

    VoucherType(String voucher) {
        this.voucherType = voucher;
    }

    @Override
    public String toString() {
        return " Voucher type='" + voucherType;
    }
}
