package com.dubodelov.controller;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.model.Voucher;

import java.util.List;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(List<Voucher> vouchers, String userCommand) {

        final AppCommand command = commandFactory.getCommand(userCommand);
        command.execute(vouchers);
    }

}
