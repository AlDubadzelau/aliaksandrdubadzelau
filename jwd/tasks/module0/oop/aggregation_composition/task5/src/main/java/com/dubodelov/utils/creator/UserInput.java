package com.dubodelov.utils.creator;

import java.util.Scanner;

public class UserInput {

    private final Scanner scan = new Scanner(System.in);

    public int inputInt() {
        return scan.nextInt();
    }
}
