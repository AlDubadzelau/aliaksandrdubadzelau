package com.dubodelov.model;

public class Voucher implements Comparable<Voucher> {

    private int amountOfDays;
    private MealType mealType;
    private VehicleType vehicleType;
    private VoucherType voucherType;

    public Voucher(int amountOfDays, MealType mealType, VehicleType vehicleType, VoucherType voucherType) {
        this.amountOfDays = amountOfDays;
        this.mealType = mealType;
        this.vehicleType = vehicleType;
        this.voucherType = voucherType;
    }

    public int getAmountOfDays() {
        return amountOfDays;
    }

    public MealType getMealType() {
        return mealType;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public VoucherType getVoucherType() {
        return voucherType;
    }

    @Override
    public int compareTo(Voucher o) {
        return this.amountOfDays - o.amountOfDays;
    }

    @Override
    public String toString() {
        return "Voucher: " + "\n" +
                " days - " + amountOfDays + "\n" +
                voucherType.toString() + "\n" +
                vehicleType.toString() + "\n" +
                mealType.toString() + ".\n";
    }
}
