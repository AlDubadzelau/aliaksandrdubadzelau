package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.exception.VehicleCreatorException;
import com.dubodelov.model.VehicleType;
import com.dubodelov.model.Voucher;
import com.dubodelov.service.api.VoucherInfoService;
import com.dubodelov.service.api.VoucherService;
import com.dubodelov.utils.creator.VehicleCreator;

import java.util.List;

public class ChoicerVoucherByVehicleCommand implements AppCommand {

    private final VoucherService voucherService;
    private final VoucherInfoService voucherInfoService;

    public ChoicerVoucherByVehicleCommand(VoucherService voucherService, VoucherInfoService voucherInfoService) {
        this.voucherService = voucherService;
        this.voucherInfoService = voucherInfoService;
    }

    @Override
    public void execute(List<Voucher> vouchers) {

        try {
            VehicleCreator creator = new VehicleCreator();
            VehicleType vehicleType = creator.create();
            List<Voucher> certainVouchers = voucherService.findVouchersByTransport(vouchers, vehicleType);

            for (Voucher voucher : certainVouchers) {
                voucherInfoService.printVoucher(voucher);
            }

        } catch (VehicleCreatorException e) {
            System.err.println(e);
        }
    }
}
