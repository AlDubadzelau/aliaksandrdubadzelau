package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Voucher;
import com.dubodelov.service.api.VoucherInfoService;
import com.dubodelov.service.api.VoucherService;
import com.dubodelov.utils.creator.Printer;
import com.dubodelov.utils.creator.UserInput;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class ChoicerVoucherByDaysCommand implements AppCommand {

    private final static String DEFAULT_MESSAGE = "Enter min amount of days: ";
    private final VoucherService voucherService;
    private final VoucherInfoService voucherInfoService;

    public ChoicerVoucherByDaysCommand(VoucherService voucherService, VoucherInfoService voucherInfoService) {
        this.voucherService = voucherService;
        this.voucherInfoService = voucherInfoService;
    }

    @Override
    public void execute(List<Voucher> vouchers) {

        Printer.print(DEFAULT_MESSAGE);

        try {
            UserInput input = new UserInput();
            int amountOfPreferenceDays = input.inputInt();
            List<Voucher> certainVouchers = voucherService.findVoucherByDays(vouchers, amountOfPreferenceDays);

            for (Voucher voucher : certainVouchers) {
                voucherInfoService.printVoucher(voucher);
            }
        } catch (InputMismatchException e) {
            System.err.println(e);
        }
    }
}
