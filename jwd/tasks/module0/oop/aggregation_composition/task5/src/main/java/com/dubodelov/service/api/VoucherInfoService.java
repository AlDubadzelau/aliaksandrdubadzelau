package com.dubodelov.service.api;

import com.dubodelov.model.Voucher;

import java.util.List;

public interface VoucherInfoService {

    public void printVoucher(Voucher voucher);

}
