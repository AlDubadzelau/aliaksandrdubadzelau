package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Voucher;
import com.dubodelov.service.api.VoucherInfoService;
import com.dubodelov.service.api.VoucherService;

import java.util.List;

public class SorterCommand implements AppCommand {

    private final VoucherService voucherService;

    public SorterCommand(VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    @Override
    public void execute(List<Voucher> vouchers) {
        voucherService.sortByDays(vouchers);
    }
}
