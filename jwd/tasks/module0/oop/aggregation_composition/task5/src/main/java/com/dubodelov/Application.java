package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.*;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.MealType;
import com.dubodelov.model.VehicleType;
import com.dubodelov.model.Voucher;
import com.dubodelov.model.VoucherType;
import com.dubodelov.service.api.VoucherInfoService;
import com.dubodelov.service.api.VoucherService;
import com.dubodelov.service.impl.SimpleVoucherInfoService;
import com.dubodelov.service.impl.SimpleVoucherService;

import java.util.*;

public class Application {

    public static void main(String[] args) {

        VoucherInfoService voucherInfoService = new SimpleVoucherInfoService();
        VoucherService voucherService = new SimpleVoucherService();

        AppCommand print = new PrinterCommand(voucherInfoService);
        AppCommand sort = new SorterCommand(voucherService);
        AppCommand choiceByDay = new ChoicerVoucherByDaysCommand(voucherService, voucherInfoService);
        AppCommand choiceByMeal = new ChoicerVoucherByMealCommand(voucherService, voucherInfoService);
        AppCommand choiceByVehicle = new ChoicerVoucherByVehicleCommand(voucherService, voucherInfoService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT, print);
        commands.put(AppCommandName.CHOICE_BY_DAY, choiceByDay);
        commands.put(AppCommandName.CHOICE_BY_MEAL, choiceByMeal);
        commands.put(AppCommandName.CHOICE_BY_VEHICLE, choiceByVehicle);
        commands.put(AppCommandName.SORT, sort);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        Voucher firstVoucher = new Voucher(7, MealType.ALL_INCLUSIVE, VehicleType.PLANE, VoucherType.EXCURSION);
        Voucher secondVoucher = new Voucher(7, MealType.HALF_BOARD, VehicleType.BUS, VoucherType.EXCURSION);
        Voucher thirdVoucher = new Voucher(14, MealType.ALL_INCLUSIVE, VehicleType.PLANE, VoucherType.EXCURSION);
        Voucher forthVoucher = new Voucher(14, MealType.BED_BREAKFAST, VehicleType.TRAIN, VoucherType.EXCURSION);
        Voucher fifthVoucher = new Voucher(14, MealType.ALL_INCLUSIVE, VehicleType.BUS, VoucherType.SANATORIUM_VOUCHER);
        Voucher sixthVoucher = new Voucher(21, MealType.ALL_INCLUSIVE, VehicleType.PLANE, VoucherType.SANATORIUM_VOUCHER);
        Voucher seventhVoucher = new Voucher(14, MealType.FULL_BOARD, VehicleType.PLANE, VoucherType.SANATORIUM_VOUCHER);
        Voucher eighthVoucher = new Voucher(3, MealType.BED_BREAKFAST, VehicleType.BUS, VoucherType.SHOPPING);
        Voucher ninthVoucher = new Voucher(4, MealType.BED_BREAKFAST, VehicleType.TRAIN, VoucherType.SHOPPING);
        Voucher temthVoucher = new Voucher(10, MealType.FULL_BOARD, VehicleType.PLANE, VoucherType.EXCURSION);

        List<Voucher> vouchers = new ArrayList<>();
        vouchers.add(firstVoucher);
        vouchers.add(secondVoucher);
        vouchers.add(thirdVoucher);
        vouchers.add(forthVoucher);
        vouchers.add(fifthVoucher);
        vouchers.add(sixthVoucher);
        vouchers.add(seventhVoucher);
        vouchers.add(eighthVoucher);
        vouchers.add(ninthVoucher);
        vouchers.add(temthVoucher);

        while (isRunning) {
            System.out.println(AppCommandName.PRINT);
            System.out.println(AppCommandName.SORT);
            System.out.println(AppCommandName.CHOICE_BY_DAY);
            System.out.println(AppCommandName.CHOICE_BY_VEHICLE);
            System.out.println(AppCommandName.CHOICE_BY_MEAL);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(vouchers, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }
}

