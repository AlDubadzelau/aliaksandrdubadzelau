package com.dubodelov.model;

public enum VehicleType {

    PLANE("Plane"),
    BUS("Bus"),
    TRAIN("Train");

    private String vehicleType;

    VehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    @Override
    public String toString() {
        return " Vehicle type= " + vehicleType;
    }
}
