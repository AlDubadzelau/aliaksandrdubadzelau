package com.dubodelov.utils.creator;

import com.dubodelov.exception.MealCreatorException;
import com.dubodelov.model.MealType;

import java.util.Scanner;

public class MealCreator {

    private final Scanner scan = new Scanner(System.in);
    private final static int EQUALS_ITEM = 0;
    private final static String EXCEPTION_MESSAGE = "WrongItem";

    public MealType create() throws MealCreatorException {

        System.out.println(MealType.ALL_INCLUSIVE);
        System.out.println(MealType.BED_BREAKFAST);
        System.out.println(MealType.FULL_BOARD);
        System.out.println(MealType.HALF_BOARD);

        MealType mealType;
        String type = scan.next();

        if (type.compareTo(MealType.ALL_INCLUSIVE.getMealType()) == EQUALS_ITEM) {
            mealType = MealType.ALL_INCLUSIVE;
        } else if (type.compareTo(MealType.FULL_BOARD.getMealType()) == EQUALS_ITEM) {
            mealType = MealType.FULL_BOARD;
        } else if (type.compareTo(MealType.HALF_BOARD.getMealType()) == EQUALS_ITEM) {
            mealType = MealType.HALF_BOARD;
        } else if (type.compareTo(MealType.BED_BREAKFAST.getMealType()) == EQUALS_ITEM) {
            mealType = MealType.BED_BREAKFAST;
        } else {
            throw new MealCreatorException(EXCEPTION_MESSAGE);
        }

        return mealType;

    }
}
