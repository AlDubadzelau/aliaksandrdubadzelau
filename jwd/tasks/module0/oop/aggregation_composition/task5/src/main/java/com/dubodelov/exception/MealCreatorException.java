package com.dubodelov.exception;

public class MealCreatorException extends Exception {

    public MealCreatorException(String message) {
        super(message);
    }

    public MealCreatorException(String message, Throwable cause) {
        super(message, cause);
    }

}
