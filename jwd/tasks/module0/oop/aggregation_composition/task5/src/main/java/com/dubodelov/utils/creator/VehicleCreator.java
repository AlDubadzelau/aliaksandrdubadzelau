package com.dubodelov.utils.creator;

import com.dubodelov.exception.VehicleCreatorException;
import com.dubodelov.model.VehicleType;

import java.util.Scanner;

public class VehicleCreator {

    private final Scanner scan = new Scanner(System.in);
    private final static int EQUALS_ITEM = 0;
    private final static String EXCEPTION_MESSAGE = "WrongItem";

    public VehicleType create() throws VehicleCreatorException {

        System.out.println(VehicleType.BUS);
        System.out.println(VehicleType.PLANE);
        System.out.println(VehicleType.TRAIN);

        VehicleType vehicleType;
        String type = scan.next();

        if (type.compareTo(VehicleType.BUS.getVehicleType()) == EQUALS_ITEM) {
            vehicleType = VehicleType.BUS;
        } else if (type.compareTo(VehicleType.PLANE.getVehicleType()) == EQUALS_ITEM) {
            vehicleType = VehicleType.PLANE;
        } else if (type.compareTo(VehicleType.TRAIN.getVehicleType()) == EQUALS_ITEM) {
            vehicleType = VehicleType.TRAIN;
        } else {
            throw new VehicleCreatorException(EXCEPTION_MESSAGE);
        }

        return vehicleType;

    }
}
