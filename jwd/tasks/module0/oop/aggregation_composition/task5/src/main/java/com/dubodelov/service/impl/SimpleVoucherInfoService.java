package com.dubodelov.service.impl;

import com.dubodelov.model.Voucher;
import com.dubodelov.service.api.VoucherInfoService;

public class SimpleVoucherInfoService implements VoucherInfoService {

    @Override
    public void printVoucher(Voucher voucher) {
        System.out.println(voucher);
    }
}
