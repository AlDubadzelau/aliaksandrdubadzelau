package com.dubodelov.command;

import com.dubodelov.model.Country;

public interface AppCommand {

    public void execute(Country country);
}
