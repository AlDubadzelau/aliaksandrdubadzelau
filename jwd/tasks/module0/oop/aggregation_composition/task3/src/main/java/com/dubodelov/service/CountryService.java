package com.dubodelov.service;

import com.dubodelov.model.City;
import com.dubodelov.model.Region;

import java.util.List;

public interface CountryService {

    public void printCity(City city);

    public void printAmountOfRegions(int amountOfRegions);

    public void printSquare(double square);

    public void printRegionsCenters(List<Region> regions);
}
