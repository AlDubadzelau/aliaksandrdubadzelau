package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.City;
import com.dubodelov.model.Country;
import com.dubodelov.service.CountryService;

public class PrinterCapitalCommand implements AppCommand {

    private final CountryService countryService;

    public PrinterCapitalCommand(CountryService countryService) {
        this.countryService = countryService;
    }

    @Override
    public void execute(Country country) {

        City city = country.getCapital();

        countryService.printCity(city);
    }
}
