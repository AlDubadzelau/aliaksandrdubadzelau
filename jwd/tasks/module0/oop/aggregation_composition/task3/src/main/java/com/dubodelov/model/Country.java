package com.dubodelov.model;

import java.util.List;

public class Country {

    private City capital;
    private List<Region> regions;
    private double square;

    public Country(String capitalName, int amountOfPeople, List<Region> regions, double square) {
        this.capital = new City(capitalName, amountOfPeople);
        this.regions = regions;
        this.square = square;
    }

    public City getCapital() {
        return capital;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public double getSquare() {
        return square;
    }

    @Override
    public String toString() {
        return "Country: " + "\n" +
                " Capital " + capital.toString() +
                regions.toString() +
                ", Square= " + square;
    }
}
