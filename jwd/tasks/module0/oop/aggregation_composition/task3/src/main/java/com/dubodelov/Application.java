package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.*;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.City;
import com.dubodelov.model.Country;
import com.dubodelov.model.Region;
import com.dubodelov.service.CountryService;
import com.dubodelov.service.impl.SimpleCountryService;

import java.util.*;

public class Application {
    public static void main(String[] args) {

        CountryService countryService = new SimpleCountryService();

        AppCommand printCapital = new PrinterCapitalCommand(countryService);
        AppCommand printSquare = new PrinterSquareCommand(countryService);
        AppCommand printAmountOfRegions = new PrinterAmountOfRegionCommand(countryService);
        AppCommand printRegionsCenters = new PrinterRegionCentersCommand(countryService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT_SQUARE, printSquare);
        commands.put(AppCommandName.PRINT_REGIONS_CENTERS, printRegionsCenters);
        commands.put(AppCommandName.PRINT_CAPITAL, printCapital);
        commands.put(AppCommandName.PRINT_AMOUNT_OF_REGIONS, printAmountOfRegions);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);

        //
        String capitalName = "Minsk";
        int amountOfPeople = 2000000;

        String[] namesOfRegions = {"Minskaya", "Mogilevskaya", "Brestskaya", "Grodnenskaya", "Vitebskaya", "Gomelskay"};
        String[] nameOfRegionsCenters = {"Minsk", "Mogilev", "Brest", "Grodno", "Vitebsk", "Gomel"};
        int[] population = {2000000, 400000, 310000, 380000, 370000, 490000};

        List<Region> regions = new ArrayList<>();
        for (int i = 0; i < nameOfRegionsCenters.length; i++) {
            Region region = new Region(namesOfRegions[i], nameOfRegionsCenters[i], population[i]);
            regions.add(region);
        }

        double square = 207595;
        //

        Country country = new Country(capitalName, amountOfPeople, regions, square);

        while (isRunning) {
            System.out.println(AppCommandName.PRINT_SQUARE);
            System.out.println(AppCommandName.PRINT_CAPITAL);
            System.out.println(AppCommandName.PRINT_REGIONS_CENTERS);
            System.out.println(AppCommandName.PRINT_AMOUNT_OF_REGIONS);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(country, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }

    }
}
