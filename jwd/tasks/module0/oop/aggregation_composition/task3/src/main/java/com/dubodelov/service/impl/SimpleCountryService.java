package com.dubodelov.service.impl;

import com.dubodelov.model.City;
import com.dubodelov.model.Region;
import com.dubodelov.service.CountryService;

import java.util.List;

public class SimpleCountryService implements CountryService {

    @Override
    public void printCity(City city) {
        System.out.println(city);
    }

    @Override
    public void printAmountOfRegions(int amountOfRegions) {
        System.out.println(amountOfRegions);
    }

    @Override
    public void printSquare(double square) {
        System.out.println(square);
    }

    @Override
    public void printRegionsCenters(List<Region> regions) {

        for (Region region : regions) {
            City city = region.getRegionCenter();
            printCity(city);
        }
    }
}
