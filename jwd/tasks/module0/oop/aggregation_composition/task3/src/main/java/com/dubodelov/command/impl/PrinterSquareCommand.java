package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Country;
import com.dubodelov.service.CountryService;

public class PrinterSquareCommand implements AppCommand {

    private final CountryService countryService;

    public PrinterSquareCommand(CountryService countryService) {
        this.countryService = countryService;
    }

    @Override
    public void execute(Country country) {

        double square = country.getSquare();

        countryService.printSquare(square);
    }
}
