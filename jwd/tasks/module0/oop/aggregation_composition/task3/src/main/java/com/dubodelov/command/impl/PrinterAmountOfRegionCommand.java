package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Country;
import com.dubodelov.service.CountryService;

public class PrinterAmountOfRegionCommand implements AppCommand {

    private final CountryService countryService;

    public PrinterAmountOfRegionCommand(CountryService countryService) {
        this.countryService = countryService;
    }

    @Override
    public void execute(Country country) {

        int amountOfRegions = country.getRegions().size();

        countryService.printAmountOfRegions(amountOfRegions);
    }
}
