package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Country;
import com.dubodelov.model.Region;
import com.dubodelov.service.CountryService;

import java.util.List;

public class PrinterRegionCentersCommand implements AppCommand {

    private final CountryService countryService;

    public PrinterRegionCentersCommand(CountryService countryService) {
        this.countryService = countryService;
    }

    @Override
    public void execute(Country country) {

        List<Region> regions = country.getRegions();

        countryService.printRegionsCenters(regions);
    }
}
