package com.dubodelov.model;

public class Region {

    private String name;
    private City regionCenter;

    public Region(String name, String nameOfCenter, int amountOfPeople) {
        this.name = name;
        this.regionCenter = new City(nameOfCenter, amountOfPeople);
    }

    public City getRegionCenter() {
        return regionCenter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Region: " + "\n" +
                " Name= " + name +
                " Center " + regionCenter.toString();
    }
}
