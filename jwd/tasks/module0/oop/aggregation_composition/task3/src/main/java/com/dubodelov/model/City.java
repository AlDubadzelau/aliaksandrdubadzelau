package com.dubodelov.model;

public class City {

    private String name;
    private int amountOfPeople;

    public City(String name, int amountOfPeople) {
        this.name = name;
        this.amountOfPeople = amountOfPeople;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmountOfPeople() {
        return amountOfPeople;
    }

    public void setAmountOfPeople(int amountOfPeople) {
        this.amountOfPeople = amountOfPeople;
    }

    @Override
    public String toString() {
        return "City: " +
                " Name= " + name +
                " People= " + amountOfPeople;
    }
}
