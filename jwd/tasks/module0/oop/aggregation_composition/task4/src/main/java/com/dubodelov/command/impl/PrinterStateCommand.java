package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Account;
import com.dubodelov.service.api.AccountInfoService;

import java.util.List;

public class PrinterStateCommand implements AppCommand {

    private final AccountInfoService accountInfoService;

    public PrinterStateCommand(AccountInfoService accountInfoService) {
        this.accountInfoService = accountInfoService;
    }

    @Override
    public void execute(List<Account> accounts) {

        for (Account account : accounts) {
            boolean state = account.isBlock();
            accountInfoService.printCondition(state);
        }
    }
}
