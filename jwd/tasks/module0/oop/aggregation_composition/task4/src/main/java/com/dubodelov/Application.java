package com.dubodelov;

import com.dubodelov.command.AppCommand;
import com.dubodelov.command.factory.AppCommandFactory;
import com.dubodelov.command.factory.impl.SimpleAppCommandFactory;
import com.dubodelov.command.impl.*;
import com.dubodelov.controller.AppController;
import com.dubodelov.model.Account;
import com.dubodelov.service.api.AccountInfoService;
import com.dubodelov.service.api.AccountService;
import com.dubodelov.service.impl.SimpleAccountInfoService;
import com.dubodelov.service.impl.SimpleAccountService;

import java.math.BigDecimal;
import java.util.*;

public class Application {

    public static void main(String[] args) {

        AccountInfoService accountInfoService = new SimpleAccountInfoService();
        AccountService accountService = new SimpleAccountService();

        AppCommand printInfo = new PrinterInfoCommand(accountInfoService);
        AppCommand printState = new PrinterStateCommand(accountInfoService);
        AppCommand findSum = new SummationCommand(accountService, accountInfoService);
        AppCommand findPositiveSum = new PositiveSummationCommand(accountService, accountInfoService);
        AppCommand findNegativeSum = new NegativeSummationCommand(accountService, accountInfoService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commands.put(AppCommandName.PRINT_INFO, printInfo);
        commands.put(AppCommandName.PRINT_STATE, printState);
        commands.put(AppCommandName.FIND_FULL_SUM, findSum);
        commands.put(AppCommandName.FIND_NEGATIVE_SUM, findNegativeSum);
        commands.put(AppCommandName.FIND_POSITIVE_SUM, findPositiveSum);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;
        Scanner scan = new Scanner(System.in);
        Account firstAccount = new Account(new BigDecimal(1234), true);
        Account fivesAccount = new Account(new BigDecimal(-1234), true);
        Account forthAccount = new Account(new BigDecimal(1234));
        Account secondAccount = new Account(new BigDecimal(-1000));
        Account thirdAccount = new Account(new BigDecimal(9999));
        List<Account> accounts = new ArrayList<>();
        accounts.add(firstAccount);
        accounts.add(secondAccount);
        accounts.add(thirdAccount);
        accounts.add(forthAccount);
        accounts.add(fivesAccount);

        while (isRunning) {
            System.out.println(AppCommandName.FIND_FULL_SUM);
            System.out.println(AppCommandName.FIND_NEGATIVE_SUM);
            System.out.println(AppCommandName.FIND_POSITIVE_SUM);
            System.out.println(AppCommandName.PRINT_INFO);
            System.out.println(AppCommandName.PRINT_STATE);
            System.out.println("-q");
            System.out.println("Enter commands: ");
            String command = scan.next();

            controller.handleUserData(accounts, command);

            if (command.equalsIgnoreCase("-q")) {
                isRunning = false;
            }
        }
    }

}
