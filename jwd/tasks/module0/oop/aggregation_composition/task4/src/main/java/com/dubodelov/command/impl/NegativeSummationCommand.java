package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Account;
import com.dubodelov.service.api.AccountInfoService;
import com.dubodelov.service.api.AccountService;

import java.math.BigDecimal;
import java.util.List;

public class NegativeSummationCommand implements AppCommand {

    private final AccountService accountService;
    private final AccountInfoService accountInfoService;

    public NegativeSummationCommand(AccountService accountService, AccountInfoService accountInfoService) {
        this.accountService = accountService;
        this.accountInfoService = accountInfoService;
    }

    @Override
    public void execute(List<Account> accounts) {

        BigDecimal sum = accountService.findNegativeSum(accounts);

        System.out.println(sum);
    }
}
