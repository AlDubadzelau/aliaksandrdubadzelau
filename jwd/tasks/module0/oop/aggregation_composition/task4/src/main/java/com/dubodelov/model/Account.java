package com.dubodelov.model;

import java.math.BigDecimal;

public class Account {

    private final boolean DEFAULT_IS_BLOCK = false;

    private BigDecimal money;
    private boolean isBlock = DEFAULT_IS_BLOCK;

    public Account(BigDecimal money) {
        this.money = money;
    }

    public Account(BigDecimal money, boolean isBlock) {
        this.money = money;
        this.isBlock = isBlock;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public boolean isBlock() {
        return isBlock;
    }

    public void setBlock(boolean block) {
        isBlock = block;
    }

    @Override
    public String toString() {
        return "Money= " + money +
                "Is block= " + isBlock;
    }
}
