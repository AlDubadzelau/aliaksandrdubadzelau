package com.dubodelov.service.impl;

import com.dubodelov.model.Account;
import com.dubodelov.service.api.AccountService;

import java.math.BigDecimal;
import java.util.List;

public class SimpleAccountService implements AccountService {

    private final static int DEFAULT_SUM = 0;

    @Override
    public BigDecimal findFullSum(List<Account> accounts) {

        BigDecimal sum = new BigDecimal(DEFAULT_SUM);

        for (Account account : accounts) {

            BigDecimal money = account.getMoney();

            if (!account.isBlock()) {
                sum = sum.add(money);
            }
        }

        return sum;
    }

    @Override
    public BigDecimal findNegativeSum(List<Account> accounts) {

        BigDecimal sum = new BigDecimal(DEFAULT_SUM);

        for (Account account : accounts) {

            BigDecimal money = account.getMoney();

            if (money.compareTo(BigDecimal.valueOf(DEFAULT_SUM)) < DEFAULT_SUM && !account.isBlock()) {
                sum = sum.add(money);
            }
        }

        return sum;
    }

    @Override
    public BigDecimal findPositiveSum(List<Account> accounts) {

        BigDecimal sum = new BigDecimal(DEFAULT_SUM);

        for (Account account : accounts) {

            BigDecimal money = account.getMoney();

            if (money.compareTo(BigDecimal.valueOf(DEFAULT_SUM)) > DEFAULT_SUM && !account.isBlock()) {
                sum = sum.add(money);
            }
        }

        return sum;
    }
}
