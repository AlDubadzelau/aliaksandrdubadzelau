package com.dubodelov.command.impl;

import com.dubodelov.command.AppCommand;
import com.dubodelov.model.Account;
import com.dubodelov.service.api.AccountInfoService;

import java.util.List;

public class PrinterInfoCommand implements AppCommand {

    private final AccountInfoService accountInfoService;

    public PrinterInfoCommand(AccountInfoService accountInfoService) {
        this.accountInfoService = accountInfoService;
    }

    @Override
    public void execute(List<Account> accounts) {

        for (Account account : accounts) {
            accountInfoService.printInfo(account);
        }
    }
}
