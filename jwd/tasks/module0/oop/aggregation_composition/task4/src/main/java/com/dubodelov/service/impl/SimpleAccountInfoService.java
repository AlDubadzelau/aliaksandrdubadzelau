package com.dubodelov.service.impl;

import com.dubodelov.model.Account;
import com.dubodelov.service.api.AccountInfoService;

public class SimpleAccountInfoService implements AccountInfoService {

    @Override
    public void printInfo(Account account) {
        System.out.println(account);
    }

    @Override
    public void printCondition(boolean isBlock) {
        System.out.println(isBlock);
    }
}
