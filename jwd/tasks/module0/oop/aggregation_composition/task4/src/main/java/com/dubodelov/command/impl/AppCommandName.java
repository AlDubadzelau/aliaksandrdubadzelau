package com.dubodelov.command.impl;

public enum AppCommandName {

    PRINT_INFO("-pi"),
    PRINT_STATE("-ps"),
    FIND_FULL_SUM("-ffs"),
    FIND_POSITIVE_SUM("-fps"),
    FIND_NEGATIVE_SUM("-fns");

    private static final AppCommandName DEFAULT_VALUE = null;
    private final String shortCommand;

    AppCommandName(String shortCommand) {
        this.shortCommand = shortCommand;
    }


    public static AppCommandName fromString(String name) {

        AppCommandName command = DEFAULT_VALUE;
        final AppCommandName[] values = AppCommandName.values();

        for (AppCommandName commandName : values) {
            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
                command = commandName;
            }
        }

        return command;
    }

    @Override
    public String toString() {
        return shortCommand;
    }

}
