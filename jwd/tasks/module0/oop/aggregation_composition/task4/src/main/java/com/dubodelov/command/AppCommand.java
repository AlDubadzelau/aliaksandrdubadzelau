package com.dubodelov.command;

import com.dubodelov.model.Account;

import java.util.List;

public interface AppCommand {

    public void execute(List<Account> accounts);
}
