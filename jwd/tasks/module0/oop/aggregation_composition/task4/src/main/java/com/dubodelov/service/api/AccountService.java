package com.dubodelov.service.api;

import com.dubodelov.model.Account;

import java.math.BigDecimal;
import java.util.List;

public interface AccountService {

    public BigDecimal findFullSum(List<Account> accounts);

    public BigDecimal findNegativeSum(List<Account> accounts);

    public BigDecimal findPositiveSum(List<Account> accounts);
}
