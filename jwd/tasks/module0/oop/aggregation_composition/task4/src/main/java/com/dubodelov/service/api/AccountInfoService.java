package com.dubodelov.service.api;

import com.dubodelov.model.Account;

public interface AccountInfoService {

    void printInfo(Account account);

    void printCondition(boolean isBlock);
}
