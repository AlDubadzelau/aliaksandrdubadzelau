package logic;

public class LinearAlgorithm {

    private static final int DEFAULT_STEP = 1;
    private static final double DEFAULT_FIRST_NUM = 1;
    private static final double DEFAULT_SECOND_NUM = 2;
    private static final double DEFAULT_THIRD_NUM = 3;
    private static final double DEFAULT_FOURTH_NUM = 4;
    private static final double DEFAULT_FIFVES_NUM = 5;
    private static final int DEFAULT_SIXES_NUM = 6;
    private static final int DEFAULT_DIVIDER_10 = 10;
    private static final int DEFAULT_DIVIDER_100 = 100;
    private static final int DEFAULT_DIVIDER_1000 = 1000;
    private static final int AMOUNT_OF_BYTES = 1024;
    private static final double DEFAULT_AMOUNT_OF_MILKS = 80;
    private static final double DEFAULT_INCREMENTAL_VALUE = 12;
    private static final double DEFAULT_GRADE = 180;
    private static final double AMOUNT_OF_SECONDS = 60;
    private static final double AMOUNT_OF_MINUTES = 60;

    //1
    public static double sum(double firstNum, double secondNum) {
        return firstNum + secondNum;
    }

    public static double subtract(double firstNum, double secondNum) {
        return firstNum - secondNum;
    }

    public static double multiply(double firstNum, double secondNum) {
        return firstNum * secondNum;
    }

    public static double division(double dividend, double divisor) {
        return dividend / divisor;
    }

    //2
    public static double findFirstFunction(double variable) {
        return DEFAULT_THIRD_NUM + variable;
    }

    //3
    public static double findSecondFunction(double firstNum, double secondNum) {
        return DEFAULT_SECOND_NUM * firstNum + (secondNum - DEFAULT_SECOND_NUM) * DEFAULT_FIFVES_NUM;
    }

    //4
    public static double findThirdFunctions(double firstNum, double secondNum, double thirdNum) {
        return ((firstNum - DEFAULT_THIRD_NUM) * secondNum / DEFAULT_SECOND_NUM) + thirdNum;
    }

    //5
    public static double findArithmeticMean(double firstNum, double secondNum) {
        return (firstNum + secondNum) / DEFAULT_SECOND_NUM;
    }

    //6
    public static double findSolution(int amountOfLittleBuckets, int amountOfLargeBuckets) {

        double valueOfLittleBucket = DEFAULT_AMOUNT_OF_MILKS / amountOfLittleBuckets;
        double valueOfLargeBucket = valueOfLittleBucket + DEFAULT_INCREMENTAL_VALUE;

        return amountOfLargeBuckets * valueOfLargeBucket;
    }

    //7
    public static double findSquare(double width) {
        double length = findLength(width);

        return length * width;
    }

    public static double findLength(double width) {
        return width * DEFAULT_FOURTH_NUM;
    }

    //8
    public static double findForthFunction(double firstNum, double secondNum, double thirdNum) {
        double thirdPart = DEFAULT_FIRST_NUM / (secondNum * secondNum);
        double secondPart = firstNum * firstNum * firstNum * thirdNum;
        double middlePart = secondNum * secondNum + Math.sqrt(DEFAULT_FOURTH_NUM * firstNum * thirdNum);
        double firstPart = (secondNum + (middlePart)) / (DEFAULT_SECOND_NUM * firstNum);

        return firstPart - secondPart + thirdPart;
    }

    //9
    public static double findFivesFunction(double firstNum, double secondNum, double thirdNum, double forthNum) {
        double firstPart = firstNum / thirdNum;
        double secondPart = secondNum / forthNum;
        double thirdPart = (firstNum * secondNum - thirdNum) / (thirdNum * forthNum);

        return firstPart * secondPart - thirdPart;
    }

    //10
    public static double findSixesFunction(double firstNum, double secondNum) {
        double firstPart = Math.sin(firstNum) + Math.cos(secondNum);
        double secondPart = Math.cos(firstNum) - Math.sin(secondNum);
        double thirdPart = Math.tan(firstNum * secondNum);

        return (firstPart / secondPart) * thirdPart;
    }

    //11
    public static double findTrianglePerimeter(double firstSide, double secondSide) {
        double thirdSide = findThirdSide(firstSide, secondSide);

        return firstSide + secondSide + thirdSide;
    }

    public static double findTriangleSquare(double firstSide, double secondSide) {
        return firstSide * secondSide / DEFAULT_SECOND_NUM;
    }

    private static double findThirdSide(double firstSide, double secondSide) {
        return Math.sqrt(firstSide * firstSide - secondSide * secondSide);
    }

    //12
    public static double findDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

    //13
    public static double findTrianglePerimeter(double x1, double y1, double x2, double y2, double x3, double y3) {
        double firstSide = findDistance(x1, y1, x2, y2);
        double secondSide = findDistance(x1, y1, x3, y3);
        double thirdSide = findDistance(x2, y2, x3, y3);

        return firstSide + secondSide + thirdSide;
    }

    public static double findTriangleSquare(double x1, double y1, double x2, double y2, double x3, double y3) {
        double firstSide = findDistance(x1, y1, x2, y2);
        double secondSide = findDistance(x1, y1, x3, y3);
        double thirdSide = findDistance(x2, y2, x3, y3);
        double semiPerimeter = findTrianglePerimeter(x1, y1, x2, y2, x3, y3) * DEFAULT_SECOND_NUM;

        return Math.sqrt(semiPerimeter * (firstSide - semiPerimeter) * (secondSide - semiPerimeter) * (thirdSide - semiPerimeter));
    }

    //14
    public static double findCircleSquare(double radius) {
        return Math.PI * radius * radius;
    }

    public static double findLengthOfCircle(double radius) {
        return DEFAULT_SECOND_NUM * Math.PI * radius;
    }

    //15
    public static double findFirstPowerOfPI() {
        return Math.PI;
    }

    public static double findSecondPowerOfPI() {
        return Math.PI * Math.PI;
    }

    public static double findThirdPowerOfPI() {
        return findSecondPowerOfPI() * Math.PI;
    }

    public static double findForthPowerOfPI() {
        return findThirdPowerOfPI() * Math.PI;
    }

    //16
    public static int findSumOfNumbers(int number) {

        int sum = 0;

        for (int i = 0; i < DEFAULT_FOURTH_NUM; i++) {
            int digital = number % DEFAULT_DIVIDER_10;
            number /= DEFAULT_DIVIDER_10;
            sum += digital;
        }

        return sum;
    }

    //17
    public static double findAverageArithmeticMean(double firstNum, double secondNum) {
        return Math.pow(firstNum, DEFAULT_THIRD_NUM) + Math.pow(secondNum, DEFAULT_THIRD_NUM) / DEFAULT_SECOND_NUM;
    }

    public static double findAverageGeometricalMean(double firstNum, double secondNum) {
        return Math.abs(firstNum * secondNum / DEFAULT_SECOND_NUM);
    }

    //18
    public static double findValueOfCube(double side) {
        return side * side * side;
    }

    public static double findTotalSurfaceSquare(double side) {
        return findSquareOfFace(side) * DEFAULT_SIXES_NUM;
    }

    public static double findSquareOfFace(double side) {
        return side * side;
    }

    //19
    public static double findSquareOfEquilateralTriangle(double side) {
        return side * side * Math.sqrt(DEFAULT_THIRD_NUM) / DEFAULT_FOURTH_NUM;
    }

    public static double findHeightOfEquilateralTriangle(double side) {
        return side * Math.sqrt(DEFAULT_THIRD_NUM) / DEFAULT_SECOND_NUM;
    }

    public static double findRadiusOfEnteredCircle(double side) {
        return side * Math.sqrt(DEFAULT_THIRD_NUM) / DEFAULT_SIXES_NUM;
    }

    public static double findRadiusOfCircumscribedCircle(double side) {
        return side * Math.sqrt(DEFAULT_THIRD_NUM) / DEFAULT_THIRD_NUM;
    }

    //20
    public static double findSquareOfCircle(double length) {
        double radius = findRadius(length);

        return Math.PI * radius * radius;
    }

    private static double findRadius(double length) {
        return length / Math.PI / DEFAULT_SECOND_NUM;
    }

    //21


    //22


    //23

    public static double findRingSquare(double biggestRadius, double lowestRadius) {
        return findCircleSquare(biggestRadius) - findCircleSquare(lowestRadius);
    }

    //24

    public static double findTrapezoidSquare(double largestSide, double smallestSide, double corner) {
        double heights = (largestSide - smallestSide) * Math.tan(corner);

        return (largestSide + smallestSide) * heights / DEFAULT_SECOND_NUM;
    }

    //25

    public static double[] findRoots(double a, double b, double c) {
        double discriminant = findDiscriminant(a, b, c);
        double firstDecision = Math.sqrt((b + discriminant) / (DEFAULT_SECOND_NUM * a));
        double secondDecision = Math.sqrt((b - discriminant) / (DEFAULT_SECOND_NUM * a));

        return new double[]{firstDecision, secondDecision};
    }

    private static double findDiscriminant(double a, double b, double c) {
        return b * b - DEFAULT_FOURTH_NUM * a * c;
    }

    //26

    public static double foundTriangleSquare(double firstSide, double secondSide, double corner) {
        return firstSide * secondSide * Math.sin(corner) / DEFAULT_SECOND_NUM;
    }

    //27

    public static double findEightsPowerOfNumber(double number) {
        number *= number;
        number *= number;
        number *= number;

        return number;
    }

    public static double findTensPowerOfNumber(double number) {
        double resultNumber = number * number;

        resultNumber *= resultNumber;
        resultNumber *= number;
        resultNumber *= resultNumber;

        return resultNumber;
    }

    //28

    public static double translateAngleInGrades(double corner) {
        return corner * DEFAULT_GRADE / Math.PI;
    }

    public static double translateAngleInMinutes(double corner) {
        return translateAngleInGrades(corner) * AMOUNT_OF_MINUTES;
    }

    public static double translateAngleInSeconds(double corner) {
        return translateAngleInMinutes(corner) * AMOUNT_OF_SECONDS;
    }


    //29

    public static double[] findCornersInRadians(double firstSide, double secondSide, double thirdSide) {
        double cosOfFirstCorner = findCosOfCorner(firstSide, secondSide, thirdSide);
        double cosOfSecondCorner = findCosOfCorner(secondSide, firstSide, thirdSide);
        double cosOfThirdCorner = findCosOfCorner(thirdSide, firstSide, secondSide);

        double firstCornerInRadians = Math.acos(cosOfFirstCorner);
        double secondCornerInRadians = Math.acos(cosOfSecondCorner);
        double thirdCornerInRadians = Math.acos(cosOfThirdCorner);

        return new double[]{firstCornerInRadians, secondCornerInRadians, thirdCornerInRadians};
    }

    public static double[] findCornersInGrades(double firstSide, double secondSide, double thirdSide) {
        double[] corners = findCornersInRadians(firstSide, secondSide, thirdSide);

        for (int i = 0; i < corners.length; i++) {
            corners[i] = translateGradesInRadian(corners[i]);
        }

        return corners;
    }

    private static double translateGradesInRadian(double cornerInRadians) {
        return cornerInRadians * DEFAULT_GRADE / Math.PI;
    }

    private static double findCosOfCorner(double firstSide, double secondSide, double thirdSide) {
        return (firstSide * firstSide - secondSide * secondSide - thirdSide * thirdSide) / (-DEFAULT_SECOND_NUM * thirdSide * secondSide);
    }

    //30

    public static double findFullResistance(double firstResistance, double secondResistance, double thirdResistance) {
        double firstOppositeResistanceValue = findOppositeResistanceValue(firstResistance);
        double secondOppositeResistanceValue = findOppositeResistanceValue(secondResistance);
        double thirdOppositeResistanceValue = findOppositeResistanceValue(thirdResistance);

        double oppositeResistance = firstOppositeResistanceValue + secondOppositeResistanceValue + thirdOppositeResistanceValue;

        return findOppositeResistanceValue(oppositeResistance);
    }

    private static double findOppositeResistanceValue(double resistance) {
        return DEFAULT_FIRST_NUM / resistance;
    }

    //31

    public static double findFullDistance(double boatSpeed, double riverSpeed, double lakeTime, double riverTime) {

        double lakeDistance = boatSpeed * lakeTime;
        double totalSpeed = findSpeedInRiver(boatSpeed, riverSpeed);
        double riverDistance = totalSpeed * riverTime;

        return lakeDistance + riverDistance;
    }

    private static double findSpeedInRiver(double boatSpeed, double riverSpeed) {
        return boatSpeed - riverSpeed;
    }

    //32


    //33

    public static int checkSymbol(char symbol) {
        return (int) symbol;
    }

    public static int checkPreviousSymbol(char symbol) {
        return checkSymbol(symbol) - DEFAULT_STEP;
    }

    public static int checkNextSymbol(char symbol) {
        return checkSymbol(symbol) + DEFAULT_STEP;
    }

    //34

    public static double convertToKBytes(double amountOfBytes) {
        return amountOfBytes / AMOUNT_OF_BYTES;
    }

    //35

    public static int findMinorNumber(double firstNumber, double secondNumber) {

        int result = (int) (firstNumber / secondNumber);

        return result % DEFAULT_DIVIDER_10;
    }

    public static double findMajorNumber(double firstNumber, double secondNumber) {

        double result = firstNumber / secondNumber;

        return result * DEFAULT_DIVIDER_10 % DEFAULT_DIVIDER_10;

    }

    //36

    public static double findSevensFunction(int number) {

        double dividend = (number / DEFAULT_DIVIDER_100 % DEFAULT_DIVIDER_10) * (number % DEFAULT_DIVIDER_10);
        double divider = (number / DEFAULT_DIVIDER_1000) * (number / DEFAULT_DIVIDER_10 % DEFAULT_DIVIDER_10);

        return dividend / divider;
    }


    //37


    //38


    //39


    public static double findEightsFunction(double number) {

        double interimResult = (DEFAULT_SECOND_NUM * number - DEFAULT_THIRD_NUM) * number + DEFAULT_FOURTH_NUM; //4

        return (interimResult * number - DEFAULT_FIFVES_NUM) * number + DEFAULT_SIXES_NUM; //4
    }


    //40

    public static double findNinesFunction(double number){
        return -((DEFAULT_FOURTH_NUM * number - DEFAULT_THIRD_NUM) * number + DEFAULT_SECOND_NUM) * number;
    }

    public static double findTensFunction(double number){

        double interimResult = (DEFAULT_FOURTH_NUM * number + DEFAULT_THIRD_NUM) * number + DEFAULT_SECOND_NUM;

        return interimResult * number + DEFAULT_FIRST_NUM;
    }

}