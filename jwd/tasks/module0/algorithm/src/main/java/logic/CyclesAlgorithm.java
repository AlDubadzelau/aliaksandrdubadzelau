package logic;

import view.Printer;

public class CyclesAlgorithm {

    private static final int DEFAULT_NUMBER = 100;
    private static final int DEFAULT_ASCII_NUMBER = 255;
    private static final int DEFAULT_THOUSAND_NUMBER = 1000;
    private static final int DEFAULT_BORDER_NUMBER = 10000;
    private static final int DEFAULT_TEN_DIVIDER = 10;
    private static final int DEFAULT_HUNDRED_DIVIDER = 100;
    private static final int DEFAULT_THOUSAND_DIVIDER = 1000;
    private static final int DEFAULT_FIFTEEN_NUMBER = 15;
    private static final double DEFAULT_ZERO_NUMBER = 0;
    private static final double DEFAULT_FIRST_NUMBER = 1;
    private static final double DEFAULT_SECOND_NUMBER = 2;
    private static final double DEFAULT_ELEVENTH_NUMBER = 11;
    private static final double DEFAULT_FACTOR = 3;

    //1
    public static void createNumbers() {
        for (int i = 1; i <= 5; i++) {
            Printer.printInfo(i);
        }
    }

    //2
    public static void createBackNumber() {
        for (int i = 5; i > 0; i--) {
            Printer.printInfo(i);
        }
    }

    //3
    public static void printTable() {
        for (int i = 0; i < 10; i++) {
            Printer.printInfo(i + " * " + DEFAULT_FACTOR + " = " + i * DEFAULT_FACTOR);
        }
    }

    //4
    public static void printNumbers() {
        double number = DEFAULT_NUMBER;

        while (number > DEFAULT_FIRST_NUMBER) {
            if (number % DEFAULT_SECOND_NUMBER == DEFAULT_ZERO_NUMBER) {
                Printer.printInfo(number);
            }

            number = number - DEFAULT_SECOND_NUMBER;
        }
    }

    //9
    public static void printSecondPowerNumbers() {
        for (int i = 0; i < DEFAULT_NUMBER; i++) {
            Printer.printInfo(i * i);
        }
    }

    //15
    public static double findSecondFunction() {
        double sum = DEFAULT_FIRST_NUMBER;

        for (int i = 1; i <= DEFAULT_TEN_DIVIDER; i++) {
            sum += Math.pow(2.0, i);
        }

        return sum;
    }

    //16
    public static double findFirstFunction() {
        double sum = DEFAULT_FIRST_NUMBER;
        double result = DEFAULT_FIRST_NUMBER;
        double i = DEFAULT_SECOND_NUMBER;
        while (i < DEFAULT_ELEVENTH_NUMBER) {
            sum += i;
            result *= sum;
            i++;
        }

        return result;
    }

    //25
    public static double findFactorial(double number) {
        double result = DEFAULT_FIRST_NUMBER;

        for (int i = 1; i <= number; i++) {
            result = result * i;
        }

        return result;
    }

    //26
    public static void printASCII() {
        for (int i = 0; i <= DEFAULT_ASCII_NUMBER; i++) {
            Printer.printInfo(i + " - " + (char) i);
        }
    }

    //34
    public static void findCertainNumbers() {
        for (int i = DEFAULT_THOUSAND_NUMBER; i < DEFAULT_BORDER_NUMBER; i++) {
            if (i % DEFAULT_TEN_DIVIDER + i / DEFAULT_TEN_DIVIDER % DEFAULT_TEN_DIVIDER + i / DEFAULT_HUNDRED_DIVIDER % DEFAULT_TEN_DIVIDER + i / DEFAULT_THOUSAND_DIVIDER == DEFAULT_FIFTEEN_NUMBER) {
                Printer.printInfo(i);
            }
        }
    }


}
