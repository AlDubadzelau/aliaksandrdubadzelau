package logic;

public class BranchingAlgorithm {

    private static final int DEFAULT_ZERO_NUMBER = 0;
    private static final int DEFAULT_FIRST_NUMBER = 1;
    private static final int DEFAULT_SECOND_NUMBER = 2;
    private static final int DEFAULT_THIRD_NUMBER = 3;
    private static final int DEFAULT_FORTH_NUMBER = 4;
    private static final int DEFAULT_SEVENS_NUMBER = 7;
    private static final int DEFAULT_EIGHTS_NUMBER = 8;
    private static final int DEFAULT_MAX_MONTH = 12;
    private static final int DEFAULT_MAX_DAYS = 31;
    private static final String DEFAULT_POSITIVE_ANSWER = "YES";
    private static final String DEFAULT_NEGATIVE_ANSWER = "NO";
    private static final String DEFAULT_ANSWER_BOY_MSG = "I like boys!";
    private static final String DEFAULT_ANSWER_GIRL_MSG = "I like girls!";
    private static final String MESSAGE_GIRL = "G";
    private static final String MESSAGE_BOY = "B";
    private static final String DEFAULT_ANSWER = "Something went wrong";
    private static final String FIRED_MESSAGE = "Fire situation";
    private static final String UNFIRED_MESSAGE = "Normal situation";
    private static final double[] DEFAULT_DOT = new double[]{0,0};
    private static final double DEFAULT_GRADE = 90;
    private static final double DEFAULT_MAX_GRADE = 180;
    private static final double MAX_TEMPERATURE = 60;



    //1

    public static int compareNumbers() {
        int result;

        if (DEFAULT_FIRST_NUMBER < DEFAULT_SECOND_NUMBER) {
            result = DEFAULT_SEVENS_NUMBER;
        } else {
            result = DEFAULT_EIGHTS_NUMBER;
        }

        return result;
    }

    //2

    public static String compareNumber() {
        String result;

        if (DEFAULT_FIRST_NUMBER < DEFAULT_SECOND_NUMBER) {
            result = DEFAULT_POSITIVE_ANSWER;
        } else {
            result = DEFAULT_NEGATIVE_ANSWER;
        }

        return result;
    }

    //3

    public static String compareNumbers(double number) {

        String result;

        if (number < DEFAULT_THIRD_NUMBER) {
            result = DEFAULT_POSITIVE_ANSWER;
        } else {
            result = DEFAULT_NEGATIVE_ANSWER;
        }

        return result;
    }

    //4

    public static boolean compareNumbers(double firstNumber, double secondNumber) {
        return firstNumber == secondNumber;
    }

    //5

    public static double findSmallestNumber(double firstNumber, double secondNumber) {
        return Math.min(firstNumber, secondNumber);
    }

    //6

    public static double findLargestNumber(double firstNumber, double secondNumber) {
        return Math.max(firstNumber, secondNumber);
    }

    //7

    public static double resolveFunction(double a, double b, double c, double x) {
        return Math.abs(a * x * x + b * x + c);
    }

    //8

    public static double findSmallestSecondPowerNumber(double firstNumber, double secondNumber) {
        return Math.min(firstNumber * firstNumber, secondNumber * secondNumber);
    }

    //9

    public static boolean checkEquilateralTriangle(double firstSide, double secondSide, double thirdSide) {
        return firstSide == secondSide && firstSide == thirdSide;
    }

    //10

    public static double findSmallestCircleSquare(double firstSquare, double secondSquare){
        return Math.min(firstSquare, secondSquare);
    }

    //11

    public static double findSmallestQuadrateSquare(double firstSquare, double secondSquare) {
        return Math.min(firstSquare, secondSquare);
    }

    //12

    public static double[] upgradeNumbers(double firstNumber, double secondNumber, double thirdNumber) {

        firstNumber = raise(firstNumber);
        secondNumber = raise(secondNumber);
        thirdNumber = raise(thirdNumber);

        return new double[]{firstNumber, secondNumber, thirdNumber};

    }

    private static double raise(double number) {

        if (number < DEFAULT_ZERO_NUMBER) {
            number = Math.pow(number, DEFAULT_FORTH_NUMBER);
        } else {
            number = Math.pow(number, DEFAULT_FORTH_NUMBER);
        }

        return number;

    }

    //13

    public static double[] findSmallestDistance(double[] firstDot, double[] secondDot){

        double firstDistance = findDistance(firstDot);
        double secondDistance = findDistance(secondDot);
        double[] result;

        if(firstDistance < secondDistance){
            result = firstDot;
        }
        else {
            result = secondDot;
        }

        return result;
    }

    private static double findDistance(double[] dot){
        return Math.sqrt((dot[0] - DEFAULT_DOT[0]) * (dot[0] - DEFAULT_DOT[0]) + (dot[1] - DEFAULT_DOT[1]) * (dot[1] - DEFAULT_DOT[1]));
    }

    //14

    public static boolean[] checkTriangle(double firstConner, double secondConner){

        boolean[] result = new boolean[DEFAULT_SECOND_NUMBER];

        if(firstConner + secondConner < DEFAULT_MAX_GRADE){
            result[DEFAULT_ZERO_NUMBER] = true;
            result[DEFAULT_FIRST_NUMBER] = checkRightTriangle(firstConner, secondConner);
        }

        return result;
    }

    private static boolean checkRightTriangle(double firstConner, double secondConner) {

        boolean result;

        if (firstConner + secondConner == DEFAULT_GRADE) {
            result = true;
        } else if (firstConner == DEFAULT_GRADE || secondConner == DEFAULT_GRADE) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    //15

    public static double[] upgradeNumbers(double firstNumber, double secondNumber){

        if(firstNumber < secondNumber){
            firstNumber = (firstNumber + secondNumber) / DEFAULT_SECOND_NUMBER;
            secondNumber = firstNumber * secondNumber * DEFAULT_SECOND_NUMBER;
        }
        else{
            secondNumber = (firstNumber + secondNumber) / DEFAULT_SECOND_NUMBER;
            firstNumber = firstNumber * secondNumber * DEFAULT_SECOND_NUMBER;
        }

        return new double[]{firstNumber, secondNumber};
    }





    //16





    //17

    public static double[] replaceNumbers(double firstNumber, double secondNumber){
        if(secondNumber == firstNumber){
            firstNumber = DEFAULT_ZERO_NUMBER;
            secondNumber = DEFAULT_ZERO_NUMBER;
        }
        else{
            firstNumber = Math.max(firstNumber, secondNumber);
            secondNumber = firstNumber;
        }

        return new double[]{firstNumber, secondNumber};
    }

    //18

    public static int findAmountOfNegativeNumbers(double firstNumber, double secondNumber, double thirdNumber){

        int result = 0;

        if (firstNumber < DEFAULT_ZERO_NUMBER){
            result++;
        }
        if (secondNumber < DEFAULT_ZERO_NUMBER){
            result++;
        }
        if(thirdNumber < DEFAULT_ZERO_NUMBER){
            result++;
        }

        return result;
    }

    //19

    public static int findAmountOfPositiveNumbers(double firstNumber, double secondNumber, double thirdNumber){

        int result = 0;

        if (firstNumber > DEFAULT_ZERO_NUMBER){
            result++;
        }
        if (secondNumber > DEFAULT_ZERO_NUMBER){
            result++;
        }
        if(thirdNumber > DEFAULT_ZERO_NUMBER){
            result++;
        }

        return result;
    }





    //20





    //21

    public static String flatter(String answer){

        String result;

        if(answer.equalsIgnoreCase(MESSAGE_GIRL)){
            result = DEFAULT_ANSWER_GIRL_MSG;
        }
        else if(answer.equalsIgnoreCase(MESSAGE_BOY)){
            result = DEFAULT_ANSWER_BOY_MSG;
        }
        else{
            result = DEFAULT_ANSWER;
        }

        return result;
    }

    //22

    public static double[] replaceValue(double x, double y){

        if(x < y){
            x = x + y;
            y = x - y;
            y = x - y;
        }

        return new double[]{x, y};
    }

    //23

    public static boolean checkDate(int day, int month){

        return day >= DEFAULT_FIRST_NUMBER && month >= DEFAULT_FIRST_NUMBER && day <= DEFAULT_MAX_DAYS && month <= DEFAULT_MAX_MONTH;
    }

    //24

    public static boolean checkYourLove(int amountOfPetal){

        return amountOfPetal % DEFAULT_SECOND_NUMBER == DEFAULT_ZERO_NUMBER;
    }

    //25

    public static String checkTemperature(double temperature){

        String result;

        if(temperature <= MAX_TEMPERATURE){
            result = FIRED_MESSAGE ;
        }
        else{
            result = UNFIRED_MESSAGE;
        }

        return result;

    }

    //26


    //27

    //28

    //29

    //30

    //31

    //32

    //33

    //34

    //35

    //36

    //37

    //38

    //39

    //40
}
