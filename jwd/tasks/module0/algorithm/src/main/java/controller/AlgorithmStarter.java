package controller;

import logic.BranchingAlgorithm;
import logic.CyclesAlgorithm;
import logic.LinearAlgorithm;
import utils.UserInput;
import view.Printer;


public class AlgorithmStarter {

    private static final String CYCLES_MSG = "Cycles";
    private static final String BRANCHING_MSG = "Branching";
    private static final String LINEAR_MSG = "Linear";
    private static final String DEFAULT_QUESTION_MSG = "Are you boy or girl (b/g)?";

    public static void main(String[] args) {

        Printer.printInfo(LINEAR_MSG);
        Printer.printInfo("\n");
        Printer.printInfo("1st - task");
        Printer.printInfo("Enter first number: ");
        double firstLinearNumber = UserInput.inputDouble();
        Printer.printInfo("Enter second number: ");
        double secondLinearNumber = UserInput.inputDouble();
        Printer.printInfo(LinearAlgorithm.sum(firstLinearNumber, secondLinearNumber));
        Printer.printInfo(LinearAlgorithm.subtract(firstLinearNumber, secondLinearNumber));
        Printer.printInfo(LinearAlgorithm.multiply(firstLinearNumber, secondLinearNumber));
        Printer.printInfo(LinearAlgorithm.division(firstLinearNumber, secondLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("2d - task");
        Printer.printInfo("Number in this function: " + firstLinearNumber);
        Printer.printInfo(LinearAlgorithm.findFirstFunction(firstLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("3d - task");
        Printer.printInfo("Number in this function: " + firstLinearNumber + " " + secondLinearNumber);
        Printer.printInfo(LinearAlgorithm.findSecondFunction(firstLinearNumber, secondLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("4th - task");
        Printer.printInfo("Enter third number: ");
        double thirdLinearNumber = UserInput.inputDouble();
        Printer.printInfo("Number in this function: " + firstLinearNumber + " " + secondLinearNumber + " " + thirdLinearNumber);
        Printer.printInfo(LinearAlgorithm.findThirdFunctions(firstLinearNumber, secondLinearNumber, thirdLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("5th - task");
        Printer.printInfo("Number in this function: " + firstLinearNumber + " " + secondLinearNumber);
        Printer.printInfo(LinearAlgorithm.findArithmeticMean(firstLinearNumber, secondLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("6th - task");
        Printer.printInfo("Enter amount of little buckets: ");
        int amountOfLittleBuckets = UserInput.inputInt();
        Printer.printInfo("Enter amount of little buckets: ");
        int amountOfLargeBuckets = UserInput.inputInt();
        Printer.printInfo(LinearAlgorithm.findSolution(amountOfLittleBuckets, amountOfLargeBuckets));

        Printer.printInfo("\n");
        Printer.printInfo("7th - task");
        Printer.printInfo("Width of figure: " + firstLinearNumber);
        Printer.printInfo(LinearAlgorithm.findLength(firstLinearNumber));
        Printer.printInfo(LinearAlgorithm.findSquare(firstLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("8th - task");
        Printer.printInfo("Number in this function: " + firstLinearNumber + " " + secondLinearNumber + " " + thirdLinearNumber);
        Printer.printInfo(LinearAlgorithm.findForthFunction(firstLinearNumber, secondLinearNumber, thirdLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("9th - task");
        Printer.printInfo("Enter forth number: ");
        double forthLinearNumber = UserInput.inputDouble();
        Printer.printInfo("Number in this function: " + firstLinearNumber + " " + secondLinearNumber + " " +
                thirdLinearNumber + " " + forthLinearNumber);
        Printer.printInfo(LinearAlgorithm.findFivesFunction(firstLinearNumber, secondLinearNumber, thirdLinearNumber, forthLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("10th - task");
        Printer.printInfo("Number in this function: " + firstLinearNumber + " " + secondLinearNumber);
        Printer.printInfo(LinearAlgorithm.findSixesFunction(firstLinearNumber, secondLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("11th - task");
        Printer.printInfo("Number in this function: " + firstLinearNumber + " " + secondLinearNumber);
        Printer.printInfo(LinearAlgorithm.findTrianglePerimeter(firstLinearNumber, secondLinearNumber));
        Printer.printInfo(LinearAlgorithm.findTriangleSquare(firstLinearNumber, secondLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("12th - task");
        Printer.printInfo("Number in this function: " + firstLinearNumber + " " + secondLinearNumber + " " + thirdLinearNumber + " " + forthLinearNumber);
        Printer.printInfo(LinearAlgorithm.findDistance(firstLinearNumber, secondLinearNumber, thirdLinearNumber, forthLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("13th - task");
        Printer.printInfo("Enter x of third point: ");
        double fivesLinearNumber = UserInput.inputDouble();
        Printer.printInfo("Enter y of third point: ");
        double sixesLinearNumber = UserInput.inputDouble();
        Printer.printInfo("Other points (x,y): " + firstLinearNumber + " " + secondLinearNumber + " " + thirdLinearNumber + " " + forthLinearNumber);
        Printer.printInfo(LinearAlgorithm.findTrianglePerimeter(firstLinearNumber, secondLinearNumber, thirdLinearNumber, forthLinearNumber, fivesLinearNumber, sixesLinearNumber));
        Printer.printInfo(LinearAlgorithm.findTriangleSquare(firstLinearNumber, secondLinearNumber, thirdLinearNumber, forthLinearNumber, fivesLinearNumber, sixesLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("14th - task");
        Printer.printInfo("Radius: " + firstLinearNumber);
        Printer.printInfo(LinearAlgorithm.findCircleSquare(firstLinearNumber));
        Printer.printInfo(LinearAlgorithm.findLengthOfCircle(firstLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("15th - task");
        Printer.printInfo(LinearAlgorithm.findFirstPowerOfPI());
        Printer.printInfo(LinearAlgorithm.findSecondPowerOfPI());
        Printer.printInfo(LinearAlgorithm.findThirdPowerOfPI());
        Printer.printInfo(LinearAlgorithm.findForthPowerOfPI());

        Printer.printInfo("\n");
        Printer.printInfo("16th - task");
        Printer.printInfo("Enter number in format XXXX: ");
        int number = UserInput.inputInt();
        Printer.printInfo(LinearAlgorithm.findSumOfNumbers(number));

        Printer.printInfo("\n");
        Printer.printInfo("17th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber + secondLinearNumber);
        Printer.printInfo(LinearAlgorithm.findAverageArithmeticMean(firstLinearNumber, secondLinearNumber));
        Printer.printInfo(LinearAlgorithm.findAverageGeometricalMean(firstLinearNumber, secondLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("18th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber);
        Printer.printInfo(LinearAlgorithm.findValueOfCube(firstLinearNumber));
        Printer.printInfo(LinearAlgorithm.findTotalSurfaceSquare(firstLinearNumber));
        Printer.printInfo(LinearAlgorithm.findSquareOfFace(firstLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("19th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber);
        Printer.printInfo(LinearAlgorithm.findSquareOfEquilateralTriangle(firstLinearNumber));
        Printer.printInfo(LinearAlgorithm.findHeightOfEquilateralTriangle(firstLinearNumber));
        Printer.printInfo(LinearAlgorithm.findRadiusOfEnteredCircle(firstLinearNumber));
        Printer.printInfo(LinearAlgorithm.findRadiusOfCircumscribedCircle(firstLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("20th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber);
        Printer.printInfo(LinearAlgorithm.findSquareOfCircle(firstLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("23th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber + " " + secondLinearNumber);
        Printer.printInfo(LinearAlgorithm.findRingSquare(firstLinearNumber, secondLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("24th - task");
        Printer.printInfo("Enter the large side of Trapezoid: ");
        double largeSide = UserInput.inputDouble();
        Printer.printInfo("Enter the little side of Trapezoid: ");
        double littleSide = UserInput.inputDouble();
        Printer.printInfo("Enter the corner: ");
        double corner = UserInput.inputDouble();
        Printer.printInfo(LinearAlgorithm.findTrapezoidSquare(largeSide, littleSide, corner));

        Printer.printInfo("\n");
        Printer.printInfo("25th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber + " " + secondLinearNumber + " " + thirdLinearNumber);
        Printer.printInfo(LinearAlgorithm.findRoots(firstLinearNumber, secondLinearNumber, thirdLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("26th - task");
        Printer.printInfo("Numbers in this function: " + largeSide + " " + littleSide + " " + corner);
        Printer.printInfo(LinearAlgorithm.findTrapezoidSquare(largeSide, littleSide, corner));

        Printer.printInfo("\n");
        Printer.printInfo("27th - task");
        Printer.printInfo("Number in this function: " + firstLinearNumber);
        Printer.printInfo(LinearAlgorithm.findEightsPowerOfNumber(firstLinearNumber));
        Printer.printInfo(LinearAlgorithm.findTensPowerOfNumber(firstLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("28th - task");
        Printer.printInfo("Corner: " + corner);
        Printer.printInfo(LinearAlgorithm.translateAngleInGrades(corner));
        Printer.printInfo(LinearAlgorithm.translateAngleInMinutes(corner));
        Printer.printInfo(LinearAlgorithm.translateAngleInSeconds(corner));

        Printer.printInfo("\n");
        Printer.printInfo("29th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber + " " + secondLinearNumber + " ");
        Printer.printInfo(LinearAlgorithm.findCornersInGrades(firstLinearNumber, secondLinearNumber, thirdLinearNumber));
        Printer.printInfo(LinearAlgorithm.findCornersInRadians(firstLinearNumber, secondLinearNumber, thirdLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("30th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber + " " + secondLinearNumber + " ");
        Printer.printInfo(LinearAlgorithm.findFullResistance(firstLinearNumber, secondLinearNumber, thirdLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("31th - task");
        Printer.printInfo("Enter speed of boat: ");
        double boatSpeed = UserInput.inputDouble();
        Printer.printInfo("Enter speed of river: ");
        double riverSpeed = UserInput.inputDouble();
        Printer.printInfo("Enter time in a lake: ");
        double lakeTime = UserInput.inputDouble();
        Printer.printInfo("Enter time in a river: ");
        double riverTime = UserInput.inputDouble();
        Printer.printInfo(LinearAlgorithm.findFullDistance(boatSpeed, riverSpeed, lakeTime, riverTime ));

        Printer.printInfo("\n");
        Printer.printInfo("33th - task");
        char symbol = 'a';
        Printer.printInfo("Symbol - " + symbol);
        Printer.printInfo(LinearAlgorithm.checkSymbol(symbol));
        Printer.printInfo(LinearAlgorithm.checkNextSymbol(symbol));
        Printer.printInfo(LinearAlgorithm.checkPreviousSymbol(symbol));

        Printer.printInfo("\n");
        Printer.printInfo("34th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber);
        Printer.printInfo(LinearAlgorithm.convertToKBytes(firstLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("35th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber + " " + secondLinearNumber);
        Printer.printInfo(LinearAlgorithm.findMinorNumber(firstLinearNumber, secondLinearNumber));
        Printer.printInfo(LinearAlgorithm.findMajorNumber(firstLinearNumber, secondLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("36th - task");
        Printer.printInfo("Numbers in this function: " + number);
        Printer.printInfo(LinearAlgorithm.findSevensFunction(number));

        Printer.printInfo("\n");
        Printer.printInfo("39th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber);
        Printer.printInfo(LinearAlgorithm.findEightsFunction(firstLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("40th - task");
        Printer.printInfo("Numbers in this function: " + firstLinearNumber);
        Printer.printInfo(LinearAlgorithm.findNinesFunction(firstLinearNumber));
        Printer.printInfo(LinearAlgorithm.findTensFunction(firstLinearNumber));

        Printer.printInfo("\n");
        Printer.printInfo("\n");
        Printer.printInfo("\n");
        Printer.printInfo(BRANCHING_MSG);
        Printer.printInfo("\n");
        Printer.printInfo("1st - task");
        Printer.printInfo(BranchingAlgorithm.compareNumber());

        Printer.printInfo("\n");
        Printer.printInfo("2d - task");
        Printer.printInfo(BranchingAlgorithm.compareNumbers());

        Printer.printInfo("\n");
        Printer.printInfo("3d - task");
        Printer.printInfo("Enter your number: ");
        double num = UserInput.inputDouble();
        Printer.printInfo(BranchingAlgorithm.compareNumbers(num));

        Printer.printInfo("\n");
        Printer.printInfo("4th - task");
        Printer.printInfo("Enter your first number: ");
        double firstNumber = UserInput.inputDouble();
        Printer.printInfo("Enter your second number: ");
        double secondNumber = UserInput.inputDouble();
        Printer.printInfo(BranchingAlgorithm.compareNumbers(firstNumber, secondNumber));

        Printer.printInfo("\n");
        Printer.printInfo("5th - task");
        Printer.printInfo("Smallest of them: ");
        Printer.printInfo(BranchingAlgorithm.findSmallestNumber(firstNumber, secondNumber));

        Printer.printInfo("\n");
        Printer.printInfo("6th - task");
        Printer.printInfo("Largest of them: ");
        Printer.printInfo(BranchingAlgorithm.findLargestNumber(firstNumber, secondNumber));

        Printer.printInfo("\n");
        Printer.printInfo("7th - task");
        Printer.printInfo("Enter your third number: ");
        double thirdNumber = UserInput.inputDouble();
        Printer.printInfo("Enter your forth number: ");
        double forthNumber = UserInput.inputDouble();
        Printer.printInfo(BranchingAlgorithm.resolveFunction(firstNumber, secondNumber, thirdNumber, forthNumber));

        Printer.printInfo("\n");
        Printer.printInfo("8th - task");
        Printer.printInfo("First number: " + firstNumber + " Second number: " + secondNumber);
        Printer.printInfo(BranchingAlgorithm.findSmallestSecondPowerNumber(firstNumber, secondNumber));

        Printer.printInfo("\n");
        Printer.printInfo("9th - task");
        Printer.printInfo("Enter 1st side of triangle: ");
        double firstSide = UserInput.inputDouble();
        Printer.printInfo("Enter 2d side of triangle: ");
        double secondSide = UserInput.inputDouble();
        Printer.printInfo("Enter 3d side of triangle: ");
        double thirdSide = UserInput.inputDouble();
        Printer.printInfo(BranchingAlgorithm.checkEquilateralTriangle(firstSide, secondSide, thirdSide));

        Printer.printInfo("\n");
        Printer.printInfo("10th - task");
        Printer.printInfo("First square: " + firstNumber + " Second square: " + secondNumber);
        Printer.printInfo(BranchingAlgorithm.findSmallestCircleSquare(firstNumber, secondNumber));

        Printer.printInfo("\n");
        Printer.printInfo("11th - task");
        Printer.printInfo("First square: " + firstNumber + " Second square: " + secondNumber);
        Printer.printInfo(BranchingAlgorithm.findSmallestQuadrateSquare(firstNumber, secondNumber));

        Printer.printInfo("\n");
        Printer.printInfo("12th - task");
        Printer.printInfo("First num: " + firstNumber + " Second num: " + secondNumber + " Third num: " + thirdNumber);
        Printer.printInfo(BranchingAlgorithm.upgradeNumbers(firstNumber, secondNumber, thirdNumber));

        Printer.printInfo("\n");
        Printer.printInfo("13th - task");
        Printer.printInfo("First dot: " + firstNumber + ";" + secondNumber + " Second dot: " + thirdNumber + " " + forthNumber);
        double[] firstDot = new double[]{firstNumber, secondNumber};
        double[] secondDot = new double[]{thirdNumber, forthNumber};
        Printer.printInfo(BranchingAlgorithm.findSmallestDistance(firstDot, secondDot));

        Printer.printInfo("\n");
        Printer.printInfo("14th - task");
        Printer.printInfo("Enter first triangle conner: ");
        double firstConner = UserInput.inputDouble();
        Printer.printInfo("Enter second triangle conner: ");
        double secondConner = UserInput.inputDouble();
        Printer.printInfo(BranchingAlgorithm.checkTriangle(firstConner, secondConner));

        Printer.printInfo("\n");
        Printer.printInfo("15th - task");
        Printer.printInfo("First number: " + firstNumber + " Second number: " + secondNumber);
        Printer.printInfo(BranchingAlgorithm.upgradeNumbers(firstNumber, secondNumber));

        Printer.printInfo("\n");
        Printer.printInfo("17th - task");
        Printer.printInfo("First number: " + firstNumber + " Second number: " + secondNumber);
        Printer.printInfo(BranchingAlgorithm.replaceNumbers(firstNumber, secondNumber));

        Printer.printInfo("\n");
        Printer.printInfo("18th - task");
        Printer.printInfo("Enter negative number: ");
        double negativeNumber = UserInput.inputDouble();
        Printer.printInfo("Numbers for this method: " + firstNumber + " " + secondNumber + " " + negativeNumber);
        Printer.printInfo(BranchingAlgorithm.findAmountOfNegativeNumbers(firstNumber, secondNumber, negativeNumber));

        Printer.printInfo("\n");
        Printer.printInfo("19th - task");
        Printer.printInfo("Numbers for this method: " + firstNumber + " " + secondNumber + " " + negativeNumber);
        Printer.printInfo(BranchingAlgorithm.findAmountOfPositiveNumbers(firstNumber, secondNumber, negativeNumber));

        Printer.printInfo("\n");
        Printer.printInfo("21th - task");
        Printer.printInfo(DEFAULT_QUESTION_MSG);
        String answer = UserInput.inputString();
        Printer.printInfo(BranchingAlgorithm.flatter(answer));

        Printer.printInfo("\n");
        Printer.printInfo("22th - task");
        Printer.printInfo("Numbers for this method: " + firstNumber + " " + secondNumber);
        Printer.printInfo(BranchingAlgorithm.replaceNumbers(firstNumber, secondNumber));

        Printer.printInfo("\n");
        Printer.printInfo("23th - task");
        Printer.printInfo("Enter the num of day: ");
        int numberOfDay = UserInput.inputInt();
        Printer.printInfo("Enter the num of month: ");
        int numberOfMonth = UserInput.inputInt();
        Printer.printInfo(BranchingAlgorithm.checkDate(numberOfDay, numberOfMonth));

        Printer.printInfo("\n");
        Printer.printInfo("24th - task");
        Printer.printInfo("Enter the num of petal: ");
        int amountOfPetal = UserInput.inputInt();
        Printer.printInfo(BranchingAlgorithm.checkYourLove(amountOfPetal));


        Printer.printInfo("\n");
        Printer.printInfo("24th - task");
        Printer.printInfo("Enter current temperature: ");
        double temperature = UserInput.inputDouble();
        Printer.printInfo(BranchingAlgorithm.checkTemperature(temperature));


        Printer.printInfo("\n");
        Printer.printInfo("\n");
        Printer.printInfo("\n");
        Printer.printInfo(CYCLES_MSG);
        Printer.printInfo("\n");
        Printer.printInfo("1st - task");
        CyclesAlgorithm.createNumbers();

        Printer.printInfo("\n");
        Printer.printInfo("2d - task");
        CyclesAlgorithm.createBackNumber();

        Printer.printInfo("\n");
        Printer.printInfo("3d - task");
        CyclesAlgorithm.printTable();

        Printer.printInfo("\n");
        Printer.printInfo("4th - task");
        CyclesAlgorithm.printNumbers();

        Printer.printInfo("\n");
        Printer.printInfo("9th - task");
        CyclesAlgorithm.printSecondPowerNumbers();

        Printer.printInfo("\n");
        Printer.printInfo("15st - task");
        Printer.printInfo(CyclesAlgorithm.findSecondFunction());

        Printer.printInfo("\n");
        Printer.printInfo("16th - task");
        Printer.printInfo(CyclesAlgorithm.findFirstFunction());

        Printer.printInfo("\n");
        Printer.printInfo("25th - task");
        Printer.printInfo("Enter your num for factorial: ");
        double numberFactorial = UserInput.inputDouble();
        Printer.printInfo(CyclesAlgorithm.findFactorial(numberFactorial));

        Printer.printInfo("\n");
        Printer.printInfo("26th - task");
        CyclesAlgorithm.printASCII();

        Printer.printInfo("\n");
        Printer.printInfo("34th - task");
        CyclesAlgorithm.findCertainNumbers();
    }
}
