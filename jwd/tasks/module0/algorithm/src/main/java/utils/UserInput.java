package utils;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserInput {

    private static Scanner scan = new Scanner(System.in);

    public static double inputDouble() throws InputMismatchException {
        return scan.nextDouble();
    }

    public static int inputInt() throws InputMismatchException {
        return scan.nextInt();
    }

    public static String inputString(){
        return scan.next();
    }
}
