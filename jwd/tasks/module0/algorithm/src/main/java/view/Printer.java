package view;

import java.util.Arrays;

public class Printer {

    private static String MSG = "RESULT: ";

    public static void printInfo(double result) {
        System.out.println(MSG + result);
    }

    public static void printInfo(double[] result) {
        System.out.println(MSG + Arrays.toString(result));
    }

    public static void printInfo(long result) {
        System.out.println(MSG + result);
    }

    public static void printInfo(boolean result) {
        System.out.println(MSG + result);
    }

    public static void printInfo(boolean[] result) {
        System.out.println(MSG + Arrays.toString(result));
    }

    public static void printInfo(String msg){
        System.out.println(msg);
    }
}
