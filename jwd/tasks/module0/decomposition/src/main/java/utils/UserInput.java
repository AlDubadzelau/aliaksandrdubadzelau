package utils;

import java.util.Scanner;

public class UserInput {
    private static Scanner scan = new Scanner(System.in);

    public static int inputInt() {
        return scan.nextInt();
    }

    public static double inputDouble() {
        return scan.nextDouble();
    }
}
