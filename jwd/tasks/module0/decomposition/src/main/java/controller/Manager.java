package controller;

import logic.*;
import utils.UserInput;
import view.Printer;

import java.util.Arrays;
import java.util.List;

public class Manager {

    private final static int NUMBER_WITH_UNEVEN_DIGITAL = 13579;
    private final static int NUMBER_WITH_EVEN_DIGITAL = 135796;
    private final static double[] DEFAULT_FIRST_DOT = new double[]{1, 5};
    private final static double[] DEFAULT_SECOND_DOT = new double[]{-1, 5};
    private final static double[] DEFAULT_THIRD_DOT = new double[]{1, -7};
    private final static double[][] DEFAULT_DOTS_ARRAY = new double[][]{
            {1, 1.99},
            {-10, 2.5},
            {13, 1.5},
            {16, -234},
            {-23.55, 55},
            {123, 66.34},
            {34, 55.6},
            {44.4, 44}};
    private final static double[] DEFAULT_ARRAY = new double[]{-100, -4.5, 14, 4660, 34.55, -563.5, 522, 100004, 145.0, -545.6};

    private final static String DEFAULT_ERROR_MESSAGE = "Something go wrong";
    private final static String DEFAULT_FIRST_MESSAGE = "Input first (int) number: ";
    private final static String DEFAULT_SECOND_MESSAGE = "Input second (int) number: ";
    private final static String DEFAULT_THIRD_MESSAGE = "Input third (int) number: ";
    private final static String DEFAULT_FORTH_MESSAGE = "Input forth (int) number: ";
    private final static String DEFAULT_LCM_MESSAGE = "LCM: ";
    private final static String DEFAULT_GCD_MESSAGE = "GCD: ";
    private static final String RESULT_MESSAGE = "Result: ";

    public static void main(String[] args) {

        Printer.printInfo("№1");
        double[] firstDot = DEFAULT_FIRST_DOT;
        double[] secondDot = DEFAULT_SECOND_DOT;
        double[] thirdDot = DEFAULT_THIRD_DOT;
        if (Triangle.isTriangle(firstDot, secondDot, thirdDot)) {
            double triangleSquare = Triangle.findTriangleSquare(firstDot, secondDot, thirdDot);
            Printer.printInfo(triangleSquare);
        } else {
            Printer.printInfo(DEFAULT_ERROR_MESSAGE);
        }

        Printer.printInfo("\n№2");
        Printer.printInfo(DEFAULT_FIRST_MESSAGE);
        int firstNumber = UserInput.inputInt();
        Printer.printInfo(DEFAULT_SECOND_MESSAGE);
        int secondNumber = UserInput.inputInt();
        int LCM = Mathematics.findLCM(firstNumber, secondNumber);
        int GCD = Mathematics.findGCD(firstNumber, secondNumber);
        Printer.printInfo(DEFAULT_LCM_MESSAGE);
        Printer.printInfo(LCM);
        Printer.printInfo(DEFAULT_GCD_MESSAGE);
        Printer.printInfo(GCD);

        Printer.printInfo("\n№3");
        Printer.printInfo(DEFAULT_THIRD_MESSAGE);
        int thirdNumber = UserInput.inputInt();
        Printer.printInfo(DEFAULT_FORTH_MESSAGE);
        int forthNumber = UserInput.inputInt();
        Printer.printInfo("Numbers:" + firstNumber + " " + secondNumber + " " + thirdNumber + " " + forthNumber);
        GCD = Mathematics.findGCDOfFourNumbers(firstNumber, secondNumber, thirdNumber, forthNumber);
        Printer.printInfo(DEFAULT_GCD_MESSAGE);
        Printer.printInfo(GCD);

        Printer.printInfo("\n№4");
        Printer.printInfo("Numbers:" + firstNumber + " " + secondNumber + " " + thirdNumber);
        LCM = Mathematics.findLCMOfThreeNumbers(firstNumber, secondNumber, thirdNumber);
        Printer.printInfo(DEFAULT_LCM_MESSAGE);
        Printer.printInfo(LCM);

        Printer.printInfo("\n№5");
        Printer.printInfo("Numbers:" + firstNumber + " " + secondNumber + " " + thirdNumber);
        double sum = Mathematics.findSumOfMaxAndMin(firstNumber, secondNumber, thirdNumber);
        Printer.printInfo(sum);

        Printer.printInfo("\n№6");
        Printer.printInfo("Side: " + firstNumber);
        double hexagonSquare = Hexagon.findSquare(firstNumber);
        Printer.printInfo(hexagonSquare);

        Printer.printInfo("\n№7");
        Printer.printInfo(DEFAULT_DOTS_ARRAY);
        double[][] dots = Distance.findMaxDistance(DEFAULT_DOTS_ARRAY);
        Printer.printInfo(RESULT_MESSAGE);
        Printer.printInfo(dots);

        Printer.printInfo("\n№8");
        Printer.printInfo(DEFAULT_ARRAY);
        double secondMaxNumber = Numbers.findSecondMaxNumber(DEFAULT_ARRAY);
        Printer.printInfo(secondMaxNumber);

        Printer.printInfo("\n№9");
        Printer.printInfo("Input first MutuallySimple number: ");
        int firstMutuallySimple = UserInput.inputInt();
        Printer.printInfo("Input second MutuallySimple number: ");
        int secondMutuallySimple = UserInput.inputInt();
        Printer.printInfo("Input third MutuallySimple number: ");
        int thirdMutuallySimple = UserInput.inputInt();
        boolean isMutuallySimpleNumbers = Mathematics.isMutuallySimple(firstMutuallySimple, secondMutuallySimple, thirdMutuallySimple);
        Printer.printInfo(isMutuallySimpleNumbers);

        Printer.printInfo("\n№10");
        int firstSumOfFactorials = Mathematics.findSumOfFactorials();
        Printer.printInfo(firstSumOfFactorials);

        Printer.printInfo("\n№11");
        Printer.printInfo("Enter number from 0 to 7");
        int startItem = UserInput.inputInt();
        double sumOfItems = Array.findSum(startItem, DEFAULT_ARRAY);
        Printer.printInfo(sumOfItems);

        Printer.printInfo("\n№12");
        Printer.printInfo("Enter first side of quadrangle: ");
        double firstSide = UserInput.inputDouble();
        Printer.printInfo("Enter second side of quadrangle: ");
        double secondSide = UserInput.inputDouble();
        Printer.printInfo("Enter third side of quadrangle: ");
        double thirdSide = UserInput.inputDouble();
        Printer.printInfo("Enter forth side of quadrangle: ");
        double forthSide = UserInput.inputDouble();
        if (Quadrangle.isQuadrangle(firstSide, secondSide, thirdSide, forthSide)) {
            double quadrangleSquare = Quadrangle.findSquare(firstSide, secondSide, thirdSide, forthSide);
            Printer.printInfo(quadrangleSquare);
        } else {
            Printer.printInfo(DEFAULT_ERROR_MESSAGE);
        }

        Printer.printInfo("\n№13");
        Printer.printInfo("Input (int) number (preferable >100): ");
        int num = UserInput.inputInt();
        Integer[] itemsArray = Array.createArrayOfItems(num);
        Printer.printInfo(RESULT_MESSAGE);
        Printer.printInfo(itemsArray);

        Printer.printInfo("\n№14");
        Printer.printInfo(DEFAULT_FIRST_MESSAGE);
        firstNumber = UserInput.inputInt();
        Printer.printInfo(DEFAULT_SECOND_MESSAGE);
        secondNumber = UserInput.inputInt();
        int result = Comparator.compareNumbers(firstNumber, secondNumber);
        Printer.printInfo(result);

        Printer.printInfo("\n№15");
        Printer.printInfo("Input max number border: ");
        int border = UserInput.inputInt();
        Printer.printInfo("Input number == sum of digital: ");
        int digitalNumber = UserInput.inputInt();
        List<Integer> resultList = Array.createSpecialArray(digitalNumber, border);
        Printer.printInfo(resultList);

        Printer.printInfo("\n№16");
        Printer.printInfo(DEFAULT_FIRST_MESSAGE);
        firstNumber = UserInput.inputInt();
        List<Integer[]> twinsList = Twins.findAllTwins(firstNumber);
        Printer.printInfoList(twinsList);

        Printer.printInfo("\n№17");
        Printer.printInfo("Enter last number for Armstrong's numbers: ");
        firstNumber = UserInput.inputInt();
        List<Integer> armstrongList = Array.findArmstrongsNumbers(firstNumber);
        Printer.printInfo(armstrongList);

        Printer.printInfo("\n№18");
        Printer.printInfo("Enter amount of digital in number: ");
        int amountOfDigital = UserInput.inputInt();
        List<Integer> increasingNumbers = Array.findIncreasingNumbers(amountOfDigital);
        Printer.printInfo(increasingNumbers);

        Printer.printInfo("\n№19");
        Printer.printInfo("Even number: " + NUMBER_WITH_EVEN_DIGITAL);
        int evenResult = Numbers.findSumOfUnevenNumbers(NUMBER_WITH_EVEN_DIGITAL);
        Printer.printInfo(evenResult);
        Printer.printInfo("Uneven number: " + NUMBER_WITH_UNEVEN_DIGITAL);
        int number = Numbers.findSumOfUnevenNumbers(NUMBER_WITH_UNEVEN_DIGITAL);
        Printer.printInfo(number);
        Printer.printInfo("Amount of even numbers: ");
        Printer.printInfo(Numbers.findAmountOfEvenNumbers(number));

        Printer.printInfo("\n№20");
        Printer.printInfo(DEFAULT_FIRST_MESSAGE);
        firstNumber = UserInput.inputInt();
        Printer.printInfo(Numbers.countAmountOfSubtraction(firstNumber));
    }
}