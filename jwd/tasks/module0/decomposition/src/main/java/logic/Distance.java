package logic;

public class Distance {

    private static final int DEFAULT_VALUE = 0;
    private static final int DEFAULT_FIRST_DOT_POSITION = 0;
    private static final int DEFAULT_SECOND_DOT_POSITION = 1;
    private static final int DEFAULT_AMOUNT_OF_DOTS = 2;

    //7
    public static double[][] findMaxDistance(double[][] dotsArray) {

        double[][] dots = new double[DEFAULT_AMOUNT_OF_DOTS][];
        double maxLength = DEFAULT_VALUE;

        for (int i = 0; i < dotsArray.length; i++) {
            for (int j = 0; j < dotsArray.length; j++) {
                if (i != j) {

                    double length = Triangle.findSideLength(dotsArray[i], dotsArray[j]);

                    if (maxLength < length) {

                        maxLength = length;
                        dots[DEFAULT_FIRST_DOT_POSITION] = dotsArray[i];
                        dots[DEFAULT_SECOND_DOT_POSITION] = dotsArray[j];

                    }
                }
            }
        }

        return dots;
    }

}
