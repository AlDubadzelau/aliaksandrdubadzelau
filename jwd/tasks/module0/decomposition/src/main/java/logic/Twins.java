package logic;

import java.util.ArrayList;
import java.util.List;

public class Twins {

    private static final int DEFAULT_AMOUNT_CORRECTOR = 1;
    private static final int DEFAULT_MULTIPLIER = 2;
    private static final int DEFAULT_TWIN = 2;

    //16
    public static List<Integer[]> findAllTwins(int firstNumber) {

        List<Integer[]> twinsList = new ArrayList<Integer[]>();

        int secondNumber = findSecondNumber(firstNumber);

        for (int i = firstNumber; i < secondNumber - DEFAULT_AMOUNT_CORRECTOR; i++) {
            int twin = createTwin(i);
            Integer[] twins = createCouple(i, twin);
            twinsList.add(twins);
        }

        return twinsList;
    }

    private static Integer[] createCouple(int firstTwin, int secondTwin) {
        return new Integer[]{firstTwin, secondTwin};
    }

    private static int createTwin(int number) {
        return number + DEFAULT_TWIN;
    }

    private static int findSecondNumber(int firstNumber) {

        return firstNumber * DEFAULT_MULTIPLIER;
    }
}
