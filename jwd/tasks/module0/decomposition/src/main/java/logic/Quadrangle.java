package logic;

public class Quadrangle {

    private static final int DEFAULT_DIVIDER = 2;
    private static final int MIN_VALUE = 0;
    private static final boolean DEFAULT_POSITIVE_RESULT = true;
    private static final boolean DEFAULT_NEGATIVE_RESULT = false;

    //12
    public static double findSquare(double firstSide, double secondSide, double thirdSide, double forthSide) {

        double diagonal = findDiagonal(firstSide, secondSide);

        double square = findRectangleSquare(firstSide, secondSide);
        square += findGeronsSquare(diagonal, thirdSide, forthSide);

        return square;
    }

    private static double findRectangleSquare(double firstSide, double secondSide) {
        return firstSide * secondSide / DEFAULT_DIVIDER;
    }

    public static boolean isQuadrangle(double firstSide, double secondSide, double thirdSide, double forthSide) {

        boolean isQuadrangle = DEFAULT_POSITIVE_RESULT;

        if (firstSide < MIN_VALUE || secondSide < MIN_VALUE || thirdSide < MIN_VALUE || forthSide < MIN_VALUE) {
            isQuadrangle = DEFAULT_NEGATIVE_RESULT;
        }

        return isQuadrangle;
    }

    private static double findGeronsSquare(double firstSide, double thirdSide, double forthSide) {

        double semiPerimeter = findSemiPerimeter(firstSide, thirdSide, forthSide);

        return Math.sqrt(semiPerimeter * (semiPerimeter - firstSide) * (semiPerimeter - thirdSide) * (semiPerimeter - forthSide));
    }

    private static double findSemiPerimeter(double firstSide, double secondSide, double thirdSide) {
        return (firstSide + secondSide + thirdSide) / DEFAULT_DIVIDER;
    }

    private static double findDiagonal(double firstSide, double secondSide) {
        return Math.sqrt(firstSide * firstSide + secondSide * secondSide);
    }
}

