package logic;

public class Mathematics {

    private static final int DEFAULT_VALUE = 0;
    private static final int DEFAULT_ZERO_POSITION = 0;
    private static final int DEFAULT_FIRST_NUMBER = 1;
    private static final int DEFAULT_SECOND_NUMBER = 2;
    private static final int DEFAULT_DIVIDER = 2;
    private static final int DEFAULT_AMOUNT_OF_FACTORIALS = 9;
    private static final boolean DEFAULT_POSITIVE_CONDITION = true;
    private static final boolean DEFAULT_NEGATIVE_CONDITION = false;

    //2
    public static int findLCM(int firstNumber, int secondNumber) {

        int GCD = findGCD(firstNumber, secondNumber);

        return firstNumber * secondNumber / GCD;

    }


    public static int findGCD(int firstNumber, int secondNumber) {

        while (firstNumber != DEFAULT_ZERO_POSITION && secondNumber != DEFAULT_ZERO_POSITION) {
            if (firstNumber > secondNumber) {
                firstNumber = firstNumber % secondNumber;
            } else {
                secondNumber = secondNumber % firstNumber;
            }
        }

        return firstNumber + secondNumber;
    }

    //3
    public static int findGCDOfFourNumbers(int firstNumber, int secondNumber, int thirdNumber, int forthNumber) {

        return findGCD(findGCD(firstNumber, secondNumber), findGCD(thirdNumber, forthNumber));
    }

    //4
    public static int findLCMOfThreeNumbers(int firstNumber, int secondNumber, int thirdNumber) {

        int GCD = findGCDOfThreeNumbers(firstNumber, secondNumber, thirdNumber);

        return firstNumber * secondNumber * thirdNumber / GCD;
    }

    private static int findGCDOfThreeNumbers(int firstNumber, int secondNumber, int thirdNumber) {

        return findGCD(findGCD(firstNumber, secondNumber), thirdNumber);
    }

    //5
    public static double findSumOfMaxAndMin(double firstNumber, double secondNumber, double thirdNumber) {

        double minNumber = findMinNumber(firstNumber, secondNumber, thirdNumber);
        double maxNumber = findMaxNumber(firstNumber, secondNumber, thirdNumber);

        return minNumber + maxNumber;
    }

    private static double findMaxNumber(double firstNumber, double secondNumber, double thirdNumber) {

        double maxNumber = thirdNumber;

        if (firstNumber >= secondNumber && firstNumber >= thirdNumber) {
            maxNumber = firstNumber;
        } else if (secondNumber >= thirdNumber) {
            maxNumber = secondNumber;
        }

        return maxNumber;
    }

    private static double findMinNumber(double firstNumber, double secondNumber, double thirdNumber) {

        double minNumber = thirdNumber;

        if (firstNumber <= secondNumber && firstNumber <= thirdNumber) {
            minNumber = firstNumber;
        } else if (secondNumber <= thirdNumber) {
            minNumber = secondNumber;
        }

        return minNumber;
    }

    //9
    public static boolean isMutuallySimple(int firstNumber, int secondNumber, int thirdNumber) {

        boolean result = DEFAULT_NEGATIVE_CONDITION;

        if (findGCD(findGCD(firstNumber, secondNumber), thirdNumber) == DEFAULT_FIRST_NUMBER) {
            result = DEFAULT_POSITIVE_CONDITION;
        }

        return result;
    }

    //10
    public static int findSumOfFactorials() {

        int sum = DEFAULT_VALUE;
        int factorial = DEFAULT_FIRST_NUMBER;


        for (int i = factorial; i <= DEFAULT_AMOUNT_OF_FACTORIALS; i++) {

            factorial *= i;

            if (i % DEFAULT_DIVIDER != DEFAULT_ZERO_POSITION) {
                sum += factorial;
            }
        }

        return sum;
    }

}
