package logic;

public class Numbers {

    private static final int DEFAULT_VALUE = 0;
    private static final double DEFAULT_DIVIDER = 2;
    private static final boolean DEFAULT_POSITIVE_RESULT = true;
    private static final boolean DEFAULT_NEGATIVE_RESULT = false;

    //8
    public static double findSecondMaxNumber(double[] array) {

        double secondMaxNumber = DEFAULT_VALUE;
        double maxNumber = DEFAULT_VALUE;

        for (double number : array) {
            if (number > maxNumber) {
                secondMaxNumber = maxNumber;
                maxNumber = number;
            } else if (number < maxNumber && number > secondMaxNumber) {
                secondMaxNumber = number;
            }
        }

        return secondMaxNumber;
    }

    //19
    public static int findAmountOfEvenNumbers(int number) {

        int amountOfEvenNumbers = DEFAULT_VALUE;
        Integer[] items = Array.createArrayOfItems(number);

        for (Integer item : items) {
            if (item % DEFAULT_DIVIDER == DEFAULT_VALUE) {
                amountOfEvenNumbers++;
            }
        }

        return amountOfEvenNumbers;
    }

    public static int findSumOfUnevenNumbers(int number) {

        int sum = DEFAULT_VALUE;
        Integer[] items = Array.createArrayOfItems(number);

        if (checkItemsInNumber(items)) {
            for (Integer item : items) {
                sum += item;
            }
        }

        return sum;
    }

    private static boolean checkItemsInNumber(Integer[] items) {

        boolean result = DEFAULT_POSITIVE_RESULT;

        for (Integer number : items) {
            if (number % DEFAULT_DIVIDER == DEFAULT_VALUE) {
                result = DEFAULT_NEGATIVE_RESULT;
                break;
            }
        }

        return result;
    }

    //20
    public static int countAmountOfSubtraction(int number) {

        int counter = DEFAULT_VALUE;

        while (number > DEFAULT_VALUE) {

            Integer[] items = Array.createArrayOfItems(number);
            int subtrahend = Numbers.findSumOfItems(items);
            number -= subtrahend;
            counter++;
        }

        return counter;
    }

    private static int findSumOfItems(Integer[] items) {

        int sum = DEFAULT_VALUE;

        for (Integer item : items) {
            sum += item;
        }

        return sum;
    }
}
