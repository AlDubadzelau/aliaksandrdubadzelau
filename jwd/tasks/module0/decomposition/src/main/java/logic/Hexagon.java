package logic;

public class Hexagon {

    private static final double DEFAULT_MULTIPLIER = Math.sqrt(3);
    private static final double DEFAULT_DIVIDER = 4;
    private static final int AMOUNT_OF_SIDES = 6;


    //6
    public static double findSquare(double side) {

        double triangleSquare = DEFAULT_MULTIPLIER * side * side / DEFAULT_DIVIDER;

        return triangleSquare * AMOUNT_OF_SIDES;
    }

}
