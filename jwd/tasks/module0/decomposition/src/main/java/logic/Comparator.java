package logic;

import creator.StringCreator;

public class Comparator {

    //14
    public static int compareNumbers(int firstNumber, int secondNumber) {

        String firstStringNumber = StringCreator.creteStringNumber(firstNumber);
        String secondStringNumber = StringCreator.creteStringNumber(secondNumber);
        int result = firstNumber;

        if (firstStringNumber.length() < secondStringNumber.length()) {
            result = secondNumber;

        }

        return result;
    }
}

