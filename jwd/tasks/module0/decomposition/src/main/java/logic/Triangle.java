package logic;

public class Triangle {

    private static final int DEFAULT_X_POSITION = 0;
    private static final int DEFAULT_Y_POSITION = 1;
    private static final int DEFAULT_POWER = 2;
    private static final double DEFAULT_DIVIDER = 2;
    private static final boolean DEFAULT_POSITIVE_RESULT = true;
    private static final boolean DEFAULT_NEGATIVE_RESULT = false;

    //1
    public static boolean isTriangle(double[] firstDot, double[] secondDot, double[] thirdDot) {

        boolean isTriangle = DEFAULT_POSITIVE_RESULT;

        if (firstDot[DEFAULT_X_POSITION] == secondDot[DEFAULT_X_POSITION] && firstDot[DEFAULT_X_POSITION] == thirdDot[DEFAULT_X_POSITION]) {
            isTriangle = DEFAULT_NEGATIVE_RESULT;
        }

        if (firstDot[DEFAULT_Y_POSITION] == secondDot[DEFAULT_Y_POSITION] && firstDot[DEFAULT_Y_POSITION] == thirdDot[DEFAULT_Y_POSITION]) {
            isTriangle = DEFAULT_NEGATIVE_RESULT;
        }

        return isTriangle;
    }

    public static double findTriangleSquare(double[] firstDot, double[] secondDot, double[] thirdDot) {

        double firstSide = findSideLength(firstDot, secondDot);
        double secondSide = findSideLength(firstDot, thirdDot);
        double thirdSide = findSideLength(secondDot, thirdDot);
        double semiPerimeter = findPerimeter(firstSide, secondSide, thirdSide) / DEFAULT_DIVIDER;

        double square = Math.sqrt(semiPerimeter * (semiPerimeter - firstSide) * (semiPerimeter - secondSide) * (semiPerimeter - thirdSide));

        return square;
    }

    private static double findPerimeter(double firstSide, double secondSide, double thirdSide) {

        return firstSide + secondSide + thirdSide;
    }

    static double findSideLength(double[] firstDot, double[] secondDot) {

        double firstPart = Math.pow((secondDot[DEFAULT_X_POSITION] - firstDot[DEFAULT_X_POSITION]), DEFAULT_POWER);
        double secondPart = Math.pow((secondDot[DEFAULT_Y_POSITION] - firstDot[DEFAULT_Y_POSITION]), DEFAULT_POWER);

        return Math.sqrt(firstPart + secondPart);
    }
}
