package logic;

import creator.StringCreator;

import java.util.ArrayList;
import java.util.List;

public class Array {

    private static final int MAX_VALUE = 3;
    private static final int MIN_VALUE = 0;
    private static final int DEFAULT_NEXT_ELEMENT = 1;
    private static final int DEFAULT_PREVIOUS_ELEMENT = 1;
    private static final int DEFAULT_LAST_ELEMENT = 2;
    private static final int DEFAULT_FIRST_ARMSTRONG_NUMBER = 1;
    private static final int DEFAULT_FIRST_NUMBER = 1;
    private static final int DEFAULT_MAX_DIGITAL = 9;
    private static final int DEFAULT_TENTH_NUMBER = 10;
    private static final boolean DEFAULT_POSITIVE_RESULT = true;
    private static final boolean DEFAULT_NEGATIVE_RESULT = false;


    //11
    public static double findSum(int firstElement, double[] array) {

        double sum = MIN_VALUE;

        if (firstElement <= MAX_VALUE && firstElement >= MIN_VALUE) {
            sum += array[firstElement] + array[firstElement + DEFAULT_NEXT_ELEMENT] + array[firstElement + DEFAULT_LAST_ELEMENT];
        }

        return sum;
    }

    //13
    public static Integer[] createArrayOfItems(int number) {

        String stringNumber = StringCreator.creteStringNumber(number);
        Integer[] array = new Integer[stringNumber.length()];

        for (int i = 0; i < array.length; i++) {
            String item = stringNumber.substring(i, i + DEFAULT_NEXT_ELEMENT);
            array[i] = Integer.valueOf(item);
        }

        return array;
    }

    //15
    public static List<Integer> createSpecialArray(int firstNumber, int secondNumber) {

        List<Integer> result = new ArrayList<Integer>();

        for (int i = 0; i < secondNumber; i++) {
            Integer[] itemsArray = createArrayOfItems(i);
            Integer sum = findSumOfItems(itemsArray);
            if (firstNumber == sum) {
                result.add(i);
            }
        }

        return result;
    }

    private static Integer findSumOfItems(Integer[] array) {

        Integer sum = MIN_VALUE;

        for (Integer integer : array) {
            sum += integer;
        }

        return sum;
    }

    //17
    public static List<Integer> findArmstrongsNumbers(int lastNumber) {

        List<Integer> armstrongsNumbers = new ArrayList<Integer>();

        for (int i = DEFAULT_FIRST_ARMSTRONG_NUMBER; i < lastNumber; i++) {
            Integer[] numbers = createArrayOfItems(i);
            upgradeToCertainPower(numbers);
            Integer resultNumber = findSumOfItems(numbers);

            if (resultNumber == i) {
                armstrongsNumbers.add(resultNumber);
            }

        }

        return armstrongsNumbers;
    }

    private static void upgradeToCertainPower(Integer[] array) {

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) Math.pow(array[i], array.length);
        }

    }

    //18
    public static List<Integer> findIncreasingNumbers(int amountOfItems) {

        List<Integer> resultList = new ArrayList<Integer>();
        int minNumber = findMinNumber(amountOfItems);
        int maxNumber = findMaxNumber(amountOfItems);

        for (int i = minNumber; i < maxNumber; i++) {
            Integer[] array = createArrayOfItems(i);
            if (isIncreasing(array)) {
                resultList.add(i);
            }
        }

        return resultList;
    }

    private static int findMaxNumber(int amountOfItems) {

        int maxNumber = DEFAULT_MAX_DIGITAL;

        for (int i = DEFAULT_FIRST_NUMBER; i < amountOfItems; i++) {
            int number = DEFAULT_MAX_DIGITAL;
            for (int k = 0; k < i; k++) {
                number *= DEFAULT_TENTH_NUMBER;
            }
            maxNumber += number;
        }

        return maxNumber;
    }

    private static int findMinNumber(int amountOfItems) {

        int minNumber = DEFAULT_FIRST_NUMBER;

        for (int i = DEFAULT_FIRST_NUMBER; i < amountOfItems; i++) {
            minNumber *= DEFAULT_TENTH_NUMBER;
        }

        return minNumber;
    }

    private static boolean isIncreasing(Integer[] array) {

        boolean isIncreasing = DEFAULT_POSITIVE_RESULT;

        for (int i = DEFAULT_FIRST_NUMBER; i < array.length; i++) {
            if (array[i] <= array[i - DEFAULT_PREVIOUS_ELEMENT]) {
                isIncreasing = DEFAULT_NEGATIVE_RESULT;
                break;
            }
        }

        return isIncreasing;
    }
}
